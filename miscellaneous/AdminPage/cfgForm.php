<?php
session_start();
?>
<?php
// Include function to read the config file
require_once('./includes/parseini.php');
?>
<!-- Read/Display/Submit changes for the config file -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<title>Document Admin </title>
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" charset="utf-8" />
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/global.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/modal.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<?php require("./includes/header.php")?>
	<body>
		<?php include 'includes/menu.php' ?>
					<?php if((is_readable($CONFIG))&&(is_writable($CONFIG))) :?>
					<?php $stores = parse_ini($CONFIG) ;?>
						<form action="Modifycfg.php" method="post">
							<div id="form" class="help">
							<h1>Form:</h1>
							<div class="col w5">
								<div class="content">
									
										<p>
											<label for="simple_input">NetCdf output directory:</label>
											<input type="text" name="netcdf_output_dir" value="<?php echo $stores['paths']['netcdf_output_dir'];?>" class="text w_20" />
											<br />
										</p>
										<p class="small"> Where the WPS should store the netcdf output files.</p>
										<p>
											<label for="simple_input">NetCdf server directory</label>
											<input type="text" name="netcdf_server_dir" value="<?php echo $stores['paths']['netcdf_server_dir'];?>" class="text w_20" />
											<br />
										</p>
										
							            <p class="small"> The prefix of the link to send back as the output of the program for netcdf</p>
										
										<p>
											<label for="simple_input">Output directory</label>
											<input type="text" name="output_dir" value="<?php echo $stores['paths']['output_dir'];?>" class="text w_20" />
											<br />
										</p>
										<p class="small"> Where the WPS should store the other output files</p>
										<p>
											<label for="simple_input">Sever Output:</label>
											<input type="text" name="server_output_dir" value="<?php echo $stores['paths']['server_output_dir'];?>" class="text w_20" />
											<br />
										</p>
										<p class="small">The prefix of the link to send back as the output of the program for npn netcdf files</p>
                                        <p>
											<label for="select">Density:</label>
											<select name="density" id="select">
												<option value="smooth" <?php if($stores['processing']['density']=="smooth") echo 'selected="selected"';?>>smooth</option>
												<option value="scipy" <?php if($stores['processing']['density']=="scipy") echo 'selected="selected"';?>>scipy</option>
											</select>
										</p>
										<p class="small"> Select Fix Processing Method ( smooth or scipy )</p>
										<p>
											<label>Use cython:</label>
                                            <input type="hidden" name="use_cython" value="0" />
											<input type="checkbox" class="checkbox" name="use_cython" <?php if($stores['processing']['use_cython']=="True") echo 'checked="checked"';?>>
                                            <br />
                                        </p>
										<p class="small"> Set this to True to use Cython optimization, False for regular Python</p>
                                        <p>
											<label for="select">Kernel:</label>
											<select name="kernel" id="select">
												<option value="lucy" <?php if($stores['processing']['kernel']=="lucy") echo 'selected="selected"';?>>lucy</option>
												<option value="gauss" <?php if($stores['processing']['kernel']=="gauss") echo 'selected="selected"';?>>gauss</option>
											</select>
										</p>
                                        <p class="small">The kernel to be used to compute the density</p>
                                </div>
								
							</div>
							
							<div class="col w5 last">
								<div class="content">
									
										<p>
											<label>Use database:</label>
                                            <input type="hidden" name="use_database" value="0" />
											<input type="checkbox" class="checkbox" name="use_database" <?php if($stores['database']['use_database']=="True") echo 'checked="checked"';?>>
											<br />
										</p>
										<p class="small"> Set this to True to use the database, False for csv file</p>
										<p>
											<label for="simple_input">Database name:</label>
											<input type="text" name="database_name" value="<?php echo $stores['database']['database_name'];?>" class="text w_20" />
											<br />
										</p>
										<p>
											<label for="simple_input">Database user:</label>
											<input type="text" name="database_user" value="<?php echo $stores['database']['database_user'];?>" class="text w_20" />
											<br />
										</p>
										<p>
											<label for="simple_input">Database password:</label>
											<input type="text" name="database_password" value="<?php echo $stores['database']['database_password'];?>" class="text w_20" />
											<br />
										</p>
										<p>
											<label for="simple_input">Database table:</label>
											<input type="text" name="database_table" value="<?php echo $stores['database']['database_table'];?>" class="text w_20" />
											<br />
										</p>
										<p class="small"> Database access information</p>
										
										<p>
											<label>Use caching:</label>
                                            <input type="hidden" name="use_caching" value="0" />
											<input type="checkbox" class="checkbox" name="use_caching" <?php if($stores['caching']['use_caching']=="True") echo 'checked="checked"';?>>
											<br />
										</p>
										<p class="small"> Set this to True to use caching, False for no caching</p>
										<p>
											<label for="simple_input">Cache directory:</label>
											<input type="text" name="cache_directory" value="<?php echo $stores['caching']['cache_directory'];?>" class="text w_20" />
											<br />
										</p>
                                        <p>
											<label>Debug:</label>
                                            <input type="hidden" name="debug" value="0" />
											<input type="checkbox" class="checkbox" name="debug" <?php if($stores['general']['debug']=="True") echo 'checked="checked"';?>>
											<br />
										</p>
                                        <p class="small"> Debug mode (will put the actual errors in conf["lenv"]["message"])</p>
                                        <p>
											<label for="simple_input">Log file:</label>
											<input type="text" name="log_file" value="<?php echo $stores['general']['log_file'];?>" class="text w_20" />
											<br />
										</p>
                                        <p class="small"> Path to the log file</p>
                                        
								</div>
							</div>
							
							<div class="clear"></div>
							<div class="cen">
							<center><a href="" class="button form_submit" ><span>Update</span></a></center>
							</div>
							<div class="clear"></div>
							</div>
							
							
							</form>
							<?php else: ?>
							<div id="message" class="help">
								<h1>Message:</h1>
									<div class="col w10 last">
										<div class="content">
											<div class="error">
												<div class="tl"></div><div class="tr"></div>
													<div class="desc">
														<p>Error ! Configuration files permission denied</br></p>
													</div>
												<div class="bl"></div><div class="br"></div>
											</div>
									
						
								
										</div>
							</div>
							</div>
							<?php endif ;?>
				</div>
			</div>
		</div>
	</body>
</html>
