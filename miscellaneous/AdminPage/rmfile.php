<?php
session_start();
?>
<?php
// Include function to read ini files (config)
require_once('./includes/parseini.php');
$config_info = getConfigInfo($CONFIG);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<title>Document Admin </title>
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" charset="utf-8" />
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/global.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/modal.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<?php require("./includes/header.php")?>
	<body>
	<?php include 'includes/menu.php' ?>
					<div>
                        <?php
                            if($config_info == null){
                                echo 'Error loading configuration file, some functionalities will not work. <br />';
                            }
                        ?>
						<div id="message" class="help">
							<h1>Message:</h1>
							<div class="col w10 last">
								<div class="content">
										<?php
											require("delete.php");
											if(isset($_GET['file'])){
												$file=$_GET['file'];
													if(preg_match("/^[a-zA-Z0-9\/\-\_]+\.xml$/",$file))
													{

														$xml="./xml/";
														del_file($xml,$file);
													}
                                                    if(isset($_GET['type'])){
                                                        if($_GET['type'] == "netcdf"){
                                                        if(preg_match("/^[a-zA-Z0-9\/\-\_]+\.nc$/",$file))
                                                        {
                                                            $dir=$config_info['paths']['netcdf_output_dir'] . '/';
                                                            del_file($dir,$file);
                                                        }
                                                        }
                                                    }
											}
											else if (isset($_GET['files'])){
												$files=$_GET['files'];
												if(empty($files)){
													echo("<div class='error'>
										<div class='tl'></div><div class='tr'></div>
										<div class='desc'>
											<p>You didn`t select any file</p>
										</div>
										<div class='bl'></div><div class='br'></div>
									</div>");
												}
												else{
                                                        echo $files;
													foreach($files as $file){
														if(preg_match("/^[a-zA-Z0-9\/\-\_]+\.xml$/",$file))
														{
															$xml="./xml/";
															del_file($xml,$file);
														}
														else if($_GET['type'] == "netcdf"){
                                                        if(preg_match("/^[a-zA-Z0-9\/\-\_]+\.nc$/",$file))
                                                        {
                                                            $dir=$config_info['paths']['netcdf_output_dir'] . '/';
                                                            del_file($dir,$file);
                                                        }
                                                        }
                                                    }
												}
											}
?>
									
									
								</div>
							</div>
							<div class="clear"></div>
							</div>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>
