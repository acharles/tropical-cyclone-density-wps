<div id="header">
			<div class="col w5 bottomlast">
				<a href="empManage.php" class="logo">
					<img src="images/logo.gif" alt="Logo" />
				</a>
			</div>
			<div class="col w5 last right bottomlast">
				<p class="last">Logged in as <span class="strong">Admin,</span> <a href="logout.php">Logout</a></p>
			</div>
			<div class="clear"></div>
		</div>
		<div id="wrapper">
			<div id="minwidth">
				<div id="holder">
					<div id="menu">
						<div id="left"></div>
						<div id="right"></div>
						<ul>
							
                            <li>
								<a href="/"><span>Home page</span></a>
							</li>
							<li>
								<a href="netcdfList.php"><span>Netcdf</span></a>
							</li>
							<li>
								<a href="cfgForm.php"><span>File Configuration</span></a>
							</li>
							<li>
								<a href="clean.php"><span>Clean</span></a>
							</li>
						</ul>
						<div class="clear"></div>
					</div>
					<div>
