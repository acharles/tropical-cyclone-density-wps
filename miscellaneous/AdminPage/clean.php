<?php
session_start();
?>
<?php
// Include function to read ini files (config)
require_once('./includes/parseini.php');
$config_info = getConfigInfo($CONFIG);
?>
<!-- Clean stuff -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<title>Document Admin </title>
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" charset="utf-8" />
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/global.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/modal.js" type="text/javascript" charset="utf-8"></script>
        <script>
            function redirect(url){
                window.location = url;
            }
            
            function setBandwidth(){
                var elem = document.getElementById('bandwidth');
                var value = elem.options[elem.selectedIndex].text;
                redirect('clean.php?bandwidth='+value);
            }
        </script>
	</head>
	<?php require("./includes/header.php")?>
	<body>
		<?php include 'includes/menu.php' ?>
<h1>Hello:</h1>
<label for="simple_input">
    <div>Clear the cache after clearing output files to avoid broken links.</div>
<?php

if($config_info == null){
    echo 'Error loading configuration file, some functionalities will not work. <br />';
    //die();
}

if(isset($_GET['clearOutputFiles'])){
    $out1 = system("rm -f " . $config_info['paths']['output_dir'] . "/TrackDensity*", $rmOut1);
    $out2 = system("rm -f " . $config_info['paths']['netcdf_output_dir'] . "/TrackDensityNetCDF-*", $rmOut2);
    if ($out1 != 0 || $rmOut1 != 0){
        echo "An error occured while clearing non netcdf output files <br />";
    }
    else{
        echo "Cleared non netcdf output files<br />";
    }
    if ($rmOut2 != 0 || $out2 != 0){
        echo "An error occured while clearing netcdf output files <br />";
    }
    else{
        echo "Cleared netcdf output files<br />";
    }
}
else if(isset($_GET['clearTemporaryFiles'])){
    $out1 = system("rm -f /var/www/temp/TrackDensity*", $rmOut);
    if ($out1 != 0 || $rmOut != 0){
        echo "An error occured while clearing temporary files (no files to delete?)<br />";
    }
    else{
        echo "Cleared temporary files<br />";
    }
}
else if(isset($_GET['clearCache'])){
    $out1 = system("rm -rf " . $config_info['caching']['cache_directory'] . "/*", $rmOut1);
    if ($out1 != 0 || $rmOut1 != 0){
        echo "An error occured while clearing the cache<br />";
    }
    else{
        echo "Cleared cache<br />";
    }
}
else if(isset($_GET['clearLog'])){
    $out1 = system("rm -rf " . $config_info['general']['log_file'], $rmOut1);
    if ($out1 != 0 || $rmOut1 != 0){
        echo "An error occured while clearing the log file.<br />";
    }
    else{
        echo "Cleared log file.<br />";
    }
}
else if (isset($_GET['logFile'])){
    $file = $config_info['general']['log_file'];
    if (file_exists($file)) {
        header('Content-Description: File Transfer');
        header('Content-Type: text/plain');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
    }
}
?>
</label>
<br />

<input type="button" value="Clear output files" onclick="redirect('clean.php?clearOutputFiles')" />
<input type="button" value="Clear temporary files" onclick="redirect('clean.php?clearTemporaryFiles')" />
<input type="button" value="Clear cache" onclick="redirect('clean.php?clearCache')" />
<input type="button" value="Clear log" onclick="redirect('clean.php?clearLog')" />
<br />
<div class="desc">
    <p>
<a href="/temp/" target="_blank">Temp directory</a> &nbsp; &nbsp; &nbsp;
<a href="clean.php?logFile" target="_blank">Log file</a>
    </p>
</div>
</body></html>
