<?php session_start();?>
<!-- This file is used to modify the config file using post parameters -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<title>Document Admin </title>
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" charset="utf-8" />
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/global.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/modal.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>
	<?php require("./includes/header.php")?>
	<?php include 'includes/menu.php' ?>
<?php require_once('./includes/parseini.php');?>
<?php
    if(!isset($_POST)){
        die();
    }
	if((is_readable($CONFIG))&&(is_writable($CONFIG))){
        // Read the config file
        $stores = parse_ini($CONFIG);
    // Set all the new parameters for the config file
	if(isset($stores['paths']['netcdf_output_dir']) && isset($_POST['netcdf_output_dir']))
		{
			$stores['paths']['netcdf_output_dir'] = $_POST['netcdf_output_dir'];
		}
	if(isset($stores['paths']['netcdf_server_dir']) && isset($_POST['netcdf_server_dir']))
		{
			$stores['paths']['netcdf_server_dir'] = $_POST['netcdf_server_dir'];
		}
	if(isset($stores['paths']['output_dir']) && isset($_POST['output_dir']))
		{
			$stores['paths']['output_dir'] = $_POST['output_dir'];
		}
	if(isset($stores['paths']['server_output_dir']) && isset($_POST['server_output_dir']))
		{
			$stores['paths']['server_output_dir'] = $_POST['server_output_dir'];
		}
	if(isset($stores['database']['use_database']) && isset($_POST['use_database']))
		{
			if($_POST['use_database']== "on"){
				$stores['database']['use_database'] = "True";
				}
			else{
				$stores['database']['use_database'] = "False";
			}
		}
    if(isset($stores['database']['database_name']) && isset($_POST['database_name']))
        {
            $stores['database']['database_name'] = $_POST['database_name'];
        }
    if(isset($stores['database']['database_user']) && isset($_POST['database_user']))
        {
            $stores['database']['database_user'] = $_POST['database_user'];
        }
    if(isset($stores['database']['database_password']) && isset($_POST['database_password']))
        {
            $stores['database']['database_password'] = $_POST['database_password'];
        }
    if(isset($stores['database']['database_table']) && isset($_POST['database_table']))
        {
            $stores['database']['database_table'] = $_POST['database_table'];
        }
	if(isset($stores['general']['debug']) && isset($_POST['debug']))
		{
			if($_POST['debug']== "on"){
				$stores['general']['debug'] = "True";
				}
			else{
				$stores['general']['debug'] = "False";
			}
		}
    if(isset($stores['general']['log_file']) && isset($_POST['log_file']))
		{
            $stores['general']['log_file'] = $_POST['log_file'];
        }
	if(isset($stores['processing']['use_cython']) && isset($_POST['use_cython']))
		{
			if($_POST['use_cython']== "on"){
				$stores['processing']['use_cython'] = "True";
				}
			else{
				$stores['processing']['use_cython'] = "False";
			}
		}
	if(isset($stores['processing']['density']) && isset($_POST['density']))
		{
			$stores['processing']['density'] = $_POST['density'];
		}
    if(isset($stores['processing']['kernel']) && isset($_POST['kernel']))
		{
			$stores['processing']['kernel'] = $_POST['kernel'];
		}
    
    if(isset($stores['caching']['use_caching']) && isset($_POST['use_caching']))
		{
			if($_POST['use_caching']== "on"){
				$stores['caching']['use_caching'] = "True";
				}
			else{
				$stores['caching']['use_caching'] = "False";
			}
		}
        
    if(isset($stores['caching']['cache_directory']) && isset($_POST['cache_directory']))
		{
			$stores['caching']['cache_directory'] = $_POST['cache_directory'];
		}

    $fopen=fopen($CONFIG,'w');
    if($fopen){
        foreach($stores as $key => $array){
            fwrite($fopen,"[".$key."]\r\n");
            foreach($array as $name => $value){
            
             fwrite($fopen,$name." = ".$value."\r\n");
            }
        }
        fclose($fopen);
        echo '<div id="message" class="help">
                                <h1>Message:</h1>
                                <div class="col w10 last">
                                    <div class="content">
                                        <div class="success">
                                            <div class="tl"></div><div class="tr"></div>
                                            <div class="desc">
                                                <p>Congratulations ! Successfully configuration</br></p>
                                            </div>
                                            <div class="bl"></div><div class="br"></div>
                                        </div>
                                        
                            
                                    
                                </div>
                        </div>';
    }
    }
    else{
        // Config file not readable/writable
        echo '<div id="message" class="help">
							<h1>Message:</h1>
							<div class="col w10 last">
								<div class="content">
									<div class="error">
										<div class="tl"></div><div class="tr"></div>
										<div class="desc">
											<p>Errors ! Configure files not allowed to modify</br></p>
										</div>
										<div class="bl"></div><div class="br"></div>
									</div>
									
						
								
							</div>
					</div>';
    }
?>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
