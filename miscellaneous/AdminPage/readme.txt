Change $CONFIG in includes/parseini.php to the path of the configuration file.

/css,js,images directories are the resources for web page
/includes/header.php is to control admin session

cfgForm.php: page to enter configuration file data
clean.php: page to remove groups of files
delete.php: Function to delete a file
index.php: redirects to cfgForm.php
login.php/loginProcess.php: pages to enter username/password and check it
logout.php: User log out
Modifycfg.php: modifies the configuration file with data from cfgForm.php
netcdfList: List of netcdf files to delete
rmfile.php: deletes one or more files
