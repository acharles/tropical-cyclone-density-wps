import httplib
import time

res1 = "/zoo/?Service=WPS&Request=Execute&Version=1.1.0&Identifier=TrackDensityDataInputs=year_range=1970,2000;hemisphere=s;output_format=NETCDF;genesis=False"
res2 = "/pydap/timingOutput.nc.das"
res3 = "/contour-wms/?styles=grid&color_scale_range=1e-05,0.000562273&color_range=-10,10&TRANSPARENT=TRUE&format=image/png&source_url=http://115.146.84.158/pydap/timingOutput.nc&layers=den&time_index=Default&width=2048&height=2048&n_color=10&crs=EPSG:4326&palette=jet&request=GetMap&time=Default"

def timing_res (res) :
    conn = httplib.HTTPConnection("115.146.84.158")
    m1 = int(round(time.time() * 1000))
    conn.request("GET", res)
    conn.getresponse()
    m2 = int(round(time.time() * 1000))
    return m2 - m1

def avg_timing (count, res) :
    total_time = 0
    index = 0
    while (index < count):
        total_time = total_time + timing_res(res)
        index += 1
    print("Resource: " + res)
    print("Testing Times: " + str(count))
    print("Average Response Time: " + str(round(total_time / count)) + "ms")

#
# Timing response time
#
print
print("------------------------------------------------------")
avg_timing(10, res1)
print("------------------------------------------------------")
avg_timing(10, res2)
print("------------------------------------------------------")
avg_timing(10, res3)
