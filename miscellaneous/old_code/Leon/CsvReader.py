'''
Created on Aug 26, 2013

@author: lionhylra
'''
import csv


# from default import tc



class CsvReader(object):
    '''
    classdocs
    '''


    def __init__(self,fname,**arglists):
        '''
        Constructor:
        1//fname is the file name of the csv to open e.g. ./file.csv
        2//args is a dictionary contains arguments used to retrieve the data e.g. {year:2002,name:"hello"}
        '''
        self.fname=fname
        self.arglists=arglists
        self.raw_file=self.load_csv_into_list()
    
    def load_csv_into_list(self):  
        '''
        this method read a csv file and out put as a list
        it is called by constructor
        e.g.
        [
        {'year': '2009', 'lat': '-14.65',...}
        {'year': '2009', 'lat': '-14.65',...}
        {'year': '2009', 'lat': '-14.65',...}
        ...
        ]
        '''
        
        with open(self.fname,'rb') as f:
            input_file = csv.DictReader(f)
            result=[]
            for row in input_file:
                result.append(row)
#         self.raw_file=result[:]#cache the file data
        return result
    
    def get_result_list(self):
        '''
        this method produce a filtered result
        '''
        
        result=self.raw_file[:]
        for attr_name in self.arglists:
            result=self.filter(result,attr_name,self.arglists[attr_name])
        return result
    
    def filter(self,data,attr,value):
        '''
        filter the result
        '''
        
        result=[]
        for row in data:
            if (row[attr] in value):
                result.append(row)
        return result

def combine_results(*readers):
    '''
    this method produce a combined results of several readers
    '''
    '''
    1//get the result from each reader
    2//put results in a list
    '''
    results=[]
    for reader in readers:
        results.append(reader.get_result_list())
    '''
    3//convert each result's entry from dict into set
    If not, they are unhashable. That means these results cannot be combined.
    ''' 
    frozened_results=[]
    for res in results:
        tmp_rows=[]
        for row in res:
            tmp_rows.append(frozenset(row.items()))
        frozened_results.append(tmp_rows)
    
    '''
    4//combine all the results
    '''
    tmp_set=set()
    for item in frozened_results:
        tmp_set=tmp_set.union(set(item))
    rows=list(tmp_set)
    '''
    5//revert the result to dict
    '''
    result=[]
    for row in rows:
        result.append(dict(row))
    return result

    
def main():
    reader1=CsvReader("tcsh1969-2009.csv",year=['1969','2009'])
    reader2=CsvReader("tcsh1969-2009.csv",year=['1979'],pressure=['1002'])
    res=combine_results(reader1, reader2)     
    i=0
    for row in res:
        i+=1
        print row
        if(i%100==0):
            raw_input('')
    print '='*20+"end of result"+'='*20
if __name__ == '__main__':
    main()
        
        