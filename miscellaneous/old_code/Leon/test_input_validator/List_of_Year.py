'''
Created on 29/08/2013

@author: s3299824
'''
import unittest
import input_validator

class Test_validate_year(unittest.TestCase):
      
        
        def setUp(self):
#    set up a valid year 
                
                self.List_of_year={'output_method': {'value': 'JSON'}, 'year': {'value': '1970,1987,2012,2022'}}
         
#    campare the expected output year and practical result 
        def test_year(self):
                expected_output = {'output_method':'JSON','year':[1970,1987,2012,2022]}
                valid_year=input_validator.check_arguments(self.List_of_year)
                self.assertEqual(valid_year, expected_output)
#    
      
#       
if __name__ == '__main__':
        unittest.main()