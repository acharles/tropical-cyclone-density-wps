import unittest
import input_validator

class Test_validate_year(unittest.TestCase):
      
        
        def setUp(self):
#    set up a valid year 
                self.valid_year={'output_method': {'value': 'JSON'}, 'year': {'value': '1970'}}
                self.InvalidFormat_year={'output_method': {'value': 'JSON'}, 'year': {'value': '193^'}}
                self.OutofRange_year1={'output_method': {'value': 'JSON'}, 'year': {'value': '0193'}}
                self.OutofRange_year2={'output_method': {'value': 'JSON'}, 'year': {'value': '10193'}}
                self.Missing_year={'output_method': {'value': 'JSON'}}
                self.Output_method={'output_method': {'value': 'WWWW'}, 'year': {'value': '1970'}}
                self.List_of_year={'output_method': {'value': 'JSON'}, 'year': {'value': '1970,1987,2012,2022'}}
                self.Invalid_Format_List_of_year={'output_method': {'value': 'JSON'}, 'year': {'value': '1970.1987.2012,,,2022'}}
                self.Invalid_List_of_year={'output_method': {'value': 'JSON'}, 'year': {'value': '11970,1987,2012,2022'}}
#    campare the expected output year and practical result 
        def test_year(self):
                expected_output = {'output_method':'JSON','year':[1970]}
                valid_year=input_validator.check_arguments(self.valid_year)
                self.assertEqual(valid_year, expected_output)
#    
        def test_exception(self):
                self.assertRaises(input_validator.check_arguments(self.valid_year))
#       
if __name__ == '__main__':
        unittest.main()
'''
Created on 29/08/2013

@author: s3299824
'''
