import pycurl
 
def get_wms_image(nc, bbox):
	"""This function calls the image url"""
	width=str((bbox[2]-bbox[0])*(2048/360))
	height=str((bbox[1]-bbox[3])*(2048/180))
	bbox_str=str(bbox)[1:-1]
	Link = 'http://0.0.0.0:8007/?styles=contour&color_range=-10,10&TRANSPARENT=TRUE&format=image/png&source_url=' + nc+'&layers=den&bbox='+bbox_str+'&time_index=Default&height='+height+'&n_color=10&crs=EPSG:4283&palette=jet&request=GetMap&width='+width+'&time=Default';
	c = pycurl.Curl()
	c.setopt(c.URL, Link)
	c.perform()