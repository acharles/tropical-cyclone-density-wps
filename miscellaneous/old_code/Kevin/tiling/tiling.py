from PIL import Image
import os


NUMBER_OF_TILES_X = 8
NUMBER_OF_TILES_Y = 8
Z_VALUE = 3
TILE_SIZE_X = 256
TILE_SIZE_Y = 256

def tileimage(image_file_path, output_directory_path):
    """From an image, create a set of images that are subsets of the 
    image provided.
    """
    file_name = os.path.basename(image_file_path)
    file_name_prefix, file_extension = os.path.splitext(file_name)
    if output_directory_path == '.':
        output_directory_path = ''
    if not output_directory_path.endswith('/'):
        output_directory_path += '/'
    layer_image = Image.open(image_file_path)
    for x in range(NUMBER_OF_TILES_X):
        for y in range(NUMBER_OF_TILES_Y):
            tile = layer_image.crop((\
            TILE_SIZE_X*x, \
            TILE_SIZE_Y*y,\
            TILE_SIZE_X+(TILE_SIZE_X*x), \
            TILE_SIZE_Y+(TILE_SIZE_Y*y )))
            tile.save("{out_dir}{prefix}-{zval}-{xval}-{yval}{ext}"\
            .format(out_dir=output_directory_path, prefix=file_name_prefix,\
            zval=Z_VALUE,xval=x,yval=y,ext=file_extension))

