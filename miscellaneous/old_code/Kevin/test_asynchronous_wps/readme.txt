This service uses a directory named 'temp' to store the xml files:
To create it:
sudo mkdir /var/www/temp
sudo chown -R www-data /var/www/temp

By default the service requests the image at localhost/success.png,
this can be changed in UpdatingService.py

By default, the service is set to work on localhost, to be run on an ip:
sed 's/localhost/127.0.0.1/g' main.cfg > main.cfg
sed 's/localhost/127.0.0.1/g' UpdatingService.py > UpdatingService.py

