import zoo
import time

def UpdatingService(conf, input, output):
    try:
        for i in range(20):
            conf['lenv']['status'] = str(i)
            zoo.update_status(conf, i)
            time.sleep(1)
        output["Result"]["value"] = "http://localhost/success.png"
    except BaseException as exc:
        conf['lenv']['message'] = str(exc)
        return 4
    return 3
