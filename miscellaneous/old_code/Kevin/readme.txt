The test_asynchronous_wps directory contains a simple wps that emulates a long
running process. It is used to test the client-side.

The tiling directory contains a python module to create tiles from an image.

The dynamic_page directory contains a template of a flask application.
