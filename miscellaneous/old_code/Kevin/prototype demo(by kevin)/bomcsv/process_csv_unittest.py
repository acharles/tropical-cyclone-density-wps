import unittest
import process_csv
import os

class InputTest(unittest.TestCase):
	"""Creates a temporary file and test that reading from it produces
	the expected output.
	"""
	
	def setUp(self):
		# Create an example csv file
		self.temporary_file_name = "unittesting_csv.csv"
		example_csv_content = "lat,lon\n1,2\n3,4\n5,6"
		self.file_handler = open(self.temporary_file_name, "w+")
		self.file_handler.write(example_csv_content)
		self.file_handler.close()
		
	def test_import_csv(self):
		expected_output = [dict(lat='1', lon='2'), 
		dict(lat='3', lon='4'), dict(lat='5',lon='6')]
		list_of_fixes = process_file.read_csv_from_file(self.temporary_file_name)
		self.assertEqual(list_of_fixes, expected_output)
	
	def tearDown(self):
		os.remove(self.temporary_file_name)
		

class OutputTest(unittest.TestCase):
	"""Provide a list of list as input and check that the program 
	produces correct json and xml for it.
	"""
	
	def setUp(self):
		self.input_data = [[1.0, 0.5, 1.1], [2.0, 0.2, 2.1], 
		[0.1, 0.1, 2.1]]
	
	def test_xml_output(self):
		expected_output_xml = """\
<root><col>1.0, 0.5, 1.1</col>\
<col>2.0, 0.2, 2.1</col><col>0.1, 0.1, 2.1</col></root>"""
		self.assertEqual(expected_output_xml, 
						 process_file.output_xml(self.input_data))
						 
	def test_json_output(self):
		expected_output_json = str(self.input_data)
		self.assertEqual(expected_output_json, 
						 process_file.output_json(self.input_data))
		

if __name__ == '__main__':
	unittest.main()
