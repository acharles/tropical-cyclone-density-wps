import csv
import xml.etree.ElementTree as et
import json
import sys

def read_rows_into_list(csv_file_handler):
	"""Read a dictionary for each line of the file into a list"""
	max_number_of_rows = 5
	list_of_fixes = []
	csv_reader = csv.DictReader(csv_file_handler)
	# Append a maximum of max_number_of_rows rows to the list
	for index, row in enumerate(csv_reader):
		if index > max_number_of_rows:
			break
		list_of_fixes.append(row)
	return list_of_fixes

def read_csv_from_file(file_name):
	"""Read lines from file_name into a list
	
	Raises an IOError if there was a problem reading the file
	"""
	list_of_fixes = []
	csv_file_handler = None
	try:
		csv_file_handler = open(file_name, 'r')
		list_of_fixes=read_rows_into_list(csv_file_handler)
	except IOError:
		raise
	finally:
		if csv_file_handler:
			csv_file_handler.close()
	return list_of_fixes
	
def output_json(data):
	"""Return a string containing the JSON representation of the data"""
	# Check if data is valid ?
	return json.dumps(data)
	
def output_xml(data):
	"""Return a string containing the XML representation of the list
	
	Arguments:
	data: a list of list of numbers to serialize to xml
	"""
	root = et.Element("root")
	for sub_list in data:
		col = et.SubElement(root, "col")
		col.text = ', '.join(str(value) for value in sub_list)
	return et.tostring(root)
