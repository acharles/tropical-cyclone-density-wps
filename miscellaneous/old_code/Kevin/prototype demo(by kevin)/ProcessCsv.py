import zoo
import json
import bomcsv.process_csv as pc
import sys


def ProcessCsv(conf,inputs,outputs):
    """Reads a csv file and returns some data in XML or JSON"""
    output_method = inputs["outputmethod"]["value"]
    if output_method != "xml" and output_method != "json":
        # Error message to return in the error report
        conf["lenv"]["message"] = "Incorrect output method"
        return zoo.SERVICE_FAILED
    # Read the data
    try:
        rows_in_csv = pc.read_csv_from_file(
    "bomcsv/res/tcsh1969-2009.csv")
    except IOError as exc:
        conf["lenv"]["message"] = "Internal error"
        sys.stderr.write("Could not read the file: {error}\n".format(
			error=str(exc)))
        return zoo.SERVICE_FAILED
    # Do some processing here
    processing_output = [[1.0,2.0,1.5],[2.3,0.5,2.2],[0.2,2.7,1.9]]
    # Get the representation of the data in the right format
    if output_method == "xml":
        outputs["Result"]["value"] = pc.output_xml(
        processing_output)
    elif output_method == "json":
        outputs["Result"]["value"] = pc.output_json(
        processing_output)
    return zoo.SERVICE_SUCCEEDED
