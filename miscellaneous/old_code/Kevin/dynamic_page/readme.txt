This directory contains files to make flask generate dynamic pages.
page.py is the code used to run the application.
templates/stuff.html is the template that can be used to create different pages.

Make sure flask is installed:
http://flask.pocoo.org/docs/installation

Then do:
python page.py
It should output:
Running on http://127.0.0.1:5000/

See the documentation at 
http://flask.pocoo.org/docs/quickstart/#rendering-templates
and
http://flask.pocoo.org/docs/templates


In order to make the application work on apache:
$ mkdir /var/www/helloflask
$ sudo nano /var/www/helloflask/application.wsgi:
import sys
sys.path.insert(0, '/var/www/helloflask')
from colgate import app as application
Copy the files into /var/www/helloflask
$ sudo nano /var/www/helloflask/application.wsgi:
Modify:
app.run(debug=True)
with
app.run(port=80)

Now in the apache config file (/etc/apache2/sites-available/default for me), 
in a virtual host element, add:
WSGIDaemonProcess helloflask user=www-data group=www-data threads=5
WSGIScriptAlias /helloflask /var/www/helloflask/application.wsgi

<Directory /var/www/helloflask>
    WSGIProcessGroup helloflask
    WSGIApplicationGroup %{GLOBAL}
    Order deny,allow
    Allow from all
</Directory>

Restart apache:
sudo apachectl restart
Access the page: http://127.0.0.1/helloflask
