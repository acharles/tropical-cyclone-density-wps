var form, year, year_from, year_to;

$(document).ready(function() {
    form = $("form:first");
    year = $('#year');
    year_from = $('#year_from');
    year_to = $('#year_to');
    form.submit(function(event) {
        year.val(year_from.val() + ',' + year_to.val());
    });
});