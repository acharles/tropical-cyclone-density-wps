var initUrl = 'InProcess.xml'; // Change this url to the real one.
var statusUrl = 'EndProcess.xml';
var msg;
var timeout = 3000;// Timeout: 3 seconds.
var counter;

$(document).ready(function() {
    msg = $('#msg');
    counter = 5;
    $.get(initUrl, function(data) {
        parseXML(data);
    });
});

function parseXML(data) {
    var finished = false;
    if(($(data).find('ProcessSucceeded').length > 0) || ($(data).find('wps\\:ProcessSucceeded').length > 0)) {
        // Chrome uses different syntax from Firefox.
        // 'wps\\:ProcessSucceeded' for Firefox, 'ProcessSucceeded' for Chrome.
        
        // Process finished.
        var pngUrl;
        if($(data).find('ProcessSucceeded').length > 0)
            pngUrl = $(data).find('ComplexData').text();
        else pngUrl = $(data).find('wps\\:ComplexData').text();
        msg.html('PNG is Ready. URL: ' + pngUrl);
    } else {
        // Fetch status file location.
        // Uncomment the following line in product.
        // statusUrl = $(data).find('wps\\:ExecuteResponse').attr('statusLocation');

        // Waiting for timeout/1000 sesonds
        msg.html('Refresh in '+ counter +' seconds...');
        timer();
    }
}

function timer() {
    setTimeout(function() {
        if(counter > 1) {
            counter --;
            msg.html('Refresh in '+ counter +' seconds...');
            timer();
        } else {
            counter = 5;
            $.get(statusUrl, function(data) {
                parseXML(data);
            });
        }
    }, 1000);
}

// The function to parse the bbox arguments supplied by the user.
// It returns the url which will be used to load the transparent png file.
function get_wms_img_url(nc, minX, minY, maxX, maxY) {
    var width = (maxX - minX) * (2048 / 360);
    var height = (maxY - minY) * (2048 / 180);
    var bboxStr = minX + ',' + minY + ',' + maxX + ',' + maxY
    var url = 'http://127.0.0.1:8007/?styles=contour&color_range=-10,10&TRANSPARENT=TRUE&format=image/png&source_url=' + nc +'&layers=den&bbox='+ bboxStr +'&time_index=Default&height='+ height +'&n_color=10&crs=EPSG:4283&palette=jet&request=GetMap&width='+ width +'&time=Default';
    return url;
}
