The AdminPage directory contains server-side pages used for the administration
of the WPS.

The old_code directory contains files that are not used in the project but
might still be useful.

The timing_module directory contains a python module to time the different
parts that the application is made out of.

The WFSProxy directory contains a php file that can be used to do a request 
to the WFS from a different domain in Javascript (using CORS).
