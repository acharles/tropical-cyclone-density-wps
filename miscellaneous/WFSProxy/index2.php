<?php
// respond to preflights
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
  // return only the headers and not the content
  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']) && $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] == 'POST') {
    header('Access-Control-Allow-Headers: X-Requested-With, Content-type, Authorization, X-Authorization');
    header('Access-Control-Allow-Methods: OPTIONS');
    }
  exit;
}

if(!isset($HTTP_RAW_POST_DATA)){
  echo "No xml file was posted!";
  die();
}

$WFSUrl = "http://reg.bom.gov.au/cgi-bin/ws/gis/ncc/tracker/beta/wfs_filter.cgi?source=sh&nofilter=true";
// Make a request to the WFS and posting the XML file posted
// to this page (through POST)
$ch = curl_init();
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml; charset=UTF-8', 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:24.0) Gecko/20100101 Firefox/24.0'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, $WFSUrl);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($ch, CURLOPT_POSTFIELDS, escapeshellarg($HTTP_RAW_POST_DATA));
curl_setopt($ch, CURLOPT_NOBODY, false);
curl_setopt($ch, CURLOPT_VERBOSE, true);
$content = curl_exec($ch);
curl_close($ch);

// Check the output of curl
if(!$content){
    echo("Request failed.");
    die(curl_error($ch));
}

// Set header and return the returned value
header("Content-type:application/json");

//header("Access-Control-Allow-Origin: *");
//header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
//echo "gecko";
echo $content;
?>
