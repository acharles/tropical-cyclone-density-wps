"""
This module outputs the result of the processing in different formats.

Input:
data: In (xm, ym, Z) format where xm, ym, and Z are numpy arrays
args: valid arguments given to the WPS, this module needs 'output_format', and
must also contain bbox if the 'data' tuple doesn't cover the whole earth 
(all latitudes and longitudes)
For example:
import output
import numpy as np
xm = np.array([[ 62.5,  63.5,  64.5,  65.5,  66.5,  67.5],
       [ 62.5,  63.5,  64.5,  65.5,  66.5,  67.5]])
ym = np.array([[-9.5, -9.5, -9.5, -9.5, -9.5, -9.5],
       [-8.5, -8.5, -8.5, -8.5, -8.5, -8.5]])
Z = np.array([[ 0.0,  0.01005579,  0.0108799 ,  0.01107924,  0.01062354,
         0.00959108],
       [ 0.00771101,  0.01004376,  0.0108636 ,  0.01105884,  0.01059998,
         0.00956597]])
data = (xm, ym, Z)
args = {'output_format': 'NETCDF', 'bbox': [62, -10, 68, -8]}
WPS_CONFIG = {'server_output_dir': './', \
              'use_database': False, \
              'output_dir': './', 'debug': True, \
              'netcdf_server_dir': './', \
              'netcdf_output_dir': './'}
output.output_data(data, args, WPS_CONFIG)

(the file will be saved in the current directory)

Output:
TODO: this will change.
This module returns a string containing the data formatted in the requested
format: currently JSON or NetCDF
"""
import json
import netCDF4 as netcdf
import random
from time import gmtime, strftime


JSON_PREFIX = 'TrackDensityJson'
GEOJSON_PREFIX = 'TrackDensityGeoJson'
NETCDF_PREFIX = 'TrackDensityNetCDF'
# Number of digits for the random number at the end of the file names
RANDOM_NUMBER_DIGITS = 5

def output_data(data, args, WPS_CONFIG):
    """This function calls the appopriate output_format"""
    output_format = args['output_format']
    if output_format.upper() == "JSON":
        return output_json(data, WPS_CONFIG['output_dir'], \
                           WPS_CONFIG['server_output_dir'])
    elif output_format.upper() == "GEOJSON":
        return output_geojson(data, WPS_CONFIG['output_dir'], \
                              WPS_CONFIG['server_output_dir'])
    elif output_format.upper() == "NETCDF":
        if 'bbox' in args:
            bbox = args['bbox']
        else:
            bbox = [0,0,360,180]
        if 'res_x' in args:
            res_x = args['res_x']
        else:
            res_x = 1.0
        if 'res_y' in args:
            res_y = args['res_y']
        else:
            res_y = 1.0
        return output_netcdf(data, bbox, WPS_CONFIG, res_x, res_y)
    
def save_to_file(data, file_dir, prefix, extension):
    """ Save data to a file, returns it file name """
    file_name = '{pre}_{number}.{ext}'.format(directory = file_dir,\
                                                   pre = prefix, \
                                                   number = get_file_number(),\
                                                   ext = extension)
    path = '{directory}/{fname}'.format(directory = file_dir,\
                                        fname = file_name)
    with open(path, 'w') as fd:
        fd.write(data)
    return file_name
    
def convert_to_list(data):
    """Takes in the multi gridded data (x, y, and z separated) 
    and returns a list of tuples of corresponding (x, y, Z)
    """
    output_data = []
    for row in range(len(data[0])):
        for col in range (len(data[0][0])):
            output_data.append((data[0][row][col], data[1][row][col], 
            data[2][row][col]))
    return output_data

def output_json(data, output_dir, server_output_dir):
    """Convert the data in a JSON format, save it
    
    The format is [[lon, lat, den], [lon, lat, den], ...]
    
    The file is saved in output_dir
    The path returned is server_output_dir + filename
    """
    output_data = convert_to_list(data)
    output_data_json = json.dumps(output_data)
    file_name = save_to_file(output_data_json, \
                 output_dir, \
                 JSON_PREFIX, \
                 'json')
    server_path = '{server_out}/{fname}'.format(\
        server_out=server_output_dir, \
        fname = file_name)
    return server_path
    
def get_file_number():
    """ Returns a number consisting of the date and a random number """
    date = strftime("%Y%m%d%H%M%S",gmtime())
    random_number = int(random.random()*pow(10, RANDOM_NUMBER_DIGITS))
    return '{the_date}{the_random}'.format(the_date=date, \
                                           the_random=random_number)
    
def output_netcdf(data, bbox, WPS_CONFIG, res_x=1.0, res_y=1.0):
    """ Output the data in netCDF format, save it
    
    The file will have:
    3 dimensions: lon, lat, time
    4 variables: time, lon, lat, den (den is the density)
    The data values:
    For time, only one value (0)
    For lon, the values in data[0][0]
    For lat, the values from data[1][0][0] to data[1][-1][0]
    For den, the values in data[2][0] but with "Infinity" as a single value
    if the original value in data[2][0][x] is too low.
    
    The file is saved in WPS_CONFIG['netcdf_output_dir']
    The path returned is WPS_CONFIG['netcdf_server_dir'] + filename
    
    res_x = resolution for longitudes (between 0 and 1)
    res_y = resolution for latitudes (between 0 and 1)
    """
    import numpy as np
    import os
    # Get the dimensions requested
    longitude_dimension_length = round((bbox[2] - bbox[0])*(1/res_x))
    latitude_dimension_length = round((bbox[3] - bbox[1])*(1/res_y))
    # Create a netcdf file
    file_name = '{prefix}-{nb}.nc'.format(prefix=NETCDF_PREFIX, \
                                          nb=get_file_number())
    file_path = '{d}/{fname}'.format(d=WPS_CONFIG['netcdf_output_dir'], \
                                    fname=file_name)
    nc = netcdf.Dataset(file_path, 'w')
    try:
        # Create dimensions in the netcdf file
        # To comply with contour-wms, we must provide a time dimension:
        time_dim = nc.createDimension('time', 1)
        lon_dim = nc.createDimension('lon', longitude_dimension_length)
        lat_dim = nc.createDimension('lat', latitude_dimension_length)
        # Create variables in the netcdf file
        time_var = nc.createVariable('time', 'f8', ('time',))
        lon_var = nc.createVariable('lon', 'f8', ('lon',))
        lat_var = nc.createVariable('lat', 'f8', ('lat',))
        # contour-wms requires the variable mapped to be declared
        # depending on time, lat, lon in that order:
        den_var = nc.createVariable('den', 'f8', ('time','lat','lon',))
        den_var.valid_min = '0.0000'
        den_var.valid_max = '0.0004'
        # Add the data to the file
        time_var[:] = 0
        lon_var[:] = data[0][0]
        lat_var[:] = [data[1][i][0] for i in range(len(data[1]))]
        # Threshold low values so that the image can be transparent
        min_value_threshold = 0.0
        den_var.actual_min = min_value_threshold
        den_var.actual_max = np.amax(data[2])
        # In the netCDF file, values which we want to appear transparent
        # have the value "Infinity"
        low_values = data[2] <= min_value_threshold
        data[2][low_values] = np.inf
        den_var[:] = data[2]
        # The file is saved when it is closed.
        nc.close()
    except:
        import logging as log
        log.error('Error while creating a netcdf file')
        # If something went wrong, delete the file created.
        try:
            os.remove(file_path)
        except:
            log.error('Could not remove the netcdf file created! '
                      '({fp})\n'.format(fp=file_path))
        # Re-raise the exception
        raise
    # Return the path on the server
    server_path = '{server_out}/{fname}'.format(\
        server_out=WPS_CONFIG['netcdf_server_dir'], \
        fname = file_name)
    return server_path

def output_geojson(data, output_dir, server_output_dir):
    """ This will convert the (x, y, Z) gridded data and return GeoJSON
    compliant JSON, with corresponding (x,y) coords and density in the
    "density" dictionary. As it is storm fixes MultiPoint is used. 
    
    The file is saved in WPS_CONFIG['output_dir']
    The path returned is WPS_CONFIG['server_output_dir'] + filename
    """
    xym = []
    density = []
    for row in range(len(data[0])):
        for col in range (len(data[0][0])):
            xym.append((data[0][row][col], data[1][row][col]))
            density.append(data[2][row][col])    
    rv = {}
    rv["type"] = "MultiPoint"
    rv["coordinates"] = xym
    rv["density"] = density
    
    output_data_geojson = json.dumps(rv)
    file_name = save_to_file(output_data_geojson, \
                 output_dir, \
                 GEOJSON_PREFIX, \
                 'json')
    server_path = '{server_out}/{fname}'.format(\
        server_out=server_output_dir, \
        fname = file_name)
    return server_path
