import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import numpy as np
import unittest
import output as ot

class TestOutPut(unittest.TestCase):
	def test_output(self):
		xm = np.array([[ 62.5,  63.5,  64.5,  65.5,  66.5,  67.5],
		       [ 62.5,  63.5,  64.5,  65.5,  66.5,  67.5]])
		ym = np.array([[-9.5, -9.5, -9.5, -9.5, -9.5, -9.5],
		       [-8.5, -8.5, -8.5, -8.5, -8.5, -8.5]])
		Z = np.array([[ 0.0,  0.01005579,  0.0108799 ,  0.01107924,  0.01062354,
		         0.00959108],
		       [ 0.00771101,  0.01004376,  0.0108636 ,  0.01105884,  0.01059998,
		         0.00956597]])
		data = (xm, ym, Z)
		output=ot.convert_to_list(data)
		self.assertTrue(len(output)==12);
		for row in range(len(output)):
			self.assertTrue(len(output[row])==3)
		data2 = np.array([[62.5,-9.5,0.0],\
						  [63.5,-9.5,0.01005579],\
						  [64.5,-9.5,0.0108799],\
						  [65.5,-9.5,0.01107924],\
						  [66.5,-9.5,0.01062354],\
						  [67.5,-9.5,0.00959108],\
						  [62.5,-8.5,0.00771101],\
						  [63.5,-8.5,0.01004376],\
						  [64.5,-8.5,0.0108636],\
						  [65.5,-8.5,0.01105884],\
						  [66.5,-8.5,0.01059998],\
						  [67.5,-8.5,0.00956597]])
		for row in range(len(output)):
			for col in range(len(output[row])):
				self.assertTrue(output[row][col]==data2[row][col])
if __name__ == '__main__':
	unittest.main()
