"""
This module tests the output of the whole Track Density WPS process for all the
supported arguments.
Turn off caching before testing.
Run using
sudo -u www-data python Runtest.py
or
python Runtest.py
depending on permissions to access directory and files in the config file.
"""
# Make the input_checking module accessible from this subdirectory
import sys
import os.path
PATH_TO_TRACKDENSITY_MODULE_DIRECTORY = '../..'
PATH_TO_THIS_FILE_FROM_TRACKDENSITY_MODULE_DIRECTORY = 'modules/testing'
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), \
                    PATH_TO_TRACKDENSITY_MODULE_DIRECTORY)))

import TrackDensity as td
import unittest
conf = {'lenv': {'message': ''}}
out = {'Result': {'value' : ''}}

#Set arguments value
output_format = 'json'
hemisphere = 'N' 
year = '1991,1987'
month = '1,5,7'
year_range = '1980,1990'
month_range = '2,5'
date_list = '197612,197801,198201'
name = 'ALICE'
sbox = '1,-10,10,0'
bbox = '1,-10,10,0'
pressmin = '990'
pressmax = '1000'
genesis = 'True'
res_x = '0.5'
res_y = '0.5'
bandwidth = '2.0'


class TestWPSProcess(unittest.TestCase):
    """ Tests all the arguments for the WPS process """
    def setUp(self):
        # Change working directory so we don't have problem with relative
        # file paths (config file, output directories, ...)
        os.chdir(PATH_TO_TRACKDENSITY_MODULE_DIRECTORY)
        
    def tearDown(self):
        # Come back to the directory
        os.chdir(PATH_TO_THIS_FILE_FROM_TRACKDENSITY_MODULE_DIRECTORY)

    def test_filter_year(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'year': {'value': year}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)
            
    def test_filter_month(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'year': {'value': year},
                  'month': {'value': month}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)

    def test_filter_year_range(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'year_range': {'value': year_range}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)
        
    def test_filter_month_range(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'year_range': {'value': year_range},
                  'month_range': {'value': month_range}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)
        
    def test_filter_date_list(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'date_list': {'value': date_list}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)

    def test_filter_name(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'name': {'value': name}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)
        
    def test_filter_sbox(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'sbox': {'value': sbox}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)

    def test_filter_bbox(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'bbox': {'value': bbox}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)

    def test_filter_pressmin(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'pressmin': {'value': pressmin}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)

    def test_filter_pressmax(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'pressmax': {'value': pressmax}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)

    def test_filter_press(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'pressmin': {'value': pressmin},
                  'pressmax': {'value': pressmax}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)

    def test_filter_genesis(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'genesis': {'value': genesis}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)

    def test_filter_res_x(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'res_x': {'value': res_x}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)

    def test_filter_res_y(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'res_y': {'value': res_y}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)

    def test_filter_res(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'res_x': {'value': res_x},
                  'res_y': {'value': res_y}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)
        
    def test_filter_bandwidth(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'bandwidth': {'value': bandwidth}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)

    def test_output_format(self):
        inputs = {'hemisphere': {'value': hemisphere},
                  'output_format': {'value': output_format}}	
        expected_output = 3
        actual_output = td.TrackDensity(conf,inputs,out)
        self.assertEqual(expected_output, actual_output)
     
if __name__ == '__main__':
    unittest.main()
