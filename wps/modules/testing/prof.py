"""
This module does profiling for the WPS
The file 'result.txt' will be created, it will contain the output of this module.
Run using
sudo -u www-data python prof.py
or
python prof.py
depending on permissions to access directory and files in the config file.
(You also need to have the permission to write file in the directory this file
 is in)
"""
# Make the input_checking module accessible from this subdirectory
import sys
import os
# Modify this if the file is moved to another directory
PATH_TO_TRACKDENSITY_MODULE_DIRECTORY = '../..'
PATH_TO_THIS_DIR_FROM_TRACKDENSITY_MODULE_DIR = 'modules/testing'
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), \
                    PATH_TO_TRACKDENSITY_MODULE_DIRECTORY)))
import TrackDensity as td
import pstats, cProfile

# Change working directory so we don't have problem with relative
# file paths (config file, output directories, ...)
os.chdir(PATH_TO_TRACKDENSITY_MODULE_DIRECTORY)

PROFILE_FILE_PATH = PATH_TO_THIS_DIR_FROM_TRACKDENSITY_MODULE_DIR + \
                    "/Profile.prof"
RESULTS_FILE_PATH = PATH_TO_THIS_DIR_FROM_TRACKDENSITY_MODULE_DIR + \
                    "/results.txt"

# Track Density process arguments
conf = {'lenv': {'message': ''}}
inputs = {'hemisphere': {'value': 's'}, 'output_format': {'value': 'NetCDF'}}
out = {'Result': {'value': ''}}

stream = open(RESULTS_FILE_PATH, 'w')

cProfile.runctx("td.TrackDensity(conf, inputs, out)", globals(), locals(), \
                PROFILE_FILE_PATH)

s = pstats.Stats(PROFILE_FILE_PATH, stream=stream)
s.strip_dirs().sort_stats("time").print_stats()

# Remove profile file:
os.remove(PROFILE_FILE_PATH)
# Come back to the directory
os.chdir(PATH_TO_THIS_DIR_FROM_TRACKDENSITY_MODULE_DIR)
