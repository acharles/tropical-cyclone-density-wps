import ConfigParser
import os
import ntpath


CONFIG_FILE_PATH = 'TrackDensityConfig.ini'


def check_directory(dir_path):
    """ Check that a directory exists and that we can create files in it. """
    if os.path.isdir(dir_path):
        if os.access(dir_path, os.W_OK & os.R_OK):
            return
        else:
            raise IOError(dir_path + \
                          ' is not readable or writable (change permissions).')
    else:
        raise IOError(dir_path + ' is not a directory.')
    
def check_file(file_path):
    """ Check that a file exists or that we can create it. """
    if os.path.isfile(file_path):
        if os.access(file_path, os.W_OK):
            return
        else:
            raise IOError(file_path + ' is not a writeable.')
    else:
        if os.path.isdir(file_path):
            raise IOError(file_path + 
                          ' is a directory path (expected a file path).')
        else:
            log_dir = ntpath.dirname(file_path)
            check_directory(log_dir)
            return

def no_backslash(path):
    """ Remove a backslash at the end if there is one """
    if path.endswith('/'):
        return path[:-1]
    return path

def load_config():
    """Loads configuration arguments in a dictionary.
    
    Returns a dictionary containing each argument supported by the process 
    from the  config file.
    Will throw an exception if something goes wrong.
    """
    wps_config = {}
    
    try:
        config = ConfigParser.RawConfigParser()
        config.read(CONFIG_FILE_PATH)
        
        nod = config.get('paths', 'netcdf_output_dir')
        wps_config['netcdf_output_dir'] = no_backslash(nod)
        check_directory(wps_config['netcdf_output_dir'])
        nsd = config.get('paths', 'netcdf_server_dir')
        wps_config['netcdf_server_dir'] = no_backslash(nsd)
        od = config.get('paths', 'output_dir')
        wps_config['output_dir'] = no_backslash(od)
        check_directory(wps_config['output_dir'])
        sod = config.get('paths', 'server_output_dir')
        wps_config['server_output_dir'] = no_backslash(sod)
        
        wps_config['use_database'] = config.getboolean('database', 'use_database')
        wps_config['database_name'] = config.get('database', 'database_name')
        wps_config['database_user'] = config.get('database', 'database_user')
        wps_config['database_password'] = config.get('database', \
                                                     'database_password')
        wps_config['database_table'] = config.get('database', 'database_table')
        
        wps_config['use_caching'] = config.getboolean('caching', 'use_caching')
        cache_dir = config.get('caching', 'cache_directory')
        wps_config['cache_directory'] = no_backslash(cache_dir)
        check_directory(wps_config['cache_directory'])
        
        wps_config['debug'] = config.getboolean('general', 'debug')
        wps_config['log_file'] = config.get('general', 'log_file')
        check_file(wps_config['log_file'])
        
        wps_config['use_cython'] = config.getboolean('processing', 'use_cython')
        wps_config['density'] = config.get('processing', 'density')
        if wps_config['density'] != 'smooth' and wps_config['density'] != 'scipy':
            raise ValueError("The fix processing method is not recognised: " +
                             wps_config['density'] + 
                             "it should be 'smooth' or 'scipy'.")
        wps_config['kernel'] = config.get('processing','kernel')
        if wps_config['kernel'] != 'lucy' and wps_config['kernel'] != 'gauss':
            raise ValueError("The kernel is not recognised: " +
                             wps_config['kernel'] + 
                             " it should be 'lucy' or 'gauss'.")
        
    except BaseException as exc:
        import sys
        # Preserve the original exception and add info:
        raise type(exc)('Error when loading the config file: ' + exc.message)
    
    return wps_config
