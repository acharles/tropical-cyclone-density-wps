'''
Created on 29/08/2013

@author: s3299824
'''
# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestYear(unittest.TestCase):
    """Methods testing different inputs of year"""

    def test_valid_year(self):
        params, input = {}, '1970'
        self.assertTrue(\
        ic.validate_year(input, params) is None)
    
    def test_valid_year_list(self):
        params, input = {}, '1970,1987,2012,2022'
        self.assertTrue(\
        ic.validate_year(input, params) is None)
        
    def test_incorrect_year_format(self):
        params, input = {}, '193^'
        self.assertRaises(\
        ValueError, ic.validate_year, input, params)
        
        # This is valid
        #params, input = {}, '0193'
        #self.assertRaises(\
        #ValueError, ic.validate_year, input, params)
        
        params, input = {}, '10193'
        self.assertRaises(\
        ValueError, ic.validate_year, input, params)
        
    def test_incorrect_year_list_format(self):
        params, input = {}, '1970.1987.2012,,,2022'
        self.assertRaises(\
        ValueError, ic.validate_year, input, params)
        
        params, input = {}, '11970,1987,2012,2022'
        self.assertRaises(\
        ValueError, ic.validate_year, input, params)
        
    def test_correct_input(self):
        """Test that the output is the one expected"""
        params, input = {}, '1970'
        expected_output = {'year': [1970]}
        ic.validate_year(input, params)
        self.assertEqual(expected_output, params)
        
        params, input = {}, '1970,2000'
        expected_output = {'year': [1970,2000]}
        ic.validate_year(input, params)
        self.assertEqual(expected_output, params)
        
if __name__ == '__main__':
        unittest.main()
    
