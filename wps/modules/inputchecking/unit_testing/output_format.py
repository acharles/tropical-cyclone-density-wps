'''
Created on 29/08/2013

@author: s3299824
'''
# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestOutputformat(unittest.TestCase):
    """Functions testing different inputs of output_format"""
        
    def test_valid_output_format(self):
        params, input = {}, 'JSON'
        self.assertTrue(\
        ic.validate_output_format(input, params) is None)
        
        params, input = {}, 'json'
        self.assertTrue(\
        ic.validate_output_format(input, params) is None)
        
        params, input = {}, 'netcdf'
        self.assertTrue(\
        ic.validate_output_format(input, params) is None)
        
        params, input = {}, 'geojson'
        self.assertTrue(\
        ic.validate_output_format(input, params) is None)
        
    def test_incorrect_output_format(self):
        params, input = {}, 'badformat'
        self.assertRaises(\
        ValueError, ic.validate_output_format, input, params)
        
        params, input = {}, ''
        self.assertRaises(\
        ValueError, ic.validate_output_format, input, params)
        
    def test_correct_input(self):
        """Test that the output is the one expected"""
        params, input = {}, 'JSON'
        expected_output = {'output_format': 'JSON'}
        ic.validate_output_format(input, params)
        self.assertEqual(expected_output, params)

      
if __name__ == '__main__':
        unittest.main()
