# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestMonth(unittest.TestCase):
    """Methods testing different inputs of month"""

    def test_valid_month(self):
        params, input = {}, '1'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '2'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '3'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '4'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '5'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '6'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '7'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '8'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '9'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '10'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '11'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '12'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        
        
        params, input = {}, '1,2'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '5,7,10'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        params, input = {}, '10,3,5'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
        
        
        params, input = {}, '1,1,1,2'
        self.assertTrue(\
        ic.validate_month(input, params) is None)
    
    def test_incorrect_value(self):
        params, input = {}, '-1'
        self.assertRaises(\
        ValueError, ic.validate_month, input, params)
        
        params, input = {}, '13'
        self.assertRaises(\
        ValueError, ic.validate_month, input, params)
        
        params, input = {}, '100'
        self.assertRaises(\
        ValueError, ic.validate_month, input, params)
    
    def test_incorrect_format(self):
        params, input = {}, ''
        self.assertRaises(\
        ValueError, ic.validate_month, input, params)
        
        params, input = {}, 'janurary'
        self.assertRaises(\
        ValueError, ic.validate_month, input, params)
        
        params, input = {}, 'one'
        self.assertRaises(\
        ValueError, ic.validate_month, input, params)
        
        params, input = {}, '1,two,3'
        self.assertRaises(\
        ValueError, ic.validate_month, input, params)
        
        params, input = {}, ','
        self.assertRaises(\
        ValueError, ic.validate_month, input, params)
        
        params, input = {}, ','
        self.assertRaises(\
        ValueError, ic.validate_month, input, params)

    def test_correct_input(self):
        """Test that the output is the one expected"""
        params, input = {}, '3'
        expected_output = {'month': [3]}
        ic.validate_month(input, params)
        self.assertEqual(expected_output, params)
        
        params, input = {}, '2,4,6'
        expected_output = {'month': [2,4,6]}
        ic.validate_month(input, params)
        self.assertEqual(expected_output, params)
        
        
if __name__ == '__main__':
        unittest.main()
