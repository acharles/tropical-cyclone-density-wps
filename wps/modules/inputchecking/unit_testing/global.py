# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestModule(unittest.TestCase):
    """This class tests that the output is the one expected"""

    def test_single_year(self):
        # Normal input: year and month
        input = {'year': {'value': '1987'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'hemisphere': {'value': 'S'},\
                 'output_format': {'value': 'json'},\
                 'month': {'value': '2,6,7'}}
        expected_output = {'year': [1987],\
                           'bbox': [120.0,-30.0,140.0,0.0],\
                           'pressmin': 940.0,\
                           'hemisphere': 'S',\
                           'output_format': 'JSON',\
                           'month': [2,6,7],\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        # Normal input: year without month
        input = {'year': {'value': '1987'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'output_format': {'value': 'json'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'year': [1987],\
                           'bbox': [120.0,-30.0,140.0,0.0],\
                           'pressmin': 940.0,\
                           'output_format': 'JSON',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        # year with month_range:
        input = {'year': {'value': '1987'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'hemisphere': {'value': 'S'},\
                 'output_format': {'value': 'json'},\
                 'month_range': {'value': '4,9'}}
        self.assertRaises(ValueError, ic.check_arguments, input)
        # year with month and month_range
        input = {'year': {'value': '1987'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'hemisphere': {'value': 'S'},\
                 'output_format': {'value': 'json'},\
                 'month_range': {'value': '4,9'},\
                 'month': {'value': '2,6,7'}}
        self.assertRaises(ValueError, ic.check_arguments, input)
        
    def test_year_and_year_range(self):
        # If there is a year and a year_range specified,
        # an exception should be raised
        input = {'year': {'value': '1987'},\
                 'year_range': {'value': '1987,1989'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'output_format': {'value': 'json'},\
                 'hemisphere': {'value': 'S'}}
        self.assertRaises(ValueError, ic.check_arguments, input)
        
    def test_year_and_date_list(self):
        # If there is a year and a date_list specified,
        # an exception should be raised
        input = {'year': {'value': '1987'},\
                 'date_list': {'value': '198702'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'output_format': {'value': 'json'},\
                 'hemisphere': {'value': 'S'}}
        self.assertRaises(ValueError, ic.check_arguments, input)
        
    def test_year_range_and_date_list(self):
        # If there is a year_range and a date_list specified,
        # an exception should be raised
        input = {'year_range': {'value': '1987,1989'},\
                 'date_list': {'value': '198702'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'output_format': {'value': 'json'},\
                 'hemisphere': {'value': 'S'}}
        self.assertRaises(ValueError, ic.check_arguments, input)

    def test_year_list(self):
        # Normal input: year list
        input = {'year': {'value': '1982,1984,1988'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'output_format': {'value': 'json'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'year': [1982,1984,1988],\
                           'bbox': [120.0,-30.0,140.0,0.0],\
                           'pressmin': 940.0,\
                           'output_format': 'JSON',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        
        # Normal input: year list and month
        input = {'year': {'value': '1982,1984,1988'},\
                 'month': {'value': '2,3,9'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'output_format': {'value': 'json'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'year': [1982,1984,1988],\
                           'bbox': [120.0,-30.0,140.0,0.0],\
                           'pressmin': 940.0,\
                           'output_format': 'JSON',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        self.assertEqual(actual_output, expected_output)
        
        # year list and month_range: fail because no year_range for month_range
        input = {'year': {'value': '1982,1984,1988'},\
                 'month_range': {'value': '2,9'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'output_format': {'value': 'json'},\
                 'hemisphere': {'value': 'S'}}
        self.assertRaises(ValueError, ic.check_arguments, input)
        
    def test_year_range(self):
        # Normal input: without month_range
        input = {'year_range': {'value': '1987,1989'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'output_format': {'value': 'json'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'year_range': [1987,1989],\
                           'bbox': [120.0,-30.0,140.0,0.0],\
                           'pressmin': 940.0,\
                           'output_format': 'JSON',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        
        # Normal input: with month_range
        input = {'year_range': {'value': '1987,1989'},\
                 'month_range': {'value': '4,7'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'output_format': {'value': 'json'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'year_range': [1987,1989],\
                           'month_range': [4,7],\
                           'bbox': [120.0,-30.0,140.0,0.0],\
                           'pressmin': 940.0,\
                           'output_format': 'JSON',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        
        # year_range and month: fail because no year for month
        input = {'year_range': {'value': '1987,1989'},\
                 'month': {'value': '4,7'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'output_format': {'value': 'json'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'year_range': [1987,1989],\
                           'bbox': [120.0,-30.0,140.0,0.0],\
                           'pressmin': 940.0,\
                           'output_format': 'JSON',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        self.assertRaises(ValueError, ic.check_arguments, input)
        
    def test_unexpected_parameters(self):
        # Error when providing unknown arguments
        input = {'year': {'value': '1987'},\
                 'food': {'value': 'bread'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'pressmin': {'value': '940'},\
                 'output_format': {'value': 'json'},\
                 'hemisphere': {'value': 'S'}}
        self.assertRaises(ValueError, ic.check_arguments, input)
        
    def test_missing_argument(self):
        # Missing hemisphere
        input = {'year': {'value': '1987'},\
                 'bbox': {'value': '120,-30,140,0'},\
                 'output_format': {'value': 'json'},\
                 'pressmin': {'value': '940'}}
        self.assertRaises(ValueError, ic.check_arguments, input)
        # Month without year
        input = {'month': {'value': '07'},\
                 'output_format': {'value': 'json'},\
                 'pressmin': {'value': '940'}}
        self.assertRaises(ValueError, ic.check_arguments, input)
        # month_range without year_range
        input = {'month_range': {'value': '07,09'},\
                 'output_format': {'value': 'json'},\
                 'pressmin': {'value': '940'}}
        self.assertRaises(ValueError, ic.check_arguments, input)
        
    def test_bbox_and_sbox(self):
        # Valid input:
        input = {'bbox': {'value': '100,-50,150,0'},\
                 'sbox': {'value': '110,-40,140,-10'},\
                 'hemisphere': {'value': 'S'},\
                 'output_format': {'value': 'json'}}
        expected_output = {'bbox': [100.0,-50.0,150.0,0.0],\
                           'sbox': [110.0,-40.0,140.0,-10.0],\
                           'hemisphere': 'S',\
                           'output_format': 'JSON',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        
        # Valid input: sbox == bbox
        input = {'bbox': {'value': '100,-50,150,0'},\
                 'sbox': {'value': '100,-50,150,0'},\
                 'hemisphere': {'value': 'S'},\
                 'output_format': {'value': 'json'}}
        expected_output = {'bbox': [100.0,-50.0,150.0,0.0],\
                           'sbox': [100.0,-50.0,150.0,0.0],\
                           'hemisphere': 'S',\
                           'output_format': 'JSON',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        
    def test_unique_arguments(self):
        # output_format is added with the default value when it is not
        # in the input
        # year
        input = {'year': {'value': '1987'}, 'hemisphere': {'value':'S'}}
        expected_output = {'year': [1987],\
                           'output_format': 'NETCDF',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        # year range
        input = {'year_range': {'value': '1987,1998'},\
                 'hemisphere': {'value':'S'}}
        expected_output = {'year_range': [1987,1998],\
                           'output_format': 'NETCDF',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        # bbox
        input = {'bbox': {'value': '120,-30,140,0'},\
                 'hemisphere': {'value':'S'}}
        expected_output = {'bbox': [120.0,-30.0,140.0,0.0],\
                           'output_format': 'NETCDF',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        # sbox
        input = {'sbox': {'value': '100,-50,150,0'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'hemisphere': 'S',\
                           'sbox': [100.0,-50.0,150.0,0.0],\
                           'output_format': 'NETCDF',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        # name TODO remove
        """
        input = {'name': {'value': 'HENRY'},\
                 'hemisphere': {'value':'S'}}
        expected_output = {'name': 'HENRY',\
                           'output_format': 'NETCDF',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        """
        # pressmin
        input = {'pressmin': {'value': '940'},\
                 'hemisphere': {'value':'S'}}
        expected_output = {'pressmin': 940.0,\
                           'output_format': 'NETCDF',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        # pressmax
        input = {'pressmax': {'value': '970'},\
                 'hemisphere': {'value':'S'}}
        expected_output = {'pressmax': 970.0,\
                           'output_format': 'NETCDF',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        # hemisphere
        input = {'hemisphere': {'value':'S'}}
        expected_output = {'output_format': 'NETCDF', 'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        # output_format
        input = {'output_format': {'value': 'netcdf'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'output_format': 'NETCDF', 'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        # month
        # Need a single year for month
        input = {'year': {'value': '1987'}, 'month': {'value':'4'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'year': [1987], 'month': [4],\
                           'output_format': 'NETCDF',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        # month_range
        # Need a year_range for month_range
        input = {'year_range': {'value': '1987,1998'},\
                 'month_range': {'value':'1,5'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'year_range': [1987,1998],\
                           'month_range': [1,5],\
                           'output_format': 'NETCDF',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        #datelist
        input = {'date_list': {'value': '198702'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'date_list': [(1987,2)],\
                           'output_format': 'NETCDF',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        #genesis
        input = {'genesis': {'value': 'True'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'output_format': 'NETCDF',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': True}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)
        #bandwidth
        input = {'bandwidth': {'value': '3.0'},\
                 'hemisphere': {'value': 'S'}}
        expected_output = {'output_format': 'NETCDF',\
                           'hemisphere': 'S',\
                           'bandwidth': 3.0,\
                           'genesis': False}
        actual_output=ic.check_arguments(input)
        self.assertEqual(actual_output, expected_output)

if __name__ == '__main__':
        unittest.main()
