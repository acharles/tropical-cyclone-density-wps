# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestPressure(unittest.TestCase):
    """Methods testing different inputs of pressure"""

    def test_valid_pressmin(self):
        params, input = {}, '971'
        self.assertTrue(\
        ic.validate_pressmin(input, params) is None)

    def test_valid_pressmax(self):
        params, input = {}, '995'
        self.assertTrue(\
        ic.validate_pressmax(input, params) is None)
        
    def test_incorrect_pressmin_format(self):
        params, input = {}, 'ABC'
        self.assertRaises(\
        ValueError, ic.validate_pressmin, input, params)
        
        params, input = {}, ''
        self.assertRaises(\
        ValueError, ic.validate_pressmin, input, params)

    def test_incorrect_pressmax_format(self):
        params, input = {}, 'ABC'
        self.assertRaises(\
        ValueError, ic.validate_pressmax, input, params)
        
        params, input = {}, ''
        self.assertRaises(\
        ValueError, ic.validate_pressmax, input, params)
        
    def test_correct_input(self):
        """Test that the output is the one expected"""
        params, input = {}, '995'
        expected_output = {'pressmin': 995}
        ic.validate_pressmin(input, params)
        self.assertEqual(expected_output, params)
        
        params, input = {}, '995'
        expected_output = {'pressmax': 995}
        ic.validate_pressmax(input, params)
        self.assertEqual(expected_output, params)
        
if __name__ == '__main__':
        unittest.main()
