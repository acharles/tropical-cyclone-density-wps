# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestHemisphere(unittest.TestCase):
    """Methods testing different inputs of hemisphere"""

    def test_valid_hemisphere(self):
        params, input = {}, 'N'
        self.assertTrue(\
        ic.validate_hemisphere(input, params) is None)
        
        params, input = {}, 'S'
        self.assertTrue(\
        ic.validate_hemisphere(input, params) is None)
        
    def test_incorrect_hemisphere_value(self):
        params, input = {}, 'E'
        self.assertRaises(\
        ValueError, ic.validate_hemisphere, input, params)
        
        params, input = {}, 'W'
        self.assertRaises(\
        ValueError, ic.validate_hemisphere, input, params)
        
        params, input = {}, ''
        self.assertRaises(\
        ValueError, ic.validate_hemisphere, input, params)
        
        params, input = {}, 'North'
        self.assertRaises(\
        ValueError, ic.validate_hemisphere, input, params)
        
        params, input = {}, ' S'
        self.assertRaises(\
        ValueError, ic.validate_hemisphere, input, params)
        
        params, input = {}, 'N,S'
        self.assertRaises(\
        ValueError, ic.validate_hemisphere, input, params)

    def test_correct_input(self):
        """Test that the output is the one expected"""
        input = 'N'
        expected_output = {'hemisphere': 'N'}
        params = {}
        ic.validate_hemisphere(input, params)
        self.assertEqual(expected_output, params)
        
        params, input = {}, 'S'
        expected_output = {'hemisphere': 'S'}
        ic.validate_hemisphere(input, params)
        self.assertEqual(expected_output, params)
        
if __name__ == '__main__':
        unittest.main()
