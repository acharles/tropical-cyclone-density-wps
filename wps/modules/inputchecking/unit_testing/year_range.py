# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestYearRange(unittest.TestCase):
    """Methods testing different inputs of year_range"""

    def test_valid_input(self):
        params, input = {}, '1990,1995'
        self.assertTrue(\
        ic.validate_year_range(input, params) is None)

        params, input = {}, '1970,2000'
        self.assertTrue(\
        ic.validate_year_range(input, params) is None)
        
    def test_incorrect_year_order(self):
        params, input = {}, '1990,1989'
        self.assertRaises(\
        ValueError, ic.validate_year_range, input, params)
        
    def test_same_year(self):
        params, input = {}, '2000,2000'
        self.assertRaises(\
        ValueError, ic.validate_year_range, input, params)

    def test_not_enough_years(self):
        params, input = {}, '2000'
        self.assertRaises(\
        ValueError, ic.validate_year_range, input, params)

    def test_too_many_years(self):
        params, input = {}, '2000,2001,2002'
        self.assertRaises(\
        ValueError, ic.validate_year_range, input, params)
     
    def test_not_a_year(self):
        params, input = {}, 'hello'
        self.assertRaises(\
        ValueError, ic.validate_year_range, input, params)
        
        params, input = {}, 'hello,hi'
        self.assertRaises(\
        ValueError, ic.validate_year_range, input, params)
        
        params, input = {}, 'hello,hi,hello'
        self.assertRaises(\
        ValueError, ic.validate_year_range, input, params)
        
        params, input = {}, '2005,dogs'
        self.assertRaises(\
        ValueError, ic.validate_year_range, input, params)
        
        params, input = {}, 'dogs,2005'
        self.assertRaises(\
        ValueError, ic.validate_year_range, input, params)
        
    def test_incorrect_format(self):
        params, input = {}, '2004, 2006'
        self.assertRaises(\
        ValueError, ic.validate_year_range, input, params)
        
        params, input = {}, '2004-2006'
        self.assertRaises(\
        ValueError, ic.validate_year_range, input, params)
        
    def test_correct_input(self):
        """Test that the output is the one expected"""
        params, input = {}, '2000,2006'
        expected_output = {'year_range': [2000,2006]}
        ic.validate_year_range(input, params)
        self.assertEqual(expected_output, params)
        
if __name__ == '__main__':
        unittest.main()
