# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestMonthRange(unittest.TestCase):
    """Methods testing different inputs of month_range"""

    def setUp(self):
        self.is_required = False

    def test_valid_month_range(self):
        params, input = {}, '1,2'
        self.assertTrue(\
        ic.validate_month_range(input, params) is None)
        
        params, input = {}, '5,12'
        self.assertTrue(\
        ic.validate_month_range(input, params) is None)
        
        params, input = {}, '10,1'
        self.assertTrue(\
        ic.validate_month_range(input, params) is None)
        
    def test_incorrect_month_range(self):
        params, input = {}, '-1,5'
        self.assertRaises(\
        ValueError, ic.validate_month_range, input, params)
        
        params, input = {}, '13,5'
        self.assertRaises(\
        ValueError, ic.validate_month_range, input, params)
        
        params, input = {}, '1,-5'
        self.assertRaises(\
        ValueError, ic.validate_month_range, input, params)
        
        params, input = {}, '2,13'
        self.assertRaises(\
        ValueError, ic.validate_month_range, input, params)
        
    def test_incorrect_month_format(self):
        params, input = {}, ''
        self.assertRaises(\
        ValueError, ic.validate_month_range, input, params)
        params, input = {}, ','
        self.assertRaises(\
        ValueError, ic.validate_month_range, input, params)
        params, input = {}, 'hello'
        self.assertRaises(\
        ValueError, ic.validate_month_range, input, params)
        params, input = {}, '1,hi'
        self.assertRaises(\
        ValueError, ic.validate_month_range, input, params)
        params, input = {}, 'hi,1'
        self.assertRaises(\
        ValueError, ic.validate_month_range, input, params)
        params, input = {}, '1,2,2'
        self.assertRaises(\
        ValueError, ic.validate_month_range, input, params)
        params, input = {}, '1'
        self.assertRaises(\
        ValueError, ic.validate_month_range, input, params)

    def test_correct_input(self):
        """Test that the output is correct"""
        params, input = {}, '1,12'
        expected_output = {'month_range': [1,12]}
        ic.validate_month_range(input, params)
        self.assertEqual(expected_output, params)
        
        
if __name__ == '__main__':
        unittest.main()
