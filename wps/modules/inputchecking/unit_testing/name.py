# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestName(unittest.TestCase):
    """Methods testing different inputs of name"""

    def test_valid_name(self):
        params, input = {}, 'BLANCHE'
        self.assertTrue(\
        ic.validate_name(input, params) is None)

    def test_incorrect_name(self):
        params, input = {}, ''
        self.assertRaises(\
        ValueError, ic.validate_name, input, params)
        
        params, input = {}, '%@'
        self.assertRaises(\
        ValueError, ic.validate_name, input, params)
        
        params, input = {}, ' '
        self.assertRaises(\
        ValueError, ic.validate_name, input, params)
        
    def test_correct_input(self):
        """Test that the output is the one expected"""
        params, input = {}, 'FRANK'
        expected_output = {'name': 'FRANK'}
        ic.validate_name(input, params)
        self.assertEqual(expected_output, params)
        
if __name__ == '__main__':
        unittest.main()
