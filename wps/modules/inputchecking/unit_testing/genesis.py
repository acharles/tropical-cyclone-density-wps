# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestGenesis(unittest.TestCase):
    """Functions testing different inputs of output_format"""
        
    def test_valid_genesis(self):
        params, input = {}, 'TRUE'
        self.assertTrue(\
        ic.validate_genesis(input, params) is None)
        
        params, input = {}, 'FALSE'
        self.assertTrue(\
        ic.validate_genesis(input, params) is None)
        
        params, input = {}, 'true'
        self.assertTrue(\
        ic.validate_genesis(input, params) is None)
        
        params, input = {}, 'false'
        self.assertTrue(\
        ic.validate_genesis(input, params) is None)
        
    def test_incorrect_genesis(self):
        params, input = {}, ''
        self.assertRaises(\
        ValueError, ic.validate_genesis, input, params)
        
        params, input = {}, 'camel'
        self.assertRaises(\
        ValueError, ic.validate_genesis, input, params)
        
    def test_correct_input(self):
        """Test that the output is the one expected"""
        params, input = {}, 'TRUE'
        expected_output = {'genesis': True}
        ic.validate_genesis(input, params)
        self.assertEqual(expected_output, params)
        
        params, input = {}, 'FALSE'
        expected_output = {'genesis': False}
        ic.validate_genesis(input, params)
        self.assertEqual(expected_output, params)

      
if __name__ == '__main__':
        unittest.main()
