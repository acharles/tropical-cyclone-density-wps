# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestRes(unittest.TestCase):
    """Methods testing different inputs of name"""

    def test_valid_res(self):
        params, res_x = {}, '0.1'
        self.assertTrue(\
        ic.validate_res_x(res_x, params) is None)
        params, res_x = {}, '0.3'
        self.assertTrue(\
        ic.validate_res_x(res_x, params) is None)
        params, res_x = {}, '0.45'
        self.assertTrue(\
        ic.validate_res_x(res_x, params) is None)
        params, res_x = {}, '0.6789'
        self.assertTrue(\
        ic.validate_res_x(res_x, params) is None)
        params, res_x = {}, '0.9'
        self.assertTrue(\
        ic.validate_res_x(res_x, params) is None)
        params, res_x = {}, '1.0'
        self.assertTrue(\
        ic.validate_res_x(res_x, params) is None)
        
        params, res_y = {}, '0.1'
        self.assertTrue(\
        ic.validate_res_y(res_y, params) is None)
        params, res_y = {}, '0.3'
        self.assertTrue(\
        ic.validate_res_y(res_y, params) is None)
        params, res_y = {}, '0.45'
        self.assertTrue(\
        ic.validate_res_y(res_y, params) is None)
        params, res_y = {}, '0.6789'
        self.assertTrue(\
        ic.validate_res_y(res_y, params) is None)
        params, res_y = {}, '0.9'
        self.assertTrue(\
        ic.validate_res_y(res_y, params) is None)
        params, res_y = {}, '1.0'
        self.assertTrue(\
        ic.validate_res_y(res_y, params) is None)
        
        

    def test_incorrect_res_value(self):
        params, res_x = {}, '-1'
        self.assertRaises(\
        ValueError, ic.validate_res_x, res_x, params)
        params, res_x = {}, '1.2'
        self.assertRaises(\
        ValueError, ic.validate_res_x, res_x, params)
        params, res_x = {}, '1.001'
        self.assertRaises(\
        ValueError, ic.validate_res_x, res_x, params)
        
        params, res_y = {}, '-1'
        self.assertRaises(\
        ValueError, ic.validate_res_y, res_y, params)
        params, res_y = {}, '1.2'
        self.assertRaises(\
        ValueError, ic.validate_res_y, res_y, params)
        params, res_y = {}, '1.001'
        self.assertRaises(\
        ValueError, ic.validate_res_y, res_y, params)
        
    def test_incorrect_res_format(self):
        params, res_x = {}, 'cat'
        self.assertRaises(\
        ValueError, ic.validate_res_x, res_x, params)
        params, res_x = {}, ''
        self.assertRaises(\
        ValueError, ic.validate_res_x, res_x, params)
        params, res_x = {}, '0x6'
        self.assertRaises(\
        ValueError, ic.validate_res_x, res_x, params)
    
        params, res_y = {}, 'cat'
        self.assertRaises(\
        ValueError, ic.validate_res_y, res_y, params)
        params, res_y = {}, ''
        self.assertRaises(\
        ValueError, ic.validate_res_y, res_y, params)
        params, res_y = {}, '0x6'
        self.assertRaises(\
        ValueError, ic.validate_res_y, res_y, params)
        
    def test_correct_res(self):
        """ Test that the output is the one expected """
        params, res_x = {}, '0.2'
        expected_output = {'res_x': 0.2}
        ic.validate_res_x(res_x, params)
        self.assertEqual(expected_output, params)
        
        params, res_y = {}, '0.2'
        expected_output = {'res_y': 0.2}
        ic.validate_res_y(res_y, params)
        self.assertEqual(expected_output, params)
        
if __name__ == '__main__':
        unittest.main()
