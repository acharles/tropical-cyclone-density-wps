# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestBandwidth(unittest.TestCase):
    """Functions testing different inputs of output_format"""
        
    def test_valid_bandwidth(self):
        params, input = {}, '1.0'
        self.assertTrue(\
        ic.validate_bandwidth(input, params) is None)
        
        params, input = {}, '1'
        self.assertTrue(\
        ic.validate_bandwidth(input, params) is None)
        
        params, input = {}, '5'
        self.assertTrue(\
        ic.validate_bandwidth(input, params) is None)
        
        params, input = {}, '2.5'
        self.assertTrue(\
        ic.validate_bandwidth(input, params) is None)
        
    def test_incorrect_bandwidth(self):
        params, input = {}, ''
        self.assertRaises(\
        ValueError, ic.validate_bandwidth, input, params)
        
        params, input = {}, 'camel'
        self.assertRaises(\
        ValueError, ic.validate_bandwidth, input, params)

        params, input = {}, '6'
        self.assertRaises(\
        ValueError, ic.validate_bandwidth, input, params)

        params, input = {}, '0.2'
        self.assertRaises(\
        ValueError, ic.validate_bandwidth, input, params)

        
    def test_correct_input(self):
        """Test that the output is the one expected"""
        params, input = {}, '3'
        expected_output = {'bandwidth': 3.0}
        ic.validate_bandwidth(input, params)
        self.assertEqual(expected_output, params)
        
        params, input = {}, '4'
        expected_output = {'bandwidth': 4.0}
        ic.validate_bandwidth(input, params)
        self.assertEqual(expected_output, params)

      
if __name__ == '__main__':
        unittest.main()
