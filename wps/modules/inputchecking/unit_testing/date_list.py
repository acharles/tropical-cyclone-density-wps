# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestDateList(unittest.TestCase):
    """Functions testing different inputs of date_list"""
        
    def test_valid_date_list(self):
        params, input = {}, '196901'
        self.assertTrue(\
        ic.validate_date_list(input, params) is None)
        
        params, input = {}, '196901,200005'
        self.assertTrue(\
        ic.validate_date_list(input, params) is None)
        
        params, input = {}, '200802'
        self.assertTrue(\
        ic.validate_date_list(input, params) is None)
        
        params, input = {}, '200802,197006,199604'
        self.assertTrue(\
        ic.validate_date_list(input, params) is None)
        
    def test_incorrect_date_list(self):
        params, input = {}, 'badformat'
        self.assertRaises(\
        ValueError, ic.validate_date_list, input, params)
        
        params, input = {}, ''
        self.assertRaises(\
        ValueError, ic.validate_date_list, input, params)
        
        params, input = {}, ','
        self.assertRaises(\
        ValueError, ic.validate_date_list, input, params)
        
        params, input = {}, ',200802'
        self.assertRaises(\
        ValueError, ic.validate_date_list, input, params)
        # extra space
        params, input = {}, '20080308, 200802'
        self.assertRaises(\
        ValueError, ic.validate_date_list, input, params)
        # bad month
        params, input = {}, '200277'
        self.assertRaises(\
        ValueError, ic.validate_date_list, input, params)
        
    def test_correct_input(self):
        """Test that the output is the one expected"""
        params, input = {}, '198809'
        expected_output = {'date_list': [(1988,9)]}
        ic.validate_date_list(input, params)
        self.assertEqual(expected_output, params)
        
        params, input = {}, '198809,198808'
        expected_output = {'date_list': [(1988,9),(1988,8)]}
        ic.validate_date_list(input, params)
        self.assertEqual(expected_output, params)
        # same day
        params, input = {}, '198809,198809'
        expected_output = {'date_list': [(1988,9)]}
        ic.validate_date_list(input, params)
        self.assertEqual(expected_output, params)

      
if __name__ == '__main__':
        unittest.main()
