# Make the input_checking module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import input_checking as ic


class TestSBox(unittest.TestCase):
    """Methods testing different inputs of bounding box"""

    def test_valid_sbox(self):
        input, params = "120,-30,130,-10", {}
        self.assertTrue(\
        ic.validate_sbox(input, params) is None)
        
        input, params = "100,-40,150,60", {}
        self.assertTrue(\
        ic.validate_sbox(input, params) is None)
        # longitudes are converted to 0-360 format (negative lattitudes+=360)
        input, params = "-100,-40,-10,0", {}
        self.assertTrue(\
        ic.validate_sbox(input, params) is None)
        
        input, params = "10,-40,-10,0", {}
        self.assertTrue(\
        ic.validate_sbox(input, params) is None)

    def test_incorrect_order(self):
        # minlong > maxlong
        input, params = "120,-10,110,0", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # minlong > maxlong and minlat > maxlat
        input, params = "120,0,110,-40", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # minlat > maxlat
        input, params = "110,0,130,-40", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # minlat > maxlat
        input, params = "120,-10,130,-30", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # TODO is it incorrect to have same longitude?
        input, params = "120,-30,120,-10", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # TODO is it incorrect to have same latitude?
        input, params = "110,-30,120,-30", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        
    def test_not_enough_coordinates(self):
        input, params = "", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        
        input, params = "120", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        
        input, params = "120,-10", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        
        input, params = "100,-10,110", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
     
    def test_incorrect_format(self):
        input, params = ",,,", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        
        input, params = "130,,140,-10", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        
        input, params = "a,b,c,d", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        
        input, params = "130,b,c,d", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        
        input, params = "130,-60,c,0", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
            
    def test_out_of_bounds(self):
        # Invalid min longitude
        input, params = "-200,-20,140,0", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # Invalid min longitude &  invalid order
        input, params = "200,-20,140,0", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # Invalid min longitude & correct order (invalid max longitude)
        input, params = "200,-20,201,0", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # Invalid min latitude
        input, params = "140,-100,142,0", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # Invalid min latitude & invalid order
        input, params = "140,100,142,0", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # Invalid min latitude & valid order (invalid max latitude)
        input, params = "140,100,142,110", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # Invalid max longitude & invalid order (correct min lat)
        input, params = "100,-20,-200,10", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # Invalid max longitude & correct order
        input, params = "100,-20,200,10", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # Invalid max latitude & invalid order (correct min lat)
        input, params = "100,10,120,-100", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)
        # Invalid max latitude & correct order
        input, params = "100,10,120,100", {}
        self.assertRaises(\
        ValueError, ic.validate_sbox, input, params)

    def test_correct_input(self):
        """Test that the output is the one expected"""
        input, params = "120,-30,130,-10", {}
        expected_output = {'sbox': [120.0,-30.0,130.0,-10.0]}
        ic.validate_sbox(input, params)
        self.assertEqual(expected_output, params)
        
        # longitudes are converted to 0-360 format
        input, params = "-120,-30,-100,-10", {}
        expected_output = {'sbox': [240.0,-30.0,260.0,-10]}
        ic.validate_sbox(input, params)
        self.assertEqual(expected_output, params)
    
if __name__ == '__main__':
        unittest.main()
