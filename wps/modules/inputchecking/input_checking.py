"""This module checks that the user inputs are correct and returns a python 
dictionary of valid arguments and their values.
The function used from outside of this module is check_arguments(inputs).
"""
import re
import datetime


def check_if_supported(inputs):
    """ Check if all the arguments provided are supported """
    supported_arguments = ['hemisphere', 'year', 'month', 'year_range', \
                           'month_range', 'date_list', 'name', 'sbox', 'bbox',\
                           'pressmin', 'pressmax', 'genesis', 'res_x', \
                           'res_y', 'output_format', 'bandwidth']
    for name in inputs:
        if name not in supported_arguments:
            raise ValueError('Unsupported argument: ' + name)

def get_parameter(inputs, is_required, param_name):
    """Check if there if an argument is there and return its value"""
    # If the argument is there, return its value
    # If the argument is not there and is required, throw ValueError
    # If the argument is not there and is not required, return None
    argument = None
    if param_name in inputs:
        if 'value' in inputs[param_name]:
            argument = inputs[param_name]['value']
        else:
           raise ValueError('Parameter: {param} is ' \
                            'not formed correctly (no "value")'.\
                            format(param=param_name)) 
        # If the parameter is declared in zcfg but not provided:
        if argument == 'NULL':
            if is_required:
                raise ValueError('Missing parameter: {param}'.\
                format(param=param_name))
            argument = None
    else:
        if is_required:
            raise ValueError('Missing parameter: {param}'.\
            format(param=param_name))
    return argument

def validate_year(year_input, params):
    """Check the format of the argument 'year', convert the year(s)
    to integer(s) and add them as a list in params.
    """
    pattern = re.compile('^(\d){4}$')
    list_of_years = []
    years = year_input.split(',')
    for year in years:
        matchObj = re.match(pattern, year)
        if not matchObj:
            raise ValueError(\
            "Incorrect year format: Not a correct year {y}".format(y=year))
        try:
            year_int = int(year)
        except ValueError:
            raise ValueError("Incorrect year format: not a number")
        # Eliminate duplicates
        if year_int not in list_of_years:
            list_of_years.append(year_int)
    params['year'] = list_of_years

def get_year_range_int_list(year_range):
    """Try to convert a list of string years to a list of integer years
    and check the validity of the format.
    """
    pattern = re.compile('^(\d){4}$')
    # The first year of the range to check the chronological order:
    years_range_list = []
    for index, year in enumerate(year_range):
        matchObj = re.match(pattern, year)
        if not matchObj:
            raise ValueError(\
            "Incorrect year_range format: {y}".format(y=year))
        try:
            year_int = int(year)
        except ValueError:
            raise ValueError("Incorrect year_range format: not a number")
        # Check chronological order
        if index != 0 and year_int <= years_range_list[0]:
            raise ValueError("Incorrect year_range format: \
            Not in chronological order.")
        years_range_list.append(year_int)
    return years_range_list

def validate_year_range(year_range, params):
    """Check the format of the argument 'year_range' and add the 
    valid year_range as a list of integers in params.
    """
    years = year_range.split(',')
    if len(years) != 2:
         raise ValueError("Incorrect year_range format: \
         Incorrect number of years: {nb_years}".\
         format(nb_years=len(years)))
    params['year_range'] = get_year_range_int_list(years)
    
def get_box_float_list(box_string):
    """Convert the box string list to a float list, and check bounds."""
    # Make sure there are 4 coordinates
    if len(box_string) != 4:
        raise ValueError()
    box_valid = []
    for index, coordinate in enumerate(box_string):
        try:
            coord = float(coordinate)
        except ValueError:
            raise ValueError("Incorrect box format: not a number")
        # Check for correct latitude and longitude
        if index == 0 or index == 2:
            if coord < -180 or coord > 180:
               raise ValueError("Incorrect box format: Out of bounds")
        elif index == 1 or index == 3:
            if coord < -90 or coord > 90:
                raise ValueError("Incorrect box format: Out of bounds")
        box_valid.append(coord)
    return box_valid
    
def validate_box(box, params):
    """Check the format of a box argument ('minlon, minlat, maxlon, maxlat') 
    and return the box converted in a python list of floats.
    """
    box_string = box.split(',')
    box_valid = get_box_float_list(box_string)
    # Convert longitudes to 0-360 format
    if box_valid[0] < 0:
        box_valid[0] += 360
    if box_valid[2] < 0:
        box_valid[2] += 360
    if box_valid[0] >= box_valid[2] \
    or box_valid[1] >= box_valid[3]:
        raise ValueError("Incorrect box format: \
        Not in format minx, miny, maxx, maxy")
    return box_valid
    
def validate_bbox(bbox, params):
    """Check the format of the argument 'bbox' and add the 
    valid coordinates with converted longitudes as a list of floats 
    in params.
    """
    bbox_valid = validate_box(bbox, params)
    params['bbox'] = bbox_valid

def validate_sbox(sbox, params):
    """Check the format of the argument 'sbox' and add the 
    valid coordinates with converted longitudes as a list of floats 
    in params.
    """
    sbox_valid = validate_box(sbox, params)
    params['sbox'] = sbox_valid
    
def validate_name(name, params):
    """Check the format of the argument 'name' and add it to params."""
    matchObj = re.match('^[\w-]+$', name)
    if not matchObj:
        raise ValueError('Incorrect name')
    params['name'] = name.upper()

def validate_pressmin(pressmin, params):
    """Check the format of the argument 'pressmin', convert it to float, 
    and add it to params.
    """
    try:
        params["pressmin"] = float(pressmin)
    except ValueError:
        raise ValueError('Incorrect pressmin format')

def validate_pressmax(pressmax, params):
    """Check the format of the argument 'pressmax', convert it to float, 
    and add it to params.
    """
    try:
        params["pressmax"] = float(pressmax)
    except Exception as exc:
        raise ValueError('Incorrect pressmax format')
        
def validate_pressure(pressmin_float, pressmax_float):
    """Check order for pressures"""
    if pressmin_float > pressmax_float:
        raise ValueError('Incorrect pressure: pressmin > pressmax')
        
def validate_hemisphere(hemisphere, params):
    """Check the format of the argument 'hemisphere' and add it to params."""
    if hemisphere.upper() != 'N' and hemisphere.upper() != 'S':
        raise ValueError('Incorrect hemisphere format: ')
    params["hemisphere"] = hemisphere.upper()
    
def validate_month(month_list, params):
    """Check the format of the argument 'month', and add it as a list of 
    integers to params.
    """
    months = month_list.split(',')
    month_list_int = []
    for month in months:
        try:
            month_int = int(month)
        except ValueError:
            raise ValueError("Incorrect month value")
        if month_int < 1 or month_int > 12:
            raise ValueError("Incorrect month")
        # Eliminate duplicates
        if month_int not in month_list_int:
            month_list_int.append(month_int)
    params["month"] = month_list_int

def validate_month_range(month_range, params):
    """Check the format of the argument 'month_range', and add the 2
    months as a list of integers in params."""
    months = month_range.split(',')
    if len(months) != 2:
        raise ValueError("Incorrect month_range format: \
        Incorrect number of months: {nb_months}".\
        format(nb_months=len(months)))
    month_range_list = []
    for month in months:
        try:
            month_int = int(month)
        except ValueError:
            raise ValueError("Incorrect month_range value")
        if month_int < 1 or month_int > 12:
            raise ValueError("Incorrect month_range")
        month_range_list.append(month_int)
    params["month_range"] = month_range_list
    
def validate_single_date(date_string):
    """Validate a date in the format YYYYMM and returns its year, month 
    in a tuple.
    """
    date = None
    try:
        date = datetime.datetime.strptime(date_string, '%Y%m').date()
    except ValueError:
        raise ValueError("Incorrect date_list value")
    return (date.year, date.month)

def validate_date_list(date_list, params):
    """Check the format of the argument 'date_list', add it to params"""
    dates = date_list.split(',')
    valid_dates = []
    for date in dates:
        valid_date = validate_single_date(date)
        if valid_date not in valid_dates:
            valid_dates.append(valid_date)
    params["date_list"] = valid_dates

def validate_output_format(output_format, params):
    """Check the format of the argument 'output_format', add it to params"""
    if output_format is None:
        params['output_format'] = "NETCDF"
        return
    if output_format.upper() != "JSON" \
    and output_format.upper() != "NETCDF" \
    and output_format.upper() != "GEOJSON":
        raise ValueError('Unsupported output format: {f}'.format(
        f=output_format))
    params["output_format"] = output_format.upper()
    
def validate_genesis(genesis, params):
    """Check the format of the argument 'genesis', add it to params"""
    if genesis is None:
        params['genesis'] = False
        return
    elif genesis.upper() == "TRUE":
        params['genesis'] = True
        return
    elif genesis.upper() == "FALSE":
        params['genesis'] = False
        return
    else:
        raise ValueError('Invalid value for genesis: {g}'.format(g=genesis))
    
def validate_res_x(res_x, params):
    """ Check the values of the argument res_x and put the float in params """
    float_res_x = None
    # Convert to float
    try:
        float_res_x = float(res_x)
    except ValueError:
        raise ValueError("Incorrect res_x value")
    # Check value
    if float_res_x>1.0 or float_res_x<0.1:
        raise ValueError('Incorrect input: '\
                         'res_x must not greater than 1 or smaller than 0.1')
    params['res_x'] = float_res_x
    
def validate_res_y(res_y, params):
    """ Check the values of the argument res_y and put the float in params """
    float_res_y = None
    # Convert to float
    try:
        float_res_y = float(res_y)
    except ValueError:
        raise ValueError("Incorrect res_y value")
    # Check value
    if float_res_y>1.0 or float_res_y<0.1:
        raise ValueError('Incorrect input: '\
                         'res_y must not greater than 1 or smaller than 0.1')
    params['res_y'] = float_res_y

def validate_bandwidth(bandwidth,params):
    """Check the value of the argument bandwidth and put float in params"""
    float_bandwidth = None
    if bandwidth is None:
        bandwidth = 3.0;
    try:
        float_bandwidth = float(bandwidth)
    except ValueError:
        raise ValueError("Incorrect bandwidth value")

    if float_bandwidth<1.0 or float_bandwidth>5.0:
        raise ValueError('Incorrect input: '\
                         'bandwidth must not greater than 5 or smaller than 1')
    params['bandwidth'] = float_bandwidth

def check_arguments(inputs):
    """Get parameters from a zoo input dictionary, check them, return a 
    dictionary of valid arguments.
    """
    check_if_supported(inputs)
    
    params = {}
    hemisphere = get_parameter(inputs, True, "hemisphere")
    year = get_parameter(inputs, False, "year")
    year_range = get_parameter(inputs, False, "year_range")
    date_list = get_parameter(inputs, False, "date_list")
    bbox = get_parameter(inputs, False, "bbox")
    sbox = get_parameter(inputs, False, "sbox")
    pressmin = get_parameter(inputs, False, "pressmin")
    pressmax = get_parameter(inputs, False, "pressmax")
    name = get_parameter(inputs, False, "name")
    output_format = get_parameter(inputs, False, "output_format")
    month = get_parameter(inputs, False, "month")
    month_range = get_parameter(inputs, False, "month_range")
    genesis = get_parameter(inputs, False, "genesis")
    res_x = get_parameter(inputs, False, "res_x")
    res_y = get_parameter(inputs, False, "res_y")
    bandwidth = get_parameter(inputs, False, "bandwidth")
    
    if month is not None and year is None:
        raise ValueError('Incorrect input: '\
                         'month was specified but not year.')
    if month_range is not None and year_range is None:
        raise ValueError('Incorrect input: '\
                         'month_range was specified but not year_range.')
    

    # Parameter validation and conversion
    validate_hemisphere(hemisphere, params)
    # Make sure we only process year or year range and not both
    if year is not None:
        if year_range is not None:
            raise ValueError('Incorrect input: '\
                             'both year and year_range were supplied.')
        if date_list is not None:
            raise ValueError('Incorrect input: '\
                             'both year and date_list were supplied.')
        validate_year(year, params)
        # month list:
        if month is not None:
            validate_month(month, params)
    elif year_range is not None:
        if date_list is not None:
            raise ValueError('Incorrect input: '\
                             'both year_range and date_list were supplied')
        validate_year_range(year_range, params)
        if month_range is not None:
            validate_month_range(month_range, params)
    elif date_list is not None:
        validate_date_list(date_list, params)
    if bbox is not None:
        validate_bbox(bbox, params)
    if sbox is not None:
        validate_sbox(sbox, params)
    if pressmin is not None:
        validate_pressmin(pressmin, params)
    if pressmax is not None:
        validate_pressmax(pressmax, params)
        if pressmin is not None:
            validate_pressure(params['pressmin'], params['pressmax'])
            
    # If genesis is none, it will be assigned a default value:
    validate_genesis(genesis, params)
    #if name is not None:
    #    validate_name(name, params)
    # We need an output format (don't check for None):
    validate_output_format(output_format, params)
    if res_x is not None:
        validate_res_x(res_x, params)
    if res_y is not None:
        validate_res_y(res_y, params)
    #If bandwidth is none, it will be assigned a default value:
    validate_bandwidth(bandwidth, params)
        
    return params

    

