"""
Input:
This module takes as input a list of fixes in numpy array,
the fixes must have at least a latitude and longitude:
For example:
array([(1969, 'ALINE', -9.1, 67.8), (1969, 'ALINE', -9.0, 66.2),
       (1969, 'ALINE', -9.0, 64.3), (1969, 'ALINE', -9.0, 63.0)], 
      dtype=[('year', '<i4'), ('name', '|S12'), ('lat', '<f8'), ('lon', '<f8')])
It also uses the arguments to get the size of the bbox to be returned.

Output:
It returns a tuple of numpy arrays: (xm, ym, Z):
Z[row][col] is a density at the longitude xm[row][col], and the 
latitude ym[row][col]
(where row is in range(len).

len(xm) = number of latitudes 
(and not longitudes although the values in xm are longitudes)
for any i such that i is in range(len(xm)), len(xm[i]) = number of longitudes
Note that len(xm) = len(ym) = len(zm) and len(xm[i]) = len(ym[i]) = len(Z[i])
Also, all xm[i] are equal and all ym[i][j] are equal.
Each value in Z is different.

To test the module:
import fix_processor as fp
import numpy as np
fixes = np.array([(1969, 'ALINE', -9.1, 67.8), (1969, 'ALINE', -9.0, 66.2),
       (1969, 'ALINE', -9.0, 64.3), (1969, 'ALINE', -9.0, 63.0)], 
      dtype=[('year', '<i4'), ('name', '|S12'), ('lat', '<f8'), ('lon', '<f8')])
bbox = [62, -10, 68, -8]
res_x = 0.5
res_y = 0.5
WPS_CONFIG = {'use_cython': False, 'density': 'SCIPY', 'kernel': 'lucy', \
              'bandwidth': 3.0}
data = fp.process(fixes, WPS_CONFIG['use_cython'], WPS_CONFIG['density'], \
                  bbox, res_x, res_y, WPS_CONFIG['kernel'], \
                  WPS_CONFIG['bandwidth'])
xm = data[0]
ym = data[1]
Z = data[2]
"""
import sys
import os.path
import numpy as np


def process(tk, use_cython, density, bbox = None, res_x = 1.0, res_y =1.0, \
            density_kernel="lucy", bandwidth=3.0):
    """ Compute density for each lat-long pair requested """
    if use_cython:
        import density_cython as d
    else: # Python as a safe default
        import density_python as d
    # Get the area of the earth requested by the user
    if bbox is not None:
        xrng = (bbox[0], bbox[2])
        yrng = (bbox[1], bbox[3])
    else:
        xrng = (0.,360.)
        yrng = (-90,90.)

    if density.upper() == "SCIPY":
        # density_scipy expects the bandwidth in a different format:
        bandwidth = bandwidth*(1.0/10.0)
        return d.density_scipy(tk.copy(), xrng, yrng, res_x, res_y, bandwidth)
    else: # It is Smooth by default
        return d.density_smooth(tk.copy(), xrng, yrng, res_x, res_y, \
                                bandwidth, kernel=density_kernel)
