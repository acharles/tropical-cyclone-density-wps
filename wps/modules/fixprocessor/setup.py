""" Cython Setup File 
    Compile by:
        sudo python setup.py build_ext --inplace
"""

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [Extension("density_cython", ["density_cython.pyx"], extra_compile_args=["-O3",])]

setup(
  name = 'Density Cython Method',
  cmdclass = {'build_ext': build_ext},
  ext_modules = ext_modules
)
