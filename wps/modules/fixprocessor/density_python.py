""" A generic purpose kernel density script. """

import numpy as np
from math import exp
from math import floor


def get_grid_map(xmin,xmax,ymin,ymax,rx,ry):
    """ Returns the coordinates of the centers of the
        grid points.
        res -- grid spacing (Smaller res = finer grid).
    """
    gridx = np.arange(xmin+rx/2.0,xmax,rx)
    gridy = np.arange(ymin+ry/2.0,ymax,ry)
    return gridx,gridy  

def lucy_w(r,h):
    """ The Lucy Kernel. Returns W. Only 2d. """
    q = 5. / (np.pi * h**2)
    if r < 0:
        r = abs(r)
    if( r < h ):
        w = q * (1 + 3.*r/h)*((1.-r/h))**3
    else:
        w = 0
    return w
    
def gauss_kernel(r,h):
    """ Ye old Gaussian SPH kernel in two dimensions Returns w """
    q = r/h
    factor = 1.e0 / ( (h**2) * (np.pi) )
    w = factor * exp(-q*q)
    return w

def splash_map_2d(x,y,r,m,h,bounds,cutoff,kernel): 
    """ Kernel density estimation for particles.
        Called 'splash_map' after Daniel Price's Splash
        SPH rendering code.
        
        res is the width of grid cells (todo, should be a parameter)

        x - x positions of grid
        y - y positions of grid
        r - particle positions
        bounds - box boundaries xmin,xmax,ymin,ymax
        cutoff - smoothing cutoff

    """

    if kernel.upper() == "GAUSS":
        kernel_m = gauss_kernel
    else:
        kernel_m = lucy_w
    
    nx = x.size
    ny = y.size

    if cutoff > nx: print "cutoff is too large this won't work"

    res = x[1]-x[0] #assume regular
    Z = np.zeros((nx,ny))
    dr = np.zeros([2])
    xmin,xmax,ymin,ymax = bounds
    #loop over particle positions
    for i in range(m.size):
        # each particle contributes to pixels
        xpixmin = int(floor( (r[i,0] - cutoff - xmin)/res ) )
        ypixmin = int(floor( (r[i,1] - cutoff - ymin)/res ) )
        xpixmax = int(floor( (r[i,0] + cutoff - xmin)/res )   )
        ypixmax = int(floor( (r[i,1] + cutoff - ymin)/res )   )

        for ypx in range(ypixmin,ypixmax):
            for xpx in range(xpixmin,xpixmax):
                
                # where the pixel bounds cross the box bounds, need to 
                # wrap pixel coordinates
                if ypx >= ny:
                    continue
                if ypx < 0:
                    continue
                if xpx >= nx:
                    continue
                if xpx < 0:
                    continue
               
                dr[0] = (r[i,0] - x[xpx])
                dr[1] = (y[ypx] - r[i,1])
                rsq =  np.dot(dr,dr)
                if rsq < (cutoff*cutoff):
                    s = np.sqrt(rsq)
                    Z[xpx,ypx] += m[i]* kernel_m(s,h[i])
    return Z

import scipy.stats as stats  # @UnresolvedImport
class ackde(stats.kde.gaussian_kde):
    bandwidth = 1.0
    def constant_factor(self):
        return self.bandwidth
        
def density_scipy(tk,xrng=(0.,360.),yrng=(-90.,90.),rx=1.0,ry=1.0,bandwidth=0.5):
    """ A simple kde track density on a lat-lon regular grid. """
    """ tk -- tracks in record array form
        xrng,yrng -- geographical extent
        rx -- grid resolution in x
        ry -- grid resolution in y
        bandwidth -- width of smoothing kernel

        The resulting field will be normalised to one. Depending on
        what it is wanted for this will need to be rescaled.

        Limitations: if the fixes are inhomogenous in time, storms with more
        frequent fixes are over-represented in the density.  (This is not an issue
        if only genesis is rendered.) 
    """
    # Get the tracks as positions
    r = np.array([tk['lon'],tk['lat']])
    kde = ackde(r)
    kde.bandwidth = bandwidth
    kde.covariance_factor = kde.constant_factor
    kde._compute_covariance()
    # Make a grid
    gridx,gridy = get_grid_map(xrng[0],xrng[1],yrng[0],yrng[1],rx,ry)
    nx,ny = gridx.size,gridy.size
    xm,ym = np.meshgrid(gridx,gridy)
    grid_coords = np.append(xm.reshape(-1,1),ym.reshape(-1,1),axis=1)
    Z = kde.evaluate(grid_coords.T)
    Z = Z.reshape([ny,nx])
    #print 'kde area',kde.integrate_box((xrng[0],yrng[0]),(xrng[1],yrng[1]))
    # scale up, we want the units to be cyclones per year
    #nseas = len(list(set(tk['year'])))
    #ns =len(list(set(tk['name']))) 
    #Z = Z * ns  / nseas
    #Z = Z
    return xm,ym,Z
    
def density_smooth(tk,xrng=(0.,360.),yrng=(-90.,90.),rx=1.0,ry=1.0,bandwidth=5.0,
    normed=True,kernel="lucy"):
    """ Uses a smooth particle inspired kernel interpolation.
        Normalise by the number of points.
        Total 'mass' returned is 1
    """
    #print "normed", type(normed)
    rx,ry = float(rx),float(ry)
    r = np.array([tk['lon'],tk['lat']])
    gridx,gridy = get_grid_map(xrng[0],xrng[1],yrng[0],yrng[1],rx,ry)
    mass = np.zeros([r.shape[1]])
    h = np.zeros([r.shape[1]])
    mass[:] = 1.0
    if r.shape[1] != 0:
        mass[:] = 1.0/r.shape[1]
    h[:] = bandwidth
    Z = splash_map_2d(gridx,gridy,r.T,mass,h,[xrng[0],xrng[1],yrng[0],yrng[1]],bandwidth,kernel)
    Z = Z.T
    xm,ym = np.meshgrid(gridx,gridy)
    if not normed:
        Z = Z * r.shape[1]
    return xm,ym,Z
    
