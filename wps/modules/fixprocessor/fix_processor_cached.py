import fix_processor


def process(tk, args, WPS_CONFIG):
    """ Wrapper function to cache (or not) the output of fix_processor.process 
    
    It will use caching if WPS_CONFIG['use_caching'] is set to true.
    """
    process_method = fix_processor.process
    
    if WPS_CONFIG['use_caching']:
        from beaker.cache import CacheManager

         # Cache in a DBM file, in the cache_directory from the config file:
        cache = CacheManager(type='dbm', data_dir=WPS_CONFIG['cache_directory'])
        
        # Beaker caches the result of a function, so we need a wrapper function
        # TODO: Beaker uses a cache key to identify cached data,
        # Part of this key is generated using str(arg) for each argument
        # str(numpy array) does not show every element of the numpy array
        # for big arrays: it only shows first and last few elements
        # If two requests have the same few first and last elements,
        # the cached data will be returned.
        # This quick fix adds an argument tkstr which is expected to be 
        # equivalent to tk but that can show all the elements in the tk array
        # when using the str function (e.g. a python list)
        # Using this, the caching key will be different, even with a long 
        # numpy array of cyclone fixes with first and last same few elements
        @cache.cache('TrackDensityProcessorNamespace', expires='never')
        def process_wrapper(tk, tkstr, use_cython, density, bbox, res_x, res_y, 
                            kernel, bandwidth):
            """ The return value of this method will be cached.
            
            Next time this function will be called with the same arguments,
            the cached data will be returned.
            """
            data = fix_processor.process(tk, use_cython, density, bbox, res_x,\
                                         res_y, kernel, bandwidth)
            return data
        
        # Call the wrapper function instead of directly calling the processor
        process_method = process_wrapper

    bbox = args['bbox'] if 'bbox' in args else None
    res_x = args['res_x'] if 'res_x' in args else 1.0
    res_y = args['res_y'] if 'res_y' in args else 1.0
    bandwidth = args['bandwidth'] if 'bandwidth' in args else 3.0
    kernel = WPS_CONFIG['kernel'] if 'kernel' in WPS_CONFIG else "lucy"
    if WPS_CONFIG['use_caching']:
        # TODO this converts tk to a list to be able to see all the elements
        # when using the str function
        return process_method(tk, tk.tolist(), WPS_CONFIG['use_cython'], \
                                   WPS_CONFIG['density'], \
                                   bbox, res_x, res_y, kernel, bandwidth)
    else:
        return process_method(tk, WPS_CONFIG['use_cython'], \
                                   WPS_CONFIG['density'], \
                                   bbox, res_x, res_y, kernel, bandwidth)
