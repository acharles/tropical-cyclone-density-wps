# cython: profile=True
# density_cython.pyx
"""
This is a Cython module containing the density calculation code.

This file must be recompiled in Cython (refer to setup.py comments)
to make any changes to this file active.
"""
import os
import numpy as np
from libc.math cimport exp
from libc.math cimport floor
from libc.math cimport sqrt

cimport numpy as np
cimport cython

DTYPE = np.float64
ctypedef np.float64_t DTYPE_t

cdef packed struct FIX:
    int year
    char[12] name
    double lat
    double lon

cdef double lucy_w(double r, double h) except? -2:
    """ The Lucy Kernel. Returns W. Only 2d. """
    cdef double q, w, pi = 3.14159265358979323846
    q = 5. / (pi * h**2)
    if r < 0:
        r = abs(r)
    if( r < h ):
        w = q * (1 + 3.*r/h)*((1.-r/h))**3
    else:
        w = 0
    return w

#@cython.cdivision(True)
cdef double gauss_kernel(double r, double h) except? -2:
    """ Ye old Gaussian SPH kernel in two dimensions Returns w """
    cdef double q, factor, w, pi = 3.14159265358979323846
    q = r/h
    factor = 1.e0 / ( (h*h) * (pi) )
    w = factor * exp(-q*q)
    return w

@cython.boundscheck(False)
@cython.wraparound(False)
cdef np.ndarray[DTYPE_t, ndim=2] splash_map_2d(np.ndarray[DTYPE_t, ndim=1] x, 
                  np.ndarray[DTYPE_t, ndim=1] y, 
                  np.ndarray[DTYPE_t, ndim=2] r, 
                  np.ndarray[DTYPE_t, ndim=1] m,
                  np.ndarray[DTYPE_t, ndim=1] h, 
                  list bounds, DTYPE_t cutoff, str kernel): 
    """ Kernel density estimation for particles.
        Called 'splash_map' after Daniel Price's Splash
        SPH rendering code.
        
        res is the width of grid cells (todo, should be a parameter)

        x - x positions of grid
        y - y positions of grid
        r - particle positions
        bounds - box boundaries xmin,xmax,ymin,ymax
        cutoff - smoothing cutoff

    """
    cdef int xpixmin, ypixmin, xpixmax, ypixmax, ypx, xpx
    cdef unsigned int i, nx, ny
    cdef DTYPE_t rsq, s, res
    cdef np.ndarray[DTYPE_t, ndim=2] Z
    cdef np.ndarray[DTYPE_t, ndim=1] dr
    cdef DTYPE_t xmin, xmax, ymin, ymax

    
    if kernel.upper() == "GAUSS":
        kernel_m = gauss_kernel
    else:
        kernel_m = lucy_w
    
    nx = x.size
    ny = y.size

    if cutoff > nx: print "cutoff is too large this won't work"

    res = x[1]-x[0] #assume regular
    Z = np.zeros((nx,ny), dtype=DTYPE)
    dr = np.zeros([2], dtype=DTYPE)
    xmin,xmax,ymin,ymax = bounds
    #loop over particle positions
    for i in range(m.size):
        # each particle contributes to pixels
        xpixmin = int(floor( (r[i,0] - cutoff - xmin)/res ) )
        ypixmin = int(floor( (r[i,1] - cutoff - ymin)/res ) )
        xpixmax = int(floor( (r[i,0] + cutoff - xmin)/res ) )
        ypixmax = int(floor( (r[i,1] + cutoff - ymin)/res ) )

        for ypx in range(ypixmin, ypixmax):
            for xpx in range(xpixmin, xpixmax):
                
                # where the pixel bounds cross the box bounds, need to 
                # wrap pixel coordinates
                if ypx >= ny:
                    continue
                if ypx < 0:
                    continue
                if xpx >= nx:
                    continue
                if xpx < 0:
                    continue
                dr[0] = (r[i,0] - x[xpx])
                dr[1] = (y[ypx] - r[i,1])
                rsq = np.dot(dr,dr)
                if rsq < (cutoff*cutoff):
                    s = sqrt(rsq)
                    Z[xpx,ypx] += m[i]* kernel_m(s,h[i])
    return Z

@cython.boundscheck(False)
@cython.wraparound(False)
cdef tuple get_grid_map(DTYPE_t xmin, DTYPE_t xmax, DTYPE_t ymin, DTYPE_t ymax, 
    DTYPE_t rx, DTYPE_t ry):
    cdef np.ndarray[DTYPE_t, ndim=1] gridx, gridy
    """ Returns the coordinates of the centers of the
        grid points.
        res -- grid spacing (Smaller res = finer grid).
    """
    gridx = np.arange(xmin+rx/2.0,xmax,rx)
    gridy = np.arange(ymin+ry/2.0,ymax,ry)
    return gridx, gridy  

import scipy.stats as stats  # @UnresolvedImport
class ackde(stats.kde.gaussian_kde):
    bandwidth = 1.0
    def constant_factor(self):
        return self.bandwidth

@cython.wraparound(False)
@cython.boundscheck(False)
cpdef tuple density_smooth(tk, tuple xrng=(0.,360.), tuple yrng=(-90.,90.), 
    DTYPE_t rx=1.0, DTYPE_t ry=1.0,
    DTYPE_t bandwidth=5.0, short normed=True,  str kernel="lucy"):
    """ Uses a smooth particle inspired kernel interpolation.
        Normalise by the number of points.
        Total 'mass' returned is 1
    """
    cdef np.ndarray[DTYPE_t, ndim=2] r, xm, ym, Z
    cdef np.ndarray[DTYPE_t, ndim=1] gridx, gridy, h
    cdef np.ndarray[DTYPE_t, ndim=1] mass
    
    r = np.array([tk['lon'],tk['lat']],dtype=DTYPE)
    gridx,gridy = get_grid_map(xrng[0],xrng[1],yrng[0],yrng[1],rx,ry)
    mass = np.zeros([r.shape[1]],dtype=DTYPE)
    h = np.zeros([r.shape[1]], dtype=DTYPE)
    mass[:] = 1.0
    if r.shape[1] != 0:
        mass[:] = 1.0/r.shape[1]
    h[:] = bandwidth
    Z = splash_map_2d(gridx,gridy,r.T,mass,h,[xrng[0],xrng[1],yrng[0],
        yrng[1]], bandwidth, kernel)
    Z = Z.T
    xm,ym = np.meshgrid(gridx,gridy)
    if not normed:
        Z = Z * r.shape[1]
    return xm, ym, Z

@cython.boundscheck(False)
@cython.wraparound(False)
cpdef tuple density_scipy(tk, tuple xrng=(0.,360.), 
    tuple yrng=(-90.,90.), DTYPE_t rx=1.0, DTYPE_t ry=1.0,
    DTYPE_t bandwidth=0.5):
    """ A simple kde track density on a lat-lon regular grid. """
    """ tk -- tracks in record array form
        xrng,yrng -- geographical extent
        rx -- grid resolution in x
        ry -- grid resolution in y
        bandwidth -- width of smoothing kernel

        The resulting field will be normalised to one. Depending on
        what it is wanted for this will need to be rescaled.

        Limitations: if the fixes are inhomogenous in time, storms with more
        frequent fixes are over-represented in the density.  
        (This is not an issue if only genesis is rendered.)
    """
    cdef np.ndarray[DTYPE_t, ndim=2] r, xm, ym, grid_coords, Z
    cdef np.ndarray[DTYPE_t, ndim=1] gridx, gridy, z
    cdef int nx, ny

    # Get the tracks as positions
    r = np.array([tk['lon'],tk['lat']], dtype=DTYPE)
    kde = ackde(r)
    kde.bandwidth = bandwidth
    kde.covariance_factor = kde.constant_factor
    kde._compute_covariance()
    # Make a grid
    gridx,gridy = get_grid_map(xrng[0],xrng[1],yrng[0],yrng[1],rx,ry)
    nx,ny = gridx.size,gridy.size
    xm,ym = np.meshgrid(gridx,gridy)
    grid_coords = np.append(xm.reshape(-1,1),ym.reshape(-1,1),axis=1)
    z = kde.evaluate(grid_coords.T)
    Z = z.reshape([ny,nx])
    return xm, ym, Z

