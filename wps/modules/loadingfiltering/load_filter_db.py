"""
The filter functions add one or more where predicates to the 
sql_where_predicates variable in order to filter the data.
In the end, the predicated in sql_where_predicates will be joined together 
using the AND keyword.

To use this module:
import load_filter_db as lfd
args = {'year': [1969], 'hemisphere': 'S'}
WPS_CONFIG = {'use_database': True, 'database_table': 'tracker.tcdata_points', 'database_password': 'postgres', 'database_name': 'tc', 'database_user': 'postgres'}
fixes = lfd.load_and_filter(args, WPS_CONFIG)
"""
import datetime
from dateutil.relativedelta import relativedelta


def add_or(expressions):
    """ Add OR between the strings in expressions"""
    return '({e})'.format(e=' OR '.join(expressions))
    
def add_and(expressions):
    """ Add AND between the strings in expressions"""
    return '({e})'.format(e=' AND '.join(expressions))

def filter_southern_hemisphere_year(year, sql_where_predicates):
    """ Filter a single year """
    sql_where_predicates.append("datetime >= '{y}-07-01'".format(y=year))
    sql_where_predicates.append("datetime < '{y}-07-01'".format(y=year+1))

def filter_years(years, hemisphere, sql_where_predicates):
    """ Select fixes in a year or in a list of years. """
    if hemisphere.upper() == 'N':
        sql_statements = []
        #for year in years:
        sql_where_predicates.append(\
        "EXTRACT(year FROM datetime) in ({y})".format(y=str(years)[1:-1]))
    else:
        sql_statements = []
        for year in years:
            sql_statements.append("(datetime >= '{y1}-07-01' AND "
            "datetime < '{y2}-07-01')".format(y1=year, y2=year+1))
        sql_where_predicates.append(add_or(sql_statements))

def filter_year_range(year_range, hemisphere, sql_where_predicates):
    """ Select fixes in a year range. """
    year_from = year_range[0]
    year_to = year_range[1]
    if hemisphere.upper() == 'N':
        sql_where_predicates.append(\
        "EXTRACT(year FROM datetime) >= {y}".format(y=year_from))
        sql_where_predicates.append(\
        "EXTRACT(year FROM datetime) <= {y}".format(y=year_to))
    elif hemisphere.upper() == 'S':
        sql_where_predicates.append(\
        "(datetime >= '{y}-07-01')".format(y=year_from))
        sql_where_predicates.append(\
        "(datetime < '{y}-07-01')".format(y=year_to+1))
    
def filter_yearmonth_range(year_range, month_range, hemisphere, 
                           sql_where_predicates):
    """ Select fixes by year_range and month range
        year_range is a tuple. E.g.(1968,1970)
        month_range is a tuple. E.g.(1,12)
    """
    end_date = datetime.date(year_range[1], month_range[1], 1)
    next_month_end_date = end_date + relativedelta(months=1)
        
    if hemisphere.upper() == 'N':
        sql_where_predicates.append("datetime >= '{y}-{m}-01'".format(\
        y=year_range[0], m=month_range[0]))
        sql_where_predicates.append("datetime < '{y}-{m}-01'".format(\
        y=next_month_end_date.year, m=next_month_end_date.month))
    elif hemisphere.upper() == 'S':
        if month_range[0] <= 6:
            sql_where_predicates.append("datetime >= '{y}-{m}-01'".format(\
            y=year_range[0]+1, m=month_range[0]))
        else:
            sql_where_predicates.append("datetime >= '{y}-{m}-01'".format(\
            y=year_range[0], m=month_range[0]))
        if month_range[1] <= 6:
            sql_where_predicates.append("datetime < '{y}-{m}-01'".format(\
            y=next_month_end_date.year+1, m=next_month_end_date.month))
        else:
            sql_where_predicates.append("datetime < '{y}-{m}-01'".format(\
            y=next_month_end_date.year, m=next_month_end_date.month))

def filter_month(month, sql_where_predicates):
    """ Select fixes in a set of months """
    if len(month) == 1:
        sql_where_predicates.append(\
        "EXTRACT(month FROM datetime) = {m}".format(m=month[0]))
    else:
        sql_where_predicates.append(\
        "EXTRACT(month FROM datetime) in ({m})".format(m=str(month)[1:-1]))
        
def filter_name(name, sql_where_predicates):
    """ 
    Filter cyclones by name (debug function, it is not used in the final 
    application) 
    """
    sql_where_predicates.append("name = '{n}'".format(n=name))

def filter_bbox(bbox, sql_where_predicates):
    """ 
    Filter cyclone fixes in a box delimited by 2 pairs of latitude/longitude.
    """
    sql_where_predicates.append("lon >= {minlon}".format(minlon=bbox[0]))
    sql_where_predicates.append("lat >= {minlat}".format(minlat=bbox[1]))
    sql_where_predicates.append("lon <= {maxlon}".format(maxlon=bbox[2]))
    sql_where_predicates.append("lat <= {maxlat}".format(maxlat=bbox[3]))
     
def filter_sbox(sbox, database_table, sql_where_predicates):
    """ Filter cyclones which have at least one fix in sbox """
    sql_where_predicates.append("tc_id in "
    "(SELECT tc_id FROM {table} "
	"WHERE lon >= {minlon} AND lat >= {minlat} "
	"AND lon <= {maxlon} AND lat <= {maxlat})".format(table=database_table,\
    minlon=sbox[0], minlat=sbox[1], maxlon=sbox[2], maxlat=sbox[3]))
    
def filter_minpress(minpress, sql_where_predicates):
    """ Minimum pressure filtering """
    sql_where_predicates.append("pressure >= {press}".format(\
        press=minpress))
        
def filter_maxpress(maxpress, sql_where_predicates):
    """ Maximum pressure filtering """
    sql_where_predicates.append("pressure <= {press}".format(\
    press=maxpress))
    
def filter_date_list(date_list, hemisphere, sql_where_predicates):
    """ Filtering using a list of year/month pairs """
    sql_or_predicates = []
    for date in date_list:  
        if hemisphere.upper() == 'S':
            year = date[0] if date[1]>=7 else date[0]+1
        else:
            year = date[0]
        sql_or_predicates.append(\
        "(EXTRACT(year FROM datetime) = {y} AND "
        "EXTRACT(month FROM datetime) = {m})".format(y=year, m=date[1]))
    sql_where_predicates.append(add_or(sql_or_predicates))
    
def filter_genesis(genesis, database_table, sql_where_predicates):
    """
    Filtering fixes by only keeping the genesis of cyclones.
    The first fix is chosen for cyclones without pressure.
    The first fix with pressure >= 995 is chosen for cyclones with pressure.
    """
    # This SQL query assumes that a fix can be uniquely identified by the set
    # (tc_id, datetime)
    if genesis:
        sql_where_predicates.append(\
        "id in (SELECT id "
        "FROM {table} A "
        "INNER JOIN "
        "(SELECT tc_id, min(datetime) as min_datetime "
        "FROM {table} "
        "WHERE pressure IS NULL OR pressure <= 995 "
        "GROUP BY tc_id) B ON (A.tc_id=b.tc_id) "
        "WHERE A.datetime = B.min_datetime "
        ")".format(table=database_table))

def filter_dataset(args, database_table):
    """ Calls filter functions based on the arguments.
    Some arguments have other argument dependencies 
    (e.g. month_range needs year_range) and some are
    not allowed to be together! (e.g. year & year range)
    """
    sql_where_predicates = []
    # Filter by date: Year(s)/Months OR Year_Range
    if 'year' in args:
        filter_years(args['year'], args['hemisphere'], sql_where_predicates)
        if 'month' in args:
			filter_month(args['month'], sql_where_predicates)
    elif 'year_range' in args:
        if 'month_range' in args:
            filter_yearmonth_range(args['year_range'], args['month_range'],
            args['hemisphere'], sql_where_predicates)
        else:
            filter_year_range(args['year_range'], args['hemisphere'], \
                              sql_where_predicates)
    elif 'date_list' in args:
        filter_date_list(args['date_list'], args['hemisphere'], \
        sql_where_predicates)
    # name filter is only used for debug, it is not needed.
    #if 'name' in args:
    #    tk = filter_name(args['name'].upper(), sql_where_predicates)
    if 'genesis' in args:
        filter_genesis(args['genesis'], database_table, sql_where_predicates)
    if 'sbox' in args:
        filter_sbox(args['sbox'], database_table, sql_where_predicates)
    if 'pressmin' in args:
        filter_minpress(args['pressmin'], sql_where_predicates)
    if 'pressmax' in args:
        filter_maxpress(args['pressmax'], sql_where_predicates)
    if 'bbox' in args:
        filter_bbox(args['bbox'], sql_where_predicates)
        
    return sql_where_predicates
    
def load_and_filter(arguments, WPS_CONFIG):
    """Create the predicates to put in the where clause, create the sql query
    and execute it.
    """
    sql_where_predicates = filter_dataset(arguments, 
                                          WPS_CONFIG['database_table'])
    if len(sql_where_predicates) >= 1:
        sql_where_statement = add_and(sql_where_predicates)
        sql_query = "SELECT year, name, lat, lon FROM {table} \
                    WHERE {where}".format(\
                        table=WPS_CONFIG['database_table'], \
                        where=sql_where_statement)
    else:
        sql_query = "SELECT year, name, lat, lon " \
                    "FROM {table}".format(\
                        table=WPS_CONFIG['database_table'])
    return execute_query(sql_query, WPS_CONFIG)
    
    
data_type = [('year', 'i'), ('name', 'S12'), ('lat', '<f8'), ('lon', '<f8')]    

def execute_query(sql, WPS_CONFIG):
    """ Connect to the database, execute the sql query and return a numpy array
    
    Load data from a postgresql database.
    This function expects the selected columns in the sql query to be year, 
    name, lat, and long in that order as described by data_type.
    """
    import psycopg2
    import numpy as np
    # Get a connection object to access the database
    connection = psycopg2.connect(database=WPS_CONFIG['database_name'], \
                                  user=WPS_CONFIG['database_user'], \
                                  password=WPS_CONFIG['database_password'], \
                                  host="localhost") 
    # Get a cursor to use to go through the results of the sql statement
    cur = connection.cursor()
    cur.execute(sql)      
    data_tuples = cur.fetchall()
    # Clean up
    cur.close()
    if connection:
        connection.close()
    return np.asarray(data_tuples,dtype=data_type)
