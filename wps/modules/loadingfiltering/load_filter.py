"""
This module just calls either database or csv loading/filtering module,
see __init__.py for more information.
"""
def load_and_filter(valid_arguments, WPS_CONFIG):
    """ Checks fot the type of loading/filtering specified in the configuration
    and calls uses the appropriate module. """
    if WPS_CONFIG['use_database']:
        import modules.loadingfiltering.load_filter_db as lf
    else:
        import modules.loadingfiltering.load_filter_csv as lf
    return lf.load_and_filter(valid_arguments, WPS_CONFIG)
