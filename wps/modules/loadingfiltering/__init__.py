"""
This package is used to load and filter data using some filtering arguments:
2 methods of loading/filtering are implemented:
-Loading/filtering from a csv file.
-Loading/filtering from a database table (using postgresql).

The main entry point of both of these modules is the function 
load_and_filter(arguments):
input:
arguments: dictionary that must contain 'hemisphere': 'S' or 'hemisphere': 'N'
The dictionary can also contain the following filtering arguments:
'year': [1991, 1987, ...], there must be at least one year specified
'month': [1,5,7, ...], there must be at least one month specified
'year_range': [1980, 1990], there must be exactly 2 years specified
'month_range': [2,5], there must be exactly 2 months specified
'date_list': [(1976, 12), (1978, 5), ...], there must be at least one year-month pair specified
'name': 'ALICE'
'sbox': [1,-10,10,0], minlon, minlat, maxlon, maxlat
'bbox': [1,-10,10,0], minlon, minlat, maxlon, maxlat
'pressmin': 990
'pressmax': 1000
'genesis': True

load_and_filter(arguments) returns a numpy array containing a list of fixes, 
it contains at least the following columns:
[('year', 'i'), ('name', 'S12'), ('lat', '<f8'), ('lon', '<f8')]   
"""
