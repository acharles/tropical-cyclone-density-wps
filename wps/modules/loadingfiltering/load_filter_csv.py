"""
To use this module:
import load_filter_csv as lf
args = {'year': [1969], 'hemisphere': 'S'}
fixes = lf.load_and_filter(args, None)
"""
import numpy as np
import os


data_type = [('year', 'i'), ('seq', 'i'), ('name', 'S12'), ('datetime', 'i8')
              , ('lat', '<f8'), ('lon', '<f8'), ('pressure', '<f8')]

def load_csv(fname):
    """ Load tropical cyclone tracks from a csv file given by
        fname, which must conform to the data schema.
    """
    tk = np.genfromtxt(fname, delimiter=',', skip_header=1,
            dtype=data_type)
        # Was previously necessary, may be necessary in future:
        # Strip the extraneous quotation marks
        # for i in range(shd['name'].size):
        #    shd['name'][i] = shd['name'][i][1:-1]
    return tk

def load_cache(fname,cache_fname):
    """ Loads the cache file """
    if not os.path.exists(cache_fname):
        tk = load_csv(fname)
        np.save(cache_fname,tk)
    else:
        tk = np.load(cache_fname)
    return tk
    
def load_shtc():
    """ Loads the Southern Hemisphere tropical cyclone data. """
    fname = 'tcsh1969-2009.csv'
    cache_fname = 'shd.npy'
    return load_cache(fname,cache_fname)
    
def filter_years(tk, years,hemisphere='s'):
    """ Select only fixes in a year in the list years. """
    """ tk is a record array with tracks. years is a list. """
    dates = list(tk['datetime'])
    date_month = [getmonth(date) for date in dates]
    date_month = np.array(date_month)
    date_year = [getyear(date) for date in dates]
    date_year = np.array(date_year)
    ydx = np.zeros(tk['year'].shape, dtype='Bool')
    if hemisphere.lower()=='s':
        for year in years:
            ydx = ydx | (((date_year == year) & (date_month > 6)) 
                | ((date_year == (year + 1)) & (date_month < 7)))
    elif hemisphere.lower()=='n':
        for year in years:
            ydx = ydx | (((date_year == year) & (date_month >= 1)) 
                & (date_month <= 12)) 
    return tk[ydx]

def filter_year_range(tk, year_range, hemisphere='s'):
    """ Selects all fixes within a given year range 
        year_range is a tuple. 
        E.g.(1968,1970), both 1968 and 1970 are included. 
    """
    year_from = year_range[0]
    year_to = year_range[1]
    year_list = []
    for year in range(year_from, (year_to + 1)):
        year_list.append(year)
    tk = filter_years(tk, year_list,hemisphere)
    return tk

def filter_yearmonth_range(tk, year_range, month_range, hemisphere):
    """ Select fixes by year_range and month range
        year_range is a tuple. E.g.(1968,1970)
        month_range is a tuple. E.g.(1,12)
    """
    year_start = year_range[0]
    year_end = year_range[1]
    if hemisphere == 's':   # adjust to hemisphere season (Southern)
        if month_range[0] < 7:
            year_start += 1
        if month_range[1] < 7:
            year_end += 1
    start = long("%4d%02d010000" % (year_start, month_range[0]))
    if month_range[1] == 12:
        end = long("%4d%02d010000" % (year_end+1, 1))
    else:
        end = long("%4d%02d010000" % (year_end, month_range[1]+1))
    dates = tk['datetime']
    datesx = (dates >= start) & (dates < end)
    return tk[datesx]
    
def filter_year(tk,year):
    """ Filter out fixes by year """
    ydx = tk['year'] == year
    return tk[ydx]

def getmonth(datetime):
    return (datetime / 1000000) % 100

def getyear(datetime):
    return datetime / 100000000

def filter_month(tk, month):
    """ Select only fixes in month """
    """ Use this on something already filtered for year. 
        Datetime is an integer of length 8.              """
    dates = list(tk['datetime'])
    mons = [getmonth(date) for date in dates]
    mons = np.array(mons)
    return tk[mons == month]

def filter_months(tk, months):
    """ Select only fixes in a month in the list months """
    """ Use this on something already filtered for year. 
        Datetime is an integer of length 8.              """
    mdx = np.zeros(tk['datetime'].shape, dtype='Bool')
    for month in months:
        dates = list(tk['datetime'])
        mons = [getmonth(date) for date in dates]
        mons = np.array(mons)
        mdx = mdx | (mons == month)
    return tk[mdx]
    
def filter_seasons(tk,years):
    """ Filter fixes for seasons defined such that 1997 == July1997 to June1998 """
    ydx = np.zeros(tk['year'].shape,dtype='Bool')
    for year in years:
        ydx = ydx | (tk['year'] == year)
    return tk[ydx]
	
def filter_date_list(tk, date_list, hemisphere):
    """ Selects only fixes which satisfy the required year/month """
    """ date_list is in format: ((year,month)(year,month)) """
    #""" date_list is in format: ({'year':1969,'month':3}) """
    dates = list(tk['datetime'])
    date_month = [getmonth(date) for date in dates]
    date_month = np.array(date_month)
    date_year = [getyear(date) for date in dates]
    date_year = np.array(date_year)
    ydx = np.zeros(tk['year'].shape, dtype='Bool')
    for d in date_list:
        year = d[0]
        #year = d['year']
        month = d[1]
        #month = d['month']
        if hemisphere.lower()=='s':
            if month < 7:
                year += 1
        ydx = ydx | ((date_year == year) & (date_month == month))
    return tk[ydx]

def filter_name(tk, name):
    """ Select fixes by name. """
    """ test:sel_names = filter_name(shd,'KATRINA')['name']. """
    ndx = tk['name'] == name
    return tk[ndx]
    
def get_names(tk):
    """ Return a list of all unique storm names. """
    return list(set(tk['name']))

def filter_bbox(tk, bbox):
    """ Filter fixes with coordinates lying inside or on the box defined by
        lon1,lat1,lon2,lat2. """
    lon1, lat1, lon2, lat2 = bbox
    lons = tk['lon']
    lats = tk['lat']
    londx = (lons >= lon1) & (lons <= lon2)
    latdx = (lats >= lat1) & (lats <= lat2)
    ldx = londx & latdx
    return tk[ldx]
    
def filter_in_bbox(tk,bbox):
    """ Filters named storms if they enter the bbox at any time 
        lon1,lat1,lon2,lat2. """
    names = get_names(tk)
    lon1, lat1, lon2, lat2 = bbox
    selnames = []
    sidx = np.zeros(tk.size,dtype=bool)
    for name in names:
        tkn = filter_name(tk,name)
        years = tkn['year']
        years = list(set(years))
        for year in years:
            ytk = filter_year(tkn,[year])
            lons = ytk['lon']
            lats = ytk['lat']
            londx = (lons >= lon1) & (lons <= lon2)
            latdx = (lats >= lat1) & (lats <= lat2)
            ldx = londx & latdx
            if np.any(ldx):
                sidx = sidx | ( (tk['name'] == name) & (tk['year'] == year))
    return tk[sidx]

def filter_pressure(tk, pressure_from=None , pressure_to=None):
    """ Filter fixes with pressure that lies within the range of
        pressure_from to pressure_to [NOTE: Not all records have pressure!] 
    """
    if pressure_to != None:
        pdx = tk['pressure'] <= pressure_to
        tk = tk[pdx]
    if pressure_from!=None:
        pdx = tk['pressure'] >= pressure_from
        tk = tk[pdx]
    return tk

def filter_tropical_cyclone(tk):
    """ Filters for systems that meet the criteria for a 
        tropical cyclone, defined as having a central pressure
        lower than 995. Note that some datasets may not contain
        pressure data, or it may be unreliable as in the case of
        coarse resolution GCM output. Thus, in the case that pressure
        does not exist, it is ignored (but still not filtered OUT)
    """
    pdtc = tk['pressure'] <= 995.
    pdnan = np.isnan(tk['pressure'],dtype=bool)
    pdx = np.logical_or(pdtc,pdnan)
    tk = tk[pdx]
    return tk
    
def filter_genesis(tk):
    """ Selects the first point for each storm at which central pressure is below 995.
    """
    tk = filter_tropical_cyclone(tk)
    tk_gen = np.zeros(0,dtype=data_type)
    seasons = list(set(tk['year']))
    for season in seasons:
        stk = filter_seasons(tk,[season])
        names = list(set(stk['name']))
        gentk = np.zeros(len(names),dtype=data_type)
        i = 0
        for name in names:
            nametk = filter_name(stk,name)
            gentk[i] = nametk[nametk['datetime']==nametk['datetime'].min()]
            i += 1
        tk_gen = np.hstack([tk_gen,gentk])
    return tk_gen

def tk_filter(tk, args):
    """ Calls filter functions based on the arguments.
        Some arguments have other argument dependencies 
        (e.g. month_range needs year_range) and some are
        not allowed to be together! (e.g. year & year range)
    """
    if 'genesis' in args:
        if args['genesis']:
            tk = filter_genesis(tk)
    
    # Filter by date: Year(s)/Months OR Year_Range
    if 'year' in args and 'hemisphere' in args:
        tk = filter_years(tk, args['year'],args['hemisphere'])
        if 'month' in args:
			tk = filter_months(tk, args['month'])
    elif 'year_range' in args and 'hemisphere' in args:
        if 'month_range' in args:
            tk = filter_yearmonth_range(tk, args['year_range'], 
                args['month_range'], args['hemisphere'])
        elif 'month' in args:
            tk = filter_year_range(tk, args['year_range'],args['hemisphere'])
            tk = filter_months(tk, args['month'])
        else:
            tk = filter_year_range(tk, args['year_range'], 
                args['hemisphere'])
    elif 'date_list' in args and 'hemisphere' in args:
        tk = filter_date_list(tk, args['date_list'], args['hemisphere'])
    
    if 'name' in args:
		# input checking only accepts one name for the moment so the
		# commented lines would actually iterate over the letters in
		# the name rather than over the names
        #for i in args['name']:
            #tk = filter_name(tk, i.upper())
        tk = filter_name(tk, args['name'].upper())
        
    if 'sbox' in args:
        tk = filter_in_bbox(tk, args['sbox'])
    
    if 'bbox' in args:
        tk = filter_bbox(tk, args['bbox'])
            
    if ('pressmin' in args) and ('pressmax' in args):
        tk = filter_pressure(tk, pressure_from=args['pressmin'], 
            pressure_to=args['pressmax'])
    elif 'pressmax' in args:
        tk = filter_pressure(tk, pressure_to=args['pressmax'])
    elif 'pressmin' in args:
        tk = filter_pressure(tk, pressure_from=args['pressmin'])

        
    return tk[['year','name','lat','lon']].copy()

def load_and_filter(args, WPS_CONFIG):
	# Commented because this didn't work in the WPS
    #tk = load_shtc()
    tk= load_csv('tcsh1969-2009.csv')
    return tk_filter(tk, args)
