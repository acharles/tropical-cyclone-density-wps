import unittest
import sys
import os.path
import numpy as np
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import load_filter_csv as lf


CSV_FILE = 'load_filter_csv_unittest.csv'

# Unit Testing
class LoadFilterTest(unittest.TestCase):

    def test_single_year_s(self):
        """ Test 1: year filter - Single Year, Southern Hemisphere """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year':[1969],
                       'hemisphere':'s'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''\
[(1969, 'ALINE', -9.1, 67.8) (1969, 'DELPHINE', -21.0, 42.0)
 (1969, 'DELPHINE', -20.0, 37.1) (1969, 'LOUISE', -11.8, 67.5)]'''
        self.assertEqual(result, expected)
   
    def test_single_year_n(self):
        """ Test 2: year filter - Single Year, Northern Hemisphere """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year':[1969],
                       'hemisphere':'n'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1969, 'ALINE', -9.1, 67.8) (1969, 'DELPHINE', -21.0, 42.0)]'''
        self.assertEqual(result, expected)
        
    def test_yearlist_s(self):
        """ Test 3: year filter - Year List, Southern Hemisphere"""
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year':[1969,1970,1971],
                       'hemisphere':'s'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1969, 'ALINE', -9.1, 67.8) (1969, 'DELPHINE', -21.0, 42.0)\n (1969, 'DELPHINE', -20.0, 37.1) (1969, 'LOUISE', -11.8, 67.5)\n (1969, 'BETSY', -9.7, 64.0) (1970, 'ANDREA', -9.0, 87.0)\n (1970, 'BEVERLEY-EVA', -11.1, 129.0) (1970, 'ROSIE', -18.7, 165.4)\n (1971, 'ODETTE', -6.0, 74.0) (1971, 'RHODA', -8.5, 86.1)\n (1971, 'KITTY', -9.0, 139.0) (1971, 'BELLE', -15.0, 70.0)\n (1971, 'WENDY', -25.9, 159.7) (1971, 'IDA', -8.9, 160.2)]'''
        self.assertEqual(result, expected)    

    def test_yearlist_n(self):
        """ Test 4: year filter - Year List, Northern Hemisphere """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year':[1969,1970,1971],
                       'hemisphere':'n'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1969, 'ALINE', -9.1, 67.8) (1969, 'DELPHINE', -21.0, 42.0)\n (1969, 'DELPHINE', -20.0, 37.1) (1969, 'LOUISE', -11.8, 67.5)\n (1969, 'BETSY', -9.7, 64.0) (1970, 'ANDREA', -9.0, 87.0)\n (1970, 'BEVERLEY-EVA', -11.1, 129.0) (1970, 'ROSIE', -18.7, 165.4)\n (1971, 'ODETTE', -6.0, 74.0) (1971, 'RHODA', -8.5, 86.1)\n (1971, 'KITTY', -9.0, 139.0)]'''
        self.assertEqual(result, expected)
        
    def test_yearrange_s(self):
        """ Test 5: year range filter - Year Range, Southern """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year_range':[1973,1975],
                       'hemisphere':'s'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1973, 'ALICE', -9.0, 81.0) (1973, 'ANNIE', -15.5, 104.2)\n (1973, 'DEIDRE-DALID', -28.8, 74.9) (1974, 'MARCIA', -8.8, 88.8)\n (1974, 'SELMA', -11.0, 127.0) (1974, 'BLANDINE', -14.0, 45.9)\n (1975, 'RAY', -7.0, 102.2) (1975, 'JOAN', -11.6, 128.2)\n (1975, 'CLOTILDE', -8.0, 51.1)]'''
        self.assertEqual(result, expected)
        
    def test_yearrange_n(self):
        """ Test 6: year range filter - Year Range, Northern """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year_range':[1973,1975],
                       'hemisphere':'n'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1972, 'CHARLOTTE', -10.5, 55.0) (1972, 'MARCELLE', -8.8, 81.4)\n (1973, 'ALICE', -9.0, 81.0) (1973, 'ANNIE', -15.5, 104.2)\n (1973, 'DEIDRE-DALID', -28.8, 74.9) (1974, 'MARCIA', -8.8, 88.8)\n (1974, 'SELMA', -11.0, 127.0) (1974, 'BLANDINE', -14.0, 45.9)\n (1975, 'RAY', -7.0, 102.2) (1975, 'JOAN', -11.6, 128.2)]'''
        self.assertEqual(result, expected)
        
    def test_date_list_s(self):
        """ Test 7: date_list filter - Year Range, Southern """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'date_list':[[1970,1]],
                       'hemisphere':'s'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''\
[(1970, 'ROSIE', -18.7, 165.4)]'''
        self.assertEqual(result, expected)
    
    def test_date_list_n(self):
        """ Test 8: date_list filter - Year Range, Northern """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'date_list':[[1970,1]],
                       'hemisphere':'n'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''\
[(1969, 'DELPHINE', -20.0, 37.1)]'''
        self.assertEqual(result, expected)
        
    def test_year_month_s(self):
        """ Test 9: month /w year filter - Month, Year, Southern """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year':[1969],
                       'month':[1],
                       'hemisphere':'s'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''\
[(1969, 'DELPHINE', -20.0, 37.1)]'''
        self.assertEqual(result, expected)
    
    def test_year_month_n(self):
        """ Test 10: month /w year filter - Month, Year, Northern """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year':[1969],
                       'month':[1,8],
                       'hemisphere':'n'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        unexpected = '''\
[(1969, 'DELPHINE', -20.0, 37.1)]'''
        self.assertNotEqual(result, unexpected)
        expected = '''\
[(1969, 'ALINE', -9.1, 67.8)]'''
        self.assertEqual(result, expected)
    
    def test_yearmonth_range_s(self):
        """ Test 11: yearmonth range filter - year_range, month_range Southern """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year_range':[1969,1974],
                       'month_range':[1,1],
                       'hemisphere':'s'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1969, 'DELPHINE', -20.0, 37.1) (1969, 'LOUISE', -11.8, 67.5)\n (1969, 'BETSY', -9.7, 64.0) (1970, 'ANDREA', -9.0, 87.0)\n (1970, 'BEVERLEY-EVA', -11.1, 129.0) (1970, 'ROSIE', -18.7, 165.4)\n (1971, 'ODETTE', -6.0, 74.0) (1971, 'RHODA', -8.5, 86.1)\n (1971, 'KITTY', -9.0, 139.0) (1971, 'BELLE', -15.0, 70.0)\n (1971, 'WENDY', -25.9, 159.7) (1971, 'IDA', -8.9, 160.2)\n (1972, 'BEBE', -7.8, 182.0) (1972, 'ARIANE', -24.0, 60.0)\n (1972, 'CHARLOTTE', -10.5, 55.0) (1972, 'MARCELLE', -8.8, 81.4)\n (1973, 'ALICE', -9.0, 81.0) (1973, 'ANNIE', -15.5, 104.2)\n (1973, 'DEIDRE-DALID', -28.8, 74.9) (1974, 'MARCIA', -8.8, 88.8)\n (1974, 'SELMA', -11.0, 127.0) (1974, 'BLANDINE', -14.0, 45.9)]'''
        self.assertEqual(result, expected)
        
    def test_yearmonth_range_n(self):
        """ Test 12: yearmonth range filter - year_range, month_range Northern """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year_range':[1969,1974],
                       'month_range':[1,1],
                       'hemisphere':'n'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1969, 'ALINE', -9.1, 67.8) (1969, 'DELPHINE', -21.0, 42.0)\n (1969, 'DELPHINE', -20.0, 37.1) (1969, 'LOUISE', -11.8, 67.5)\n (1969, 'BETSY', -9.7, 64.0) (1970, 'ANDREA', -9.0, 87.0)\n (1970, 'BEVERLEY-EVA', -11.1, 129.0) (1970, 'ROSIE', -18.7, 165.4)\n (1971, 'ODETTE', -6.0, 74.0) (1971, 'RHODA', -8.5, 86.1)\n (1971, 'KITTY', -9.0, 139.0) (1971, 'BELLE', -15.0, 70.0)\n (1971, 'WENDY', -25.9, 159.7) (1971, 'IDA', -8.9, 160.2)\n (1972, 'BEBE', -7.8, 182.0) (1972, 'ARIANE', -24.0, 60.0)\n (1972, 'CHARLOTTE', -10.5, 55.0) (1972, 'MARCELLE', -8.8, 81.4)\n (1973, 'ALICE', -9.0, 81.0) (1973, 'ANNIE', -15.5, 104.2)\n (1973, 'DEIDRE-DALID', -28.8, 74.9)]'''
        self.assertEqual(result, expected)
    
    def test_yearrange_month_s(self):
        """ Test 13: month /w year range filter - Month, Year Range, Southern """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year_range':[1969,1971],
                       'month':[1],
                       'hemisphere':'s'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1969, 'DELPHINE', -20.0, 37.1) (1970, 'ROSIE', -18.7, 165.4)\n (1971, 'BELLE', -15.0, 70.0)]'''
        self.assertEqual(result, expected)
        
    def test_yearrange_month_n(self):
        """ Test 14: month /w year range filter - Month, Year Range, Northern """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year_range':[1969,1971],
                       'month':[1],
                       'hemisphere':'n'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1969, 'DELPHINE', -20.0, 37.1) (1970, 'ROSIE', -18.7, 165.4)]'''
        self.assertEqual(result, expected)
        
    def test_name(self):
        """ Test 15: name filter - name (case insentitive) """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'name':"TOM"}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1977, 'TOM', -8.5, 154.6) (1977, 'TOM', -17.5, 147.5)]'''
        self.assertEqual(result, expected)
        filter_args = {'name':"tom"}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        self.assertEqual(result, expected)
        
    def test_sbox(self):
        """ Test 16: sbox filter - sbox """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'sbox':(150,-8,155,0)}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1990, 'LISA', -6.0, 155.0) (1990, 'LISA', -7.0, 155.0)\n (1990, 'LISA', -8.0, 155.2) (1992, 'ADEL', -6.3, 156.0)\n (1992, 'ADEL', -7.7, 154.2) (1992, 'ADEL', -8.7, 153.1)\n (1992, 'ADEL', -9.1, 150.7) (1993, 'THEODORE', -7.0, 155.0)\n (1993, 'THEODORE', -39.0, 203.0) (2001, 'UPIA', -7.5, 154.0)\n (2001, 'UPIA', -8.2, 153.5) (2001, 'UPIA', -9.1, 153.8)\n (2001, 'UPIA', -10.4, 155.9) (2002, 'EPI', -7.3, 154.2)]'''
        self.assertEqual(result, expected)
        
    def test_bbox(self):
        """ Test 17: bbox filter - bbox """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'bbox':(150,-8,155,0)}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1990, 'LISA', -6.0, 155.0) (1990, 'LISA', -7.0, 155.0)\n (1992, 'ADEL', -7.7, 154.2) (1993, 'THEODORE', -7.0, 155.0)\n (2001, 'UPIA', -7.5, 154.0) (2002, 'EPI', -7.3, 154.2)]'''
        self.assertEqual(result, expected)
        
    def test_sbox_bbox(self):
        """ Test 18: sbox & bbox filter - sbox with bbox """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'sbox':(150,-8,155,0),
                       'bbox':(150,-8,154.9,0)}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1992, 'ADEL', -7.7, 154.2) (2001, 'UPIA', -7.5, 154.0)\n (2002, 'EPI', -7.3, 154.2)]'''
        self.assertEqual(result, expected)
            
    def test_pressmin(self):
        """ Test 19: min pressure filter - pressmin"""
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'pressmin':1016}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(2000, 'UNNAMED-4', -21.5, 41.1) (2007, 'KAMBA', -31.4, 77.0)]'''
        self.assertEqual(result, expected)
        
    def test_pressmax(self):
        """ Test 20: max pressure filter - pressmax """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'pressmax':899}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1981, 'CHRIS-DAMIA', -13.0, 77.3) (1981, 'CHRIS-DAMIA', -15.0, 73.2)\n (1981, 'CHRIS-DAMIA', -17.0, 69.4) (2002, 'ZOE', -12.5, 169.5)\n (2003, 'GAFILO', -14.8, 52.5) (2003, 'GAFILO', -14.9, 51.6)]'''
        self.assertEqual(result, expected)
        
    def test_pressminmax(self):
        """ Test 21: pressure range filter - pressmin & pressmax """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year_range':[1970,1980],
                       'pressmin':1008, 
                       'pressmax':1010,
                       'hemisphere':'s'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1971, 'RHODA', -8.5, 86.1) (1975, 'RAY', -7.0, 102.2)\n (1978, 'ROSA', -10.3, 152.1) (1978, 'ROSA', -11.0, 149.7)]'''
        self.assertEqual(result, expected)
        
    def test_genesis(self):
        """ Test 22: genesis test """
        tk = lf.load_csv(CSV_FILE)
        filter_args = {'year':[1969],
                   'hemisphere':'s',
                   'genesis':'True'}
        tk = lf.tk_filter(tk, filter_args)
        result = str(tk)
        expected = '''[(1969, 'DELPHINE', -21.0, 42.0) (1969, 'ALINE', -9.1, 67.8)\n (1969, 'LOUISE', -11.8, 67.5)]'''
        self.assertEqual(result, expected)
        
if __name__ == '__main__': 
    unittest.main()
