"""
This file doesn\'t check the result of the execution of the sql statements, it 
only checks if the sql statements are the ones that are expected.
"""
# Make the input_validator module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import load_filter_db as lfd


DATABASE_TABLE = 'tracker.tcdata_points'

class TestSQL(unittest.TestCase):
    """Methods testing the functions building the sql query"""

    def test_filter_southern_hemisphere_year(self):
        year = 1985
        actual_output = []
        expected_output = ["datetime >= '1985-07-01'", "datetime < '1986-07-01'"]
        lfd.filter_southern_hemisphere_year(year, actual_output)
        self.assertEqual(expected_output, actual_output)
        
    def test_filter_years(self):
        years = [1992,1996,1997]
        hemisphere = 'S'
        actual_output = []
        expected_output = ["((datetime >= '1992-07-01' AND datetime < '1993-07-01') "
        "OR (datetime >= '1996-07-01' AND datetime < '1997-07-01') "
        "OR (datetime >= '1997-07-01' AND datetime < '1998-07-01'))"]
        lfd.filter_years(years, hemisphere, actual_output)
        self.assertEqual(expected_output, actual_output)
        
        years = [1992,1996,1997]
        hemisphere = 'N'
        actual_output = []
        expected_output = ["EXTRACT(year FROM datetime) in (1992, 1996, 1997)"]
        lfd.filter_years(years, hemisphere, actual_output)
        self.assertEqual(expected_output, actual_output)
        
    def test_filter_year_range(self):
        year_range = [1992,1997]
        hemisphere = 'S'
        actual_output = []
        expected_output = ["(datetime >= '1992-07-01')", "(datetime < '1998-07-01')"]
        lfd.filter_year_range(year_range, hemisphere, actual_output)
        self.assertEqual(expected_output, actual_output)
        
        year_range = [1992,1997]
        hemisphere = 'N'
        actual_output = []
        expected_output = ["EXTRACT(year FROM datetime) >= 1992", 
                           "EXTRACT(year FROM datetime) <= 1997"]
        lfd.filter_year_range(year_range, hemisphere, actual_output)
        self.assertEqual(expected_output, actual_output)
        
    def test_filter_yearmonth_range(self):
        year_range, month_range = [1992,1997], [2,1]
        hemisphere = 'N'
        actual_output = []
        expected_output = ["datetime >= '1992-2-01'", "datetime < '1997-2-01'"]
        lfd.filter_yearmonth_range(year_range, month_range, hemisphere, 
                                   actual_output)
        self.assertEqual(expected_output, actual_output)
        
        year_range, month_range = [1992,1997], [2,1]
        hemisphere = 'S'
        actual_output = []
        expected_output = ["datetime >= '1993-2-01'", "datetime < '1998-2-01'"]
        lfd.filter_yearmonth_range(year_range, month_range, hemisphere, 
                                   actual_output)
        self.assertEqual(expected_output, actual_output)
        
        year_range, month_range = [1992,1997], [7,8]
        hemisphere = 'S'
        actual_output = []
        expected_output = ["datetime >= '1992-7-01'", "datetime < '1997-9-01'"]
        lfd.filter_yearmonth_range(year_range, month_range, hemisphere, 
                                   actual_output)
        self.assertEqual(expected_output, actual_output)
        
        year_range, month_range = [1992,1997], [1,8]
        hemisphere = 'S'
        actual_output = []
        expected_output = ["datetime >= '1993-1-01'", "datetime < '1997-9-01'"]
        lfd.filter_yearmonth_range(year_range, month_range, hemisphere, 
                                   actual_output)
        self.assertEqual(expected_output, actual_output)
        
        year_range, month_range = [1992,1997], [7,1]
        hemisphere = 'S'
        actual_output = []
        expected_output = ["datetime >= '1992-7-01'", "datetime < '1998-2-01'"]
        lfd.filter_yearmonth_range(year_range, month_range, hemisphere, 
                                   actual_output)
        self.assertEqual(expected_output, actual_output)
        
    def test_filter_month(self):
        month = [1]
        actual_output = []
        expected_output = ["EXTRACT(month FROM datetime) = 1"]
        lfd.filter_month(month, actual_output)
        self.assertEqual(expected_output, actual_output)
        
        month = [1,6,7]
        actual_output = []
        expected_output = ["EXTRACT(month FROM datetime) in (1, 6, 7)"]
        lfd.filter_month(month, actual_output)
        self.assertEqual(expected_output, actual_output)
        
    def test_filter_name(self):
        name = "TOM"
        actual_output = []
        expected_output = ["name = 'TOM'"]
        lfd.filter_name(name, actual_output)
        self.assertEqual(expected_output, actual_output)
    
    def test_filter_bbox(self):
        bbox = [60,-20,70,-10]
        actual_output = []
        expected_output = ["lon >= 60", "lat >= -20", "lon <= 70", "lat <= -10"]
        lfd.filter_bbox(bbox, actual_output)
        self.assertEqual(expected_output, actual_output) 
        
    def test_filter_sbox(self):
        bbox = [60,-20,70,-10]
        actual_output = []
        expected_output =["tc_id in "
                         "(SELECT tc_id FROM {table} "
	                     "WHERE lon >= 60 AND lat >= -20 "
                         "AND lon <= 70 AND lat <= -10)".format(\
                         table=DATABASE_TABLE)]
        lfd.filter_sbox(bbox, DATABASE_TABLE, actual_output)
        self.assertEqual(expected_output, actual_output) 
        
    def test_filter_minpress(self):
        pressmin = 900
        actual_output = []
        expected_output =["pressure >= 900"]
        lfd.filter_minpress(pressmin, actual_output)
        self.assertEqual(expected_output, actual_output) 
        
    def test_filter_maxpress(self):
        pressmax = 1000
        actual_output = []
        expected_output =["pressure <= 1000"]
        lfd.filter_maxpress(pressmax, actual_output)
        self.assertEqual(expected_output, actual_output) 
        
    def test_filter_date_list(self):
        date_list = [[1990,12], [1985,3]]
        hemisphere = 'N'
        actual_output = []
        expected_output = ["((EXTRACT(year FROM datetime) = 1990 AND "
                           "EXTRACT(month FROM datetime) = 12) OR "
                           "(EXTRACT(year FROM datetime) = 1985 AND "
                           "EXTRACT(month FROM datetime) = 3))"]
        lfd.filter_date_list(date_list, hemisphere, actual_output)
        self.assertEqual(expected_output, actual_output) 
        
        date_list = [[1990,12], [1985,3]]
        hemisphere = 'S'
        actual_output = []
        expected_output = ["((EXTRACT(year FROM datetime) = 1990 AND "
                           "EXTRACT(month FROM datetime) = 12) OR "
                           "(EXTRACT(year FROM datetime) = 1986 AND "
                           "EXTRACT(month FROM datetime) = 3))"]
        lfd.filter_date_list(date_list, hemisphere, actual_output)
        self.assertEqual(expected_output, actual_output) 
        
    def test_filter_genesis(self):
        genesis = True
        actual_output = []
        expected_output =["id in (SELECT id "
        "FROM {table} A "
        "INNER JOIN "
        "(SELECT tc_id, min(datetime) as min_datetime "
        "FROM {table} "
        "WHERE pressure IS NULL OR pressure <= 995 "
        "GROUP BY tc_id) B ON (A.tc_id=b.tc_id) "
        "WHERE A.datetime = B.min_datetime "
        ")".format(table=DATABASE_TABLE)]
        lfd.filter_genesis(genesis, DATABASE_TABLE, actual_output)
        self.assertEqual(expected_output, actual_output) 
    
if __name__ == '__main__':
        unittest.main()
