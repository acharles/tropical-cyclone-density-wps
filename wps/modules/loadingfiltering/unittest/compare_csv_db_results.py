"""
This module checks if the output data from database loading/filtering is the
same as the output data from csv loading/filtering.
"""
# Make the input_validator module accessible from this subdirectory
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), 
    os.path.pardir)))
import unittest
import load_filter_db as lfd
import load_filter_csv as lf
import numpy as np

CSV_FILE = '../../../tcsh1969-2009.csv'
WPS_CONFIG = {'use_database': True, 'database_table': 'tracker.tcdata_points', 'database_password': 'postgres', 'database_name': 'tc', 'database_user': 'postgres'}

class DbCsvTest(unittest.TestCase):
    
    def setUp(self):
        lfd.ORDER_FIXES = True
        lf.ORDER_FIXES = True
    
    def check_equality(self, filters):
        # Loading/filtering from csv file:
        tk = lf.load_csv(CSV_FILE)
        csv_output = lf.tk_filter(tk, filters)
        # Loading/filtering from database
        db_output = lfd.load_and_filter(filters, WPS_CONFIG)
        # Sort using all the fields so that the list of fixes look the same
        csv_output = np.sort(csv_output, order=['year', 'seq', 'lat', 'lon'])
        db_output = np.sort(db_output, order=['year', 'seq', 'lat', 'lon'])
        # Test
        #print(len(csv_output.tolist()))
        #print(len(db_output.tolist()))
        self.assertEqual(csv_output.tolist(), db_output.tolist())

    def test_genesis(self):
        filter_args = {'genesis': True, 'hemisphere':'s'}
        self.check_equality(filter_args)

    def test_pressmax(self):
        filter_args = {'pressmax': 1000, 'hemisphere':'s'}
        self.check_equality(filter_args)

    def test_year(self):
        filter_args = {'year': [1969, 2004], 'hemisphere':'s'}
        self.check_equality(filter_args)
        
    def test_yearmonth(self):
        filter_args = {'year': [1969, 2004], 'month': [1], 'hemisphere':'s'}
        self.check_equality(filter_args)
        
    def test_yearrange(self):
        filter_args = {'year_range': [1970, 1980], 'hemisphere':'s'}
        self.check_equality(filter_args)
        
    def test_yearrange(self):
        filter_args = {'year_range': [1970, 1980], 'hemisphere':'s'}
        self.check_equality(filter_args)
        
    def test_yearmonthrange(self):
        filter_args = {'year_range': [1970, 1980], 'month_range': [2,10], \
                       'hemisphere':'s'}
        self.check_equality(filter_args)
        
    def test_datelist(self):
        filter_args = {'datelist': [[1980, 01], [1990, 02], [2000, 01]], \
                       'hemisphere':'s'}
        self.check_equality(filter_args)
    
    def test_bbox(self):
        filter_args = {'bbox': [60,-20,70,-10], 'hemisphere':'s'}
        self.check_equality(filter_args)
        
    def test_sbox(self):
        filter_args = {'sbox': [60,-20,70,-10], 'hemisphere':'s'}
        self.check_equality(filter_args)
        
    def test_pressmin(self):
        filter_args = {'pressmin': 980, 'hemisphere':'s'}
        self.check_equality(filter_args)


if __name__ == '__main__': 
    unittest.main()
