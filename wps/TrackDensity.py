"""
This module is the entry point to the WPS, it is designed to work with 
the ZOO WPS platform.

Input:
Refer to http://zoo-project.org/docs/services/howtos.html for more information.

conf: Dictionary containing information about the environment and the WPS.
Most of the information in this dictionary will be filled by the ZOO platform.
It is also used to return an error message to the user.

Both the input and output arguments will be pre-filled by ZOO using the zcfg 
configuration file.
input: Dictionary containing the user's arguments, filled by the ZOO platform.
output: Dictionary containing the output of the WPS, the value of the 
output variable will be set by the WPS (output["Result"]["value"] = stuff)

arguments in 'input': 
Each argument will have the form 'arg': {'value': somevalue}
More key-value pairs other than 'value' are set by ZOO but they are not important.
List of accepted values for input arguments:
'hemisphere': 'S' or 'N' (mandatory)
'year': '1991,1987', there must be 1+ year specified
'month': '1,5,7', there must be 1+ month specified
'year_range': '1980, 1990', there must be exactly 2 years specified
'month_range': '2,5', there must be exactly 2 months specified
'date_list': '197612,197805', there must be at least one year-month pair specified
'name': 'ALICE'
'sbox': '1,-10,10,0', minlon, minlat, maxlon, maxlat
'bbox': '1,-10,10,0', minlon, minlat, maxlon, maxlat
'pressmin': '990'
'pressmax': '1000'
'genesis': 'True'
'res_x': '0.5', number between 0 and 1
'res_y': '0.5', number between 0 and 1
'output_format': 'NETCDF' (default 'NETCDF')
'bandwidth': '3.0' (default '3.0')
"""
import modules.config.config_loading as cl
import modules.inputchecking.input_checking as ic
import modules.loadingfiltering.load_filter as lf
import modules.fixprocessor.fix_processor_cached as fp
import modules.outputformat.output as of
import logging as log


def TrackDensity(conf, input, output):
    """ Entry point for the WPS process: compute tropical cyclone density 
    estimation, saves it in a file and returns a path to that file.
    
    Inputs:
        The inputs are the ones required by the Zoo platform:
        See the module information at the beginning of this file.
        The path to the file saved is stored in the output dictionary.
    Output:
        3 if the process was successful
        4 if the process was unsuccessful"""
    # Load the configuration file
    WPS_CONFIG = cl.load_config()
    # Check and return a list of correct arguments
    valid_arguments = input_checking(input, conf)
    if valid_arguments == None:
        # input checking has failed
        return 4
    # Initialise logging
    init_logging(WPS_CONFIG, input, valid_arguments)
    # Get the function to call to use the process (cached or not)
    process_function = get_process_function(WPS_CONFIG)
    # Load/Filter/Process/Get output data
    result = process_function(valid_arguments, WPS_CONFIG)
    conf["lenv"]["message"] = result[1]
    output["Result"]["value"] = result[2]
    return result[0]

def input_checking(input, conf):
    """ Check that the arguments provided by the user are correct
    
    If the arguments are correct, return a list of converted arguments.
    Input: 
        input: Python dictionary of args for the process passed through HTML
    Output: 
        valid_arguments is a python dictionary containing arguments
        converted from text to the expected data type.
    """
    # Dictionary that will contain valid arguments (after check_arguments)
    valid_arguments = None
    try:
        # Check the arguments to make sure they are valid, and return 
        # a dictionary of valid arguments or throw an exception
        valid_arguments = ic.check_arguments(input)
    except ValueError as exc:
        conf["lenv"]["message"] = str(exc)
        return None
    return valid_arguments
        
def init_logging(WPS_CONFIG, input, valid_arguments):
    """ This function sets up the logging according to the configuration. """
    # Change the logging level according to the debug variable from the 
    # configuration file
    if WPS_CONFIG['debug']:
        log.basicConfig(filename=WPS_CONFIG['log_file'], level=log.DEBUG)
    else:
        log.basicConfig(filename=WPS_CONFIG['log_file'], level=log.ERROR)
    # Change the output of the log entries
    log.basicConfig(format='%(levelname)s:%(asctime)s %(message)s')
    # Put some info in the log about the input and configuration:
    if WPS_CONFIG['debug']:
        #log.debug('input: ' + str(input) + '\n')
        log.debug('valid_arguments: ' + str(valid_arguments) + '\n')
        #log.debug('config: ' + str(WPS_CONFIG) + '\n')

def get_process_function(WPS_CONFIG):
    """ Get a function object to call to get the output of the process.
    
    The function will either be a function wrapper that can return cached data
    if caching is enabled or the process function directly if caching is 
    disabled.
    """
    if WPS_CONFIG['use_caching']:
        from beaker.cache import CacheManager
        
        # Cache in a DBM file, in the cache_directory from the config file:
        cache = CacheManager(type='dbm', data_dir=WPS_CONFIG['cache_directory'])
        
        # Beaker caches the result of a function, so we need a wrapper function
        # There is only 'input' as a parameter so that the cache is the same 
        # even with different conf, output, and WPS_CONFIG
        @cache.cache('TrackDensityMainNamespace', expires='never')
        def process_wrapper(valid_arguments, WPS_CONFIG):
            result = track_density_process(valid_arguments, WPS_CONFIG)
            return result
        return process_wrapper
    else:
        return track_density_process

def track_density_process(valid_arguments, WPS_CONFIG):
    """This function implements the track density process.
    
    Inputs:
    -valid_arguments: Valid input arguments for the process
    -WPS_CONFIG: Parameters from the configuration file for this process
    
    Outputs:
    A tuple (x, y, z) where 
    -x: return value of the process: 3 or 4 (as defined by the ZOO platform)
    -y: error message
    -z: output of the process
    """
    # String contaning the representation of the data in the desired  format:
    output_data = None
    try:
        # Numpy array containing fixes:
        fixes=None
        # Read from/filter data
        fixes = lf.load_and_filter(valid_arguments, WPS_CONFIG)
        if WPS_CONFIG['debug']:
            log.debug('Number of fixes: ' + str(len(fixes)) + '\n')
            #log.debug('Fixes: ' + str(fixes) + '\n')
            
        # A tuple contaning the result of the processing:
        result = None
        # Processing of the data
        result = fp.process(fixes, valid_arguments, WPS_CONFIG)
                            
        # Save the processed data in a specific format, get the location
        output_data = of.output_data(result, valid_arguments, WPS_CONFIG)
    except Exception as exc:
        import traceback
        error_message = traceback.format_exc() + '\n\n  Input:' + str(input)
        log.error(error_message)
        if not WPS_CONFIG['debug']:
            error_message = "Internal Error"
        return (4, error_message, '')
    return (3, '', output_data)
