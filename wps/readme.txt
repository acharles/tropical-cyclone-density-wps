0. Installing on Fedora
1. Dependencies
2. Database installation/configuration
3. Optimizations
4. Caching
5. Configuration
6. Test run
7. Zoo platform installation
8. WPS test run
9. Troubleshooting
10. Files in this directory


*****0. Installing on Fedora*****
To install on fedora, use the script ../tools/install_fedora.sh
It automatically does the steps 1, 2, 3, 4, 5, 7
The steps described in this readme file have been tested for ubuntu.

*****1. Dependencies*****
Before using the application, install the following libraries:
scipy (http://www.scipy.org/install.html), 
h5py, netCDF4 and basemap.

On ubuntu, use the following commands:
$ sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose
$ sudo apt-get install python-pip
$ sudo pip install h5py
$ sudo pip install netCDF4==1.0.4
$ sudo pip install netCDF4 --upgrade
$ sudo pip install cython
$ sudo apt-get install python-mpltoolkits.basemap


*****2. Database installation/configuration*****
By default, the wps uses the csv file to get the fixes,
In order to use a database locally, you need postgresql and psycopg:
sudo apt-get install postgresql
easy_install psycopg2
Modify the password for the database system and create a database:
$ sudo -u postgres psql
postgres=# ALTER ROLE postgres WITH ENCRYPTED PASSWORD 'postgres';
postgres=# CREATE DATABASE tc WITH ENCODING 'UTF-8';
postgres=# \q
In order to populate the database:
Modify the "DATABASE DETAILS" lines in bom/tools/create_tc_storage.bash with:
DB_NAME=tc
SCHEMA_NAME=tracker
PG_USER=postgres
PG_PASSWORD=postgres
PG_HOST=localhost
PG_PORT=5432
Then, run those commands (from bom/wps)
$ cp tcsh1969-2009.csv ../tools/
$ cd ../tools/
$ mv tcsh1969-2009.csv tcdata.csv
$ sudo chmod 700 create_tc_storage.bash
$ ./create_tc_storage.bash
This might show some errors that can be ignored if the tables have been created.
To test if the table we need was created, run:
$ sudo -u postgres psql -d tc -U postgres
(Connect to the database tc as postgres, password for postgres should be postgres)
tc=# SELECT * FROM tracker.tcdata_points LIMIT 5;
5 rows of data should be printed, if not, the script was unsuccessful.
tc=# \q
$ rm tcsh1969-2009.csv
Finally, modify bom/wps/TrackDensityConfig.ini to tell the program to use the 
database:
Change "use_database = False" to "use_database = True"


*****3. Optimizations*****
For better performance, you can use cython:
(Make sure cython is installed: sudo pip install cython)
Since cython uses c, the code needs to be compiled: run
$ cd modules/fixprocessor
$ sudo -u www-data python setup.py build_ext --inplace
$ cd ../..
Now, you can set use_cython to True in the configuration file 
TrackDensityConfig.ini


*****4. Caching*****
By default, caching is off.
There are 2 types of caching:
Caching for the fix processor (caching the result of the processing)
Caching for the whole process (caching the name of the output files)
To enable caching, install Beaker:
$ sudo pip install beaker
Then, you need to specify the directories used for caching in the configuration
file.
If you want to use the default ones,
Create the directory:
$ sudo mkdir /var/cache/TrackDensity
Then, make sure the user that will execute the WPS can create/read files in 
the directory (www-data is the apache user/group):
$ sudo chown www-data:www-data /var/cache/TrackDensity
Finally, modify the config file and set the use_caching variable to True.
Note that if you don't run the WPS as www-data (for example using command line), 
you might have permissions issues, you can use "sudo su www-data"


*****5. Configuration*****
The application needs to know where to save the files:
Modify bom/wps/TrackDensityConfig.ini with the directories where the files need
to be saved. If the application is not run on a server, you can set the 
output directories and server directories to be the same.
If contour-wms is run on the same server, set the output directory for 
netcdf files to the directory that pydap uses to serve files.
Set the path to the log file (make sure the permissions are set correctly) 
in the configuration file. This file will



*****6. Test run*****
Now, go to bom/wps and run the following:
$ sudo -u www-data python
import TrackDensity as td
conf = {'lenv': {'message': ''}}
inputs = {'output_format': {'value': 'NetCDF'},\
		  'hemisphere': {'value': 'S'},\
		  'year_range': {'value': '1969,1971'}}
out = {'Result': {'value' : ''}}
td.TrackDensity(conf,inputs,out)

The output should be 3.
If it is 4, you can use print(conf) to see the error message
(You might have to set the DEBUG flag in bom/wps/TrackDensityConfig.ini to
see the actual exception message if you see "Internal Error".)
If the output is neither then a library must be missing.
See the troubleshooting section for more.

If the output is 3, the application should have saved the output in a file and 
returned the location of this file on the server in out['Result']['value'].


*****7. Zoo platform installation*****
To be a WPS, the application needs the Zoo platform, follow the instructions in
INSTALL-ZOO-VM.txt (skip to "Steps") or go to the page
http://www.zoo-project.org/docs/kernel/installation.html
Since the WPS has a lot of files that the user should not access or execute
directly, you can modify the apache config file 
(/etc/apache2/sites-available/default on Ubuntu) to only allow the execution 
of the scripts you choose.
To use mod_rewrite, run the command:
$ sudo a2enmod headers
Below is an example of the pqrt of the config file to restrict access:
ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
<Directory "/usr/lib/cgi-bin">
        RewriteEngine On
        RewriteRule .cgi$ - [L,NC]
        RewriteRule .*$ - [F,L,NC]
        AllowOverride None
        Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
        Order allow,deny
        Allow from all
</Directory>
The RewriteRules will only allow the scripts which have their filenames ending
with cgi to be executed.


*****8. WPS test run*****
Then, put all the files in bom/wps in /var/lib/cgi-bin
You should be able to access the WPS at the following address:
http://localhost/zoo/?Service=WPS&Request=Execute&Version=1.0.0&Identifier=TrackDensity&DataInputs=hemisphere=S;year=1980


*****9. Troubleshooting*****
The WPS doesn't create files and throws an error:
The permissions are probably not set correctly on the 
output directories specified in bom/wps/TrackDensityConfig.ini, make sure that
the user www-data (the apache user) can write files in those directories.

netcdf4:
The netCDF4 library often causes problems, installing the dependencies on
64 bit ubuntu through the package manager creates a broken build at the moment
of the development. To fix the issue when importing netdcf4,
run the command:
$ sudo pip install netCDF4==1.0.4
However, if the WPS is deployed with a Pydap server, using an old version
of netdf4-python might prevent Pydap from working:
Pydap doesn't need python-netcdf4 to serve netCDF files but if it is there, 
it will use it.
We can modify the Pydap code (since it's in python) to remove the netCDF4 
dependency.
However the way the code works is that it first tries to use PyNIO, 
if that fails, it tries with python-netcdf4, if that fails, 
it tries with Scientific.IO.NetCDF, if that fails, it tries with pynetcdf, 
and finally if that fails, it tries with pupynere.
You can try to install PyNIO, however, this library seems to be out of date 
and I could not find an easy way to install it.
A solution is to remove the code using NetCDF4 from Pydap:
Here is how to proceed to find the netcdf reference in Pydap:
***
Find the directory where pydap is installed:
$ sudo find / -name "*pydap*"
It might look like: /usr/local/lib/python2.7/dist-packages/pydap
Go to that directory:
$ cd /usr/local/lib/python2.7/dist-packages/pydap
Then find in what file pydap refers to NetCDF4:
$ grep -r "netCDF4" *
It might be: handlers/netcdf/__init__.py
Modify that file: I just remove the code where the module uses netCDF4:
$ sudo nano handlers/netcdf/__init__.py
***


*****10. Files in this directory*****
INSTALL-ZOO-VM.txt:
Contains instructions on how to install the ZOO platform on Ubuntu.
TrackDensity.py:
The python module that is the starting point of the WPS.
It uses the modules contained in the modules directory.
main.cfg:
The ZOO platform configuration file for the whole WPS:
A WPS can have more than one process, however, we only use one (TrackDensity).
It is used for the GetCapabilities request and to set some WPS options.
This file is to be put in /var/lib/cgi-bin.
tcsh1969-2009.csv:
The csv file containing data about the cyclones.
It is not used if the database is used instead.
TrackDensityConfig.ini:
Configuration of the WPS process TrackDensity: TrackDensity.py uses it to load
various configuration options.
TrackDensity.zcfg:
Configuration of the WPS at the ZOO layer:
It will be used to populate the XML files returned by DescribeProcess and 
Execute requests.
