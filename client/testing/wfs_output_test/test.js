var WFSURL = "http://115.146.84.158/cyclonetracks2/index2.php";

var reqXML, btn_case3, btn_case4, msg;

function loadXML(id){
    var fileName = null;
    var xmlData = null;
    switch(id) {
        case 1:
          fileName = "year.xml"; break;
        case 2:
          fileName = "bbox.xml"; break;
        case 3:
          fileName = "year_list.xml"; break;
        case 4:
          fileName = "year_range.xml"; break;
        case 5:
          fileName = "pressure.xml"; break;
        case 6:
          fileName = "date_list.xml"; break;
        default:
          break;
    }
    jQuery.ajaxSetup({async:false});
    $.get( fileName, function(data) {
        xmlData = data;
    },"text")	
    jQuery.ajaxSetup({async:true});
    return xmlData;
}

$(document).ready(function(){
    reqXML = $('#reqXML');
    btn_case3 = $('#btn_case3');
    btn_case4 = $('#btn_case4');
    msg = $('#msg');

    $('input[type="button"]').click(function() {
        var id = $(this).data('id');
        var xmlData = loadXML(id);
        reqXML.val(xmlData);
        msg.html('Test case '+ id +' sending request..');
        $.ajax({
            type: 'POST',
            url: WFSURL,
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            data: xmlData,
            success: function(resp) {
                if(resp.type != 'FeatureCollection'){
                    var message = "The server returned unexpected data: ";
                    message = message + "<br/> the output was: <br/>";
                    message = message + resp;
                    msg.html(message);
                    return;
                }
                resp = newCycloneCollection(resp);// Parse fixes to cyclones.
                if(id == '1') testcase1(resp);
                if(id == '2') testcase2(resp);
                if(id == '3') testcase3(resp);
                if(id == '4') testcase4(resp);
                if(id == '5') testcase5(resp);
                if(id == '6') testcase6(resp);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status + " : " + thrownError);
            },
            cache: false,
            contentType: 'application/xml',
            processData: false
        });
    });
});

/**
 *  The rule of test case 1:
 *  For each cyclone:
 *  - it's year has to be 2004
 **/
function testcase1(resp) {
    if(resp.features.length > 0) {
        msg.html('Test case 3 response received. Parsing..');

        var testResult = true;
        for(var i=0; i<resp.features.length; i++) {
            // Looping all cyclones
            var cyclone = resp.features[i];
            for(var j=0; j<cyclone.geometry.coordinates.length; j++) {
                // Looping all coordinates
                var coordinate = cyclone.geometry.coordinates[j];
                var datetime = coordinate.datetime;
                // southern hemisphere year
                if(datetime.indexOf('2004')==0 || datetime.indexOf('2005')==0) {
                    // This coordinate is valid
                    // console.log('valid: ' + datetime);
                } else {
                    // This coordinate is invalid!
                    testResult = false;
                    console.log('Invalid: ' + datetime);
                }
                if(!testResult) break;
            }
            if(!testResult) break;
        }

        if(testResult) msg.html('Test case 1 passed! For each cyclone, all coordinates match:<ur><li>the year is 2004</li></ur>');
        else msg.html('Test case 1 failed.');
    } else {
        msg.html('No response from the XML request.');
    }
}

/**
 *  The rule of test case 2:
 *  For each cyclone:
 *  - all coordinates are in the bbox area.
 **/
function testcase2(resp) {
    if(resp.features.length > 0) {
        msg.html('Test case 3 response received. Parsing..');

        var testResult = true;
        for(var i=0; i<resp.features.length; i++) {
            // Looping all cyclones
            var cyclone = resp.features[i];
            for(var j=0; j<cyclone.geometry.coordinates.length; j++) {
                // Looping all coordinates
                var singleMatch = false;
                var coordinate = cyclone.geometry.coordinates[j];
                var x = parseInt(coordinate[0]);
                var y = parseInt(coordinate[1]);
                if(!((x >= 65) && (x < 70) && (y <= -5) && (y >= -10))) {
                    testResult = false;
                    break;
                    // This coordinate is invalid!
                    console.log('Invalid: ' + "cyclone " + i + " coordinates " + j);
                }
                if(!testResult) break;
            }
            if(!testResult) break;
        }

        if(testResult) msg.html('Test case 2 passed! For each cyclone:<ur><li>all fixes are in the bbox area</li></ur>');
        else msg.html('Test case 2 failed.');
    } else {
        msg.html('No response from the XML request.');
    }
}

/**
 *  The rule of test case 3:
 *  For each cyclone:
 *  - it's year has to be 1980 or 1982, or
 *  - it's month has to be Jan or Feb
 **/
function testcase3(resp) {
    if(resp.features.length > 0) {
        msg.html('Test case 3 response received. Parsing..');

        var testResult = true;
        for(var i=0; i<resp.features.length; i++) {
            // Looping all cyclones
            var cyclone = resp.features[i];
            for(var j=0; j<cyclone.geometry.coordinates.length; j++) {
                // Looping all coordinates
                var coordinate = cyclone.geometry.coordinates[j];
                var datetime = coordinate.datetime;
                if(datetime.indexOf('1980')==0 || datetime.indexOf('1982')==0) {
                    // This coordinate is valid
                    // console.log('valid: ' + datetime);
                } else {
                    var mon = datetime.substring(5, 2);// Get the month
                    if(mon=='01' || mon=='02') {
                        // This coordinate is valid
                        console.log('Valid month: ' + datetime);
                    } else {
                        // This coordinate is invalid!
                        testResult = false;
                        console.log('Invalid: ' + datetime);
                    }
                }
                if(!testResult) break;
            }
            if(!testResult) break;
        }

        if(testResult) msg.html('Test case 3 passed! For each cyclone, all coordinates match:<ur><li>the year is 1980/1982, or</li><li>the month is Jan/Feb</li></ur>');
        else msg.html('Test case 3 failed.');
    } else {
        msg.html('No response from the XML request.');
    }
}

/**
 *  The rule of test case 4:
 *  For each cyclone:
 *  - 1980 <= year < 1986
 **/
function testcase4(resp) {
    if(resp.features.length > 0) {
        msg.html('Test case 4 response received. Parsing..');

        var testResult = true;
        for(var i=0; i<resp.features.length; i++) {
            // Looping all cyclones
            var cyclone = resp.features[i];
            for(var j=0; j<cyclone.geometry.coordinates.length; j++) {
                // Looping all coordinates
                var coordinate = cyclone.geometry.coordinates[j];
                var datetime = coordinate.datetime;
                var year = parseInt(datetime.substring(0, 4));
                if((year >= 1980) && (year < 1986)) {
                    // This coordinate is valid
                    // console.log('valid: ' + datetime);
                } else {
                    // Invalid coordinate
                    testResult = false;
                    console.log('Invalid: ' + datetime);
                }
                if(!testResult) break;
            }
            if(!testResult) break;
        }

        if(testResult) msg.html('Test case 4 passed! For each cyclone, all coordinates match:<ur><li>1980 <= year < 1986</li></ur>');
        else msg.html('Test case 4 failed.');
    } else {
        msg.html('No response from the XML request.');
    }
}

/**
 *  The rule of test case 5:
 *  For each cyclone:
 *  - 950 <= pressure <= 951
 **/
function testcase5(resp) {
    if(resp.features.length > 0) {
        msg.html('Test case 5 response received. Parsing..');

        var testResult = true;
        for(var i=0; i<resp.features.length; i++) {
            // Looping all cyclones
            var cyclone = resp.features[i];
            for(var j=0; j<cyclone.geometry.coordinates.length; j++) {
                // Looping all coordinates
                var coordinate = cyclone.geometry.coordinates[j];
                var pressure = parseInt(coordinate.pressure);
                if((pressure >= 950) && (pressure <= 951)) {
                    // This coordinate is valid
                    // console.log('valid: ' + pressure);
                } else {
                    // Invalid coordinate
                    testResult = false;
                    console.log('Invalid: ' + pressure);
                }
                if(!testResult) break;
            }
            if(!testResult) break;
        }

        if(testResult) msg.html('Test case 5 passed! For each cyclone, all coordinates match:<ur><li>950 <= pressure <= 951</li></ur>');
        else msg.html('Test case 5 failed.');
    } else {
        msg.html('No response from the XML request.');
    }
}

/**
 *  The rule of test case 6:
 *  For each cyclone:
 *  - the month is either 02/1980 or 01/1984 
 **/
function testcase6(resp) {
    if(resp.features.length > 0) {
        msg.html('Test case 6 response received. Parsing..');

        var testResult = true;
        for(var i=0; i<resp.features.length; i++) {
            // Looping all cyclones
            var cyclone = resp.features[i];
            for(var j=0; j<cyclone.geometry.coordinates.length; j++) {
                // Looping all coordinates
                var coordinate = cyclone.geometry.coordinates[j];
                var datetime = coordinate.datetime;
                if(datetime.indexOf('1980-02')==0 || datetime.indexOf('1984-01')==0) {
                    // This coordinate is valid
                    // console.log('valid: ' + datetime);
                } else {
                    // Invalid coordinate
                    testResult = false;
                    console.log('Invalid: ' + datetime);
                }
                if(!testResult) break;
            }
            if(!testResult) break;
        }

        if(testResult) msg.html('Test case 6 passed! For each cyclone, all coordinates match:<ur><li>year == Jan 1980 or Feb 1984</li></ur>');
        else msg.html('Test case 6 failed.');
    } else {
        msg.html('No response from the XML request.');
    }
}
