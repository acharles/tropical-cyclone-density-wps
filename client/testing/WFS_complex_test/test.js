var testApp = angular.module('testApp', ['ui.bootstrap']);

function loadXML(fileName){
    jQuery.ajaxSetup({async:false});
    jQuery.get( fileName, function(data) {
        xmlData = data;
    },"text")	
    jQuery.ajaxSetup({async:true});
    return xmlData;
}

testApp.controller('TestCtrl', function($scope, $http) {
    var wfsUrl = 'http://115.146.84.158/cyclonetracks2/index2.php';
    $scope.alerts = [];

    $scope.test1 = function() {
         var xml = loadXML("bbox-pressure.xml");//$scope.xml;
         if(xml) {
            document.getElementById("xmldata").value = xml;
            $http({
                url: wfsUrl,
                method: "POST",
                data: xml
            }).success(function (data, status, headers, config) {
                var fixes = data.features;
                var valid = true;
                for(var i=0; i<fixes.length; i++) {
                    var fix = fixes[i];
                    var lat = fix.properties.lat;
                    var lon = fix.properties.lon;
                    var pressure = fix.properties.pressure;
                    if((lon >= 85.078)
                       && (lon <= 137.988)
                       && (lat >= -41.377)
                       && (lat <= 3.513) 
                       && (pressure >= 950)
                       && (pressure <= 980)) valid = true;
                    else {
                        // An exception found
                        valid = false;
                        break;
                    }
                }
                if(valid) {
                    // Pass
                    $scope.alerts = [
                        { type: 'success', msg: 'Test #1 passed.' }
                    ];
                } else {
                    // Fail
                    $scope.alerts = [
                        { type: 'error', msg: 'Test #1 failed.' }
                    ];
                }
            }).error(function (data, status, headers, config) {
                console.log(status);
            });
         }
    }

    $scope.test2 = function() {
         var xml = loadXML("date_list-bbox.xml"); //$scope.xml;
         if(xml) {
            document.getElementById("xmldata").value = xml;
            $http({
                url: wfsUrl,
                method: "POST",
                data: xml
            }).success(function (data, status, headers, config) {
                var fixes = data.features;
                var valid = true;
                for(var i=0; i<fixes.length; i++) {
                    var fix = fixes[i];
                    var lat = fix.properties.lat;
                    var lon = fix.properties.lon;
                    // var pressure = fix.properties.pressure;
                    var datetime = fix.properties.datetime;
                    var ym = datetime.substring(0, 4)+ '-' +datetime.substring(5, 7);
                    if(
                       (lon >= 98.438) &&
                       (lon <= 145.371) &&
                       (lat >= -44.965) &&
                       (lat <= -4.39) &&
                       ((ym == '1987-01') || (ym == '1989-12') || (ym == '1991-12') || (ym == '1995-12') ||
                        (ym == '1999-01') || (ym == '2002-01') || (ym == '2008-02'))
                        ) valid = true;
                    else {
                        // An exception found
                        valid = false;
                        break;
                    }
                }
                if(valid) {
                    // Pass
                    $scope.alerts = [
                        { type: 'success', msg: 'Test #2 passed.' }
                    ];
                } else {
                    // Fail
                    $scope.alerts = [
                        { type: 'error', msg: 'Test #2 failed.' }
                    ];
                }
            }).error(function (data, status, headers, config) {
                console.log(status);
            });
         }
    }

    $scope.test3 = function() {
         var xml = loadXML("date_list-bbox-pressure.xml"); //$scope.xml;
         if(xml) {
            document.getElementById("xmldata").value = xml;
            $http({
                url: wfsUrl,
                method: "POST",
                data: xml
            }).success(function (data, status, headers, config) {
                var fixes = data.features;
                var valid = true;
                for(var i=0; i<fixes.length; i++) {
                    var fix = fixes[i];
                    var lat = fix.properties.lat;
                    var lon = fix.properties.lon;
                    var pressure = fix.properties.pressure;
                    var datetime = fix.properties.datetime;
                    var ym = datetime.substring(0, 4)+ '-' +datetime.substring(5, 7);
                    if(
                       (lon >= 98.438) &&
                       (lon <= 145.371) &&
                       (lat >= -44.965) &&
                       (lat <= -4.39) &&
                       (pressure >= 950) &&
                       (pressure <= 980) &&
                       ((ym == '1987-01') || (ym == '1989-12') || (ym == '1991-12') || (ym == '1995-12') ||
                        (ym == '1999-01') || (ym == '2002-01') || (ym == '2008-02'))
                        ) valid = true;
                    else {
                        // An exception found
                        valid = false;
                        break;
                    }
                }
                if(valid) {
                    // Pass
                    $scope.alerts = [
                        { type: 'success', msg: 'Test #3 passed.' }
                    ];
                } else {
                    // Fail
                    $scope.alerts = [
                        { type: 'error', msg: 'Test #3 failed.' }
                    ];
                }
            }).error(function (data, status, headers, config) {
                console.log(status);
            });
         }
    }

    $scope.test4 = function() {
         var xml = loadXML("date_list-pressure.xml"); //$scope.xml;
         if(xml) {
            document.getElementById("xmldata").value = xml;
            $http({
                url: wfsUrl,
                method: "POST",
                data: xml
            }).success(function (data, status, headers, config) {
                var fixes = data.features;
                var valid = true;
                for(var i=0; i<fixes.length; i++) {
                    var fix = fixes[i];
                    var pressure = fix.properties.pressure;
                    var datetime = fix.properties.datetime;
                    var ym = datetime.substring(0, 4)+ '-' +datetime.substring(5, 7);
                    if(
                       (pressure >= 950) &&
                       (pressure <= 980) &&
                       ((ym == '1987-01') || (ym == '1989-12') || (ym == '1991-12') || (ym == '1995-12') ||
                        (ym == '1999-01') || (ym == '2002-01') || (ym == '2008-02'))
                        ) valid = true;
                    else {
                        // An exception found
                        valid = false;
                        break;
                    }
                }
                if(valid) {
                    // Pass
                    $scope.alerts = [
                        { type: 'success', msg: 'Test #4 passed.' }
                    ];
                } else {
                    // Fail
                    $scope.alerts = [
                        { type: 'error', msg: 'Test #4 failed.' }
                    ];
                }
            }).error(function (data, status, headers, config) {
                console.log(status);
            });
         }
    }

    $scope.test5 = function() {
         var xml = loadXML("year_range-bbox.xml"); //$scope.xml;
         if(xml) {
            document.getElementById("xmldata").value = xml;
            $http({
                url: wfsUrl,
                method: "POST",
                data: xml
            }).success(function (data, status, headers, config) {
                var fixes = data.features;
                var valid = true;
                for(var i=0; i<fixes.length; i++) {
                    var fix = fixes[i];
                    var lat = fix.properties.lat;
                    var lon = fix.properties.lon;
                    var datetime = fix.properties.datetime;
                    var year = datetime.substring(0, 4);
                    if((lon >= 98.438) &&
                       (lon <= 145.371) &&
                       (lat >= -44.965) &&
                       (lat <= -4.39) &&
                       (year >= 1970) &&
                       (year < 1981)) valid = true;
                    else {
                        // An exception found
                        valid = false;
                        break;
                    }
                }
                if(valid) {
                    // Pass
                    $scope.alerts = [
                        { type: 'success', msg: 'Test #5 passed.' }
                    ];
                } else {
                    // Fail
                    $scope.alerts = [
                        { type: 'error', msg: 'Test #5 failed.' }
                    ];
                }
            }).error(function (data, status, headers, config) {
                console.log(status);
            });
         }
    }

    $scope.test6 = function() {
         var xml = loadXML("year_range-bbox-pressure.xml"); //$scope.xml;
         if(xml) {
            document.getElementById("xmldata").value = xml;
            $http({
                url: wfsUrl,
                method: "POST",
                data: xml
            }).success(function (data, status, headers, config) {
                var fixes = data.features;
                var valid = true;
                for(var i=0; i<fixes.length; i++) {
                    var fix = fixes[i];
                    var lat = fix.properties.lat;
                    var lon = fix.properties.lon;
                    var pressure = fix.properties.pressure;
                    var datetime = fix.properties.datetime;
                    var year = datetime.substring(0, 4);
                    if((lon >= 98.438) &&
                       (lon <= 145.371) &&
                       (lat >= -44.965) &&
                       (lat <= -4.39) &&
                       (pressure >= 950) &&
                       (pressure <= 980) &&
                       (year >= 1970) &&
                       (year < 1981)) valid = true;
                    else {
                        // An exception found
                        valid = false;
                        break;
                    }
                }
                if(valid) {
                    // Pass
                    $scope.alerts = [
                        { type: 'success', msg: 'Test #6 passed.' }
                    ];
                } else {
                    // Fail
                    $scope.alerts = [
                        { type: 'error', msg: 'Test #6 failed.' }
                    ];
                }
            }).error(function (data, status, headers, config) {
                console.log(status);
            });
         }
    }

    $scope.test7 = function() {
         var xml = loadXML("year_range-pressure.xml"); //$scope.xml;
         if(xml) {
            document.getElementById("xmldata").value = xml;
            $http({
                url: wfsUrl,
                method: "POST",
                data: xml
            }).success(function (data, status, headers, config) {
                var fixes = data.features;
                var valid = true;
                for(var i=0; i<fixes.length; i++) {
                    var fix = fixes[i];
                    var pressure = fix.properties.pressure;
                    var datetime = fix.properties.datetime;
                    var year = datetime.substring(0, 4);
                    if(
                       (pressure >= 950) &&
                       (pressure <= 980) &&
                       (year >= 1970) &&
                       (year < 1981)) valid = true;
                    else {
                        // An exception found
                        valid = false;
                        break;
                    }
                }
                if(valid) {
                    // Pass
                    $scope.alerts = [
                        { type: 'success', msg: 'Test #7 passed.' }
                    ];
                } else {
                    // Fail
                    $scope.alerts = [
                        { type: 'error', msg: 'Test #7 failed.' }
                    ];
                }
            }).error(function (data, status, headers, config) {
                console.log(status);
            });
         }
    }

    $scope.test8 = function() {
         var xml = loadXML("year-bbox.xml"); //$scope.xml;
         if(xml) {
            document.getElementById("xmldata").value = xml;
            $http({
                url: wfsUrl,
                method: "POST",
                data: xml
            }).success(function (data, status, headers, config) {
                var fixes = data.features;
                var valid = true;
                for(var i=0; i<fixes.length; i++) {
                    var fix = fixes[i];
                    var lat = fix.properties.lat;
                    var lon = fix.properties.lon;
                    var datetime = fix.properties.datetime;
                    var ym = datetime.substring(0, 4)+ '' + datetime.substring(5, 7);
                    if((lon >= 85.078) &&
                       (lon <= 137.988) &&
                       (lat >= -41.377) &&
                       (lat <= -3.513) &&
                       ((ym >= 197007) && (ym < 197107) || (ym >= 198007) && (ym < 198107) || (ym >= 198207) && (ym < 198307))
                       ) valid = true;
                    else {
                        // An exception found
                        valid = false;
                        break;
                    }
                }
                if(valid) {
                    // Pass
                    $scope.alerts = [
                        { type: 'success', msg: 'Test #8 passed.' }
                    ];
                } else {
                    // Fail
                    $scope.alerts = [
                        { type: 'error', msg: 'Test #8 failed.' }
                    ];
                }
            }).error(function (data, status, headers, config) {
                console.log(status);
            });
         }
    }

    $scope.test9 = function() {
         var xml = loadXML("year-bbox-pressure.xml"); //$scope.xml;
         if(xml) {
            document.getElementById("xmldata").value = xml;
            $http({
                url: wfsUrl,
                method: "POST",
                data: xml
            }).success(function (data, status, headers, config) {
                var fixes = data.features;
                var valid = true;
                for(var i=0; i<fixes.length; i++) {
                    var fix = fixes[i];
                    var lat = fix.properties.lat;
                    var lon = fix.properties.lon;
                    var pressure = fix.properties.pressure;
                    var datetime = fix.properties.datetime;
                    var ym = datetime.substring(0, 4)+ '' + datetime.substring(5, 7);
                    if((lon >= 85.078) &&
                       (lon <= 137.988) &&
                       (lat >= -41.377) &&
                       (lat <= -3.513) &&
                       (pressure >= 950) &&
                       (pressure <= 990) &&
                       ((ym >= 197007) && (ym < 197107) || (ym >= 198007) && (ym < 198107) || (ym >= 198207) && (ym < 198307))
                       ) valid = true;
                    else {
                        // An exception found
                        valid = false;
                        break;
                    }
                }
                if(valid) {
                    // Pass
                    $scope.alerts = [
                        { type: 'success', msg: 'Test #9 passed.' }
                    ];
                } else {
                    // Fail
                    $scope.alerts = [
                        { type: 'error', msg: 'Test #9 failed.' }
                    ];
                }
            }).error(function (data, status, headers, config) {
                console.log(status);
            });
         }
    }

    $scope.test10 = function() {
         var xml = loadXML("year-pressure.xml"); //$scope.xml;
         if(xml) {
            document.getElementById("xmldata").value = xml;
            $http({
                url: wfsUrl,
                method: "POST",
                data: xml
            }).success(function (data, status, headers, config) {
                var fixes = data.features;
                var valid = true;
                for(var i=0; i<fixes.length; i++) {
                    var fix = fixes[i];
                    var pressure = fix.properties.pressure;
                    var datetime = fix.properties.datetime;
                    var ym = datetime.substring(0, 4)+ '' + datetime.substring(5, 7);
                    if(
                       (pressure >= 950) &&
                       (pressure <= 990) &&
                       ((ym >= 197007) && (ym < 197107) || (ym >= 198007) && (ym < 198107) || (ym >= 198207) && (ym < 198307))
                       ) valid = true;
                    else {
                        // An exception found
                        valid = false;
                        break;
                    }
                }
                if(valid) {
                    // Pass
                    $scope.alerts = [
                        { type: 'success', msg: 'Test #10 passed.' }
                    ];
                } else {
                    // Fail
                    $scope.alerts = [
                        { type: 'error', msg: 'Test #10 failed.' }
                    ];
                }
            }).error(function (data, status, headers, config) {
                console.log(status);
            });
         }
    }
});
