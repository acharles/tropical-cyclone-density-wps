To test the building of WFS queries, put the directory WFSFilterTest inside
the js directory of the client and execute WFSFilterTest/WFSFilterTest.html.

To test if the WFS returns the correct results for predefined queries,
put the directory wfs_output_test inside the js directory of the client and 
execute wfs_output_test/test.html.
For the more complex WFS queries, just go in WFS_complex_test and run test.html
