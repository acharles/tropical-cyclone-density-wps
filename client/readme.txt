The client calls the Track density WPS to get the address of the data 
(stored in netcdf on the server), it then calls the WMS to get a png 
representation of that data to display on the map.
The client also calls the WMS to get a legend to display on the map that 
indicate the meaning of the colours on the png image.
The client can also call a WFS to get the cyclone tracks and display them on 
the same map.

Modify js/index.js to replace the URLs for the WPS and WMS.
Modify js/WFSRequest.js to replace the URL for the WFS.
(You can use the file in miscellaneous/WFSProxy/index2.php if the WFS is not
on the same domain as the web page.)

You can remove the unit tests before deployment in js/WFSFilterTest.

In order for the opendap request (js/opendap_request.js) to work, 
you need to set the netcdf output directory for the Track Density WPS process 
(in TrackDensityConfig.ini) to a directory that will be served by an opendap 
server. The request to the OpenDAP server to get the netcdf attributes will 
fail if the location of the netcdf file returned by the Track Density WPS 
process points to a netcdf file served only through HTTP (and not OpenDAP). 
In this case, some default values are chosen (see js/opendap_request.js).

You can get the WMS from https://github.com/andrewcharles/contour-wms.

Files:
js/fixesToCyclones.js: 
    Used to sort cyclone fixes and reorganise them into a different data 
    structure that can be displayed on the map.
js/index.js:
    Main javascript file for the web page to do the request to the WPS process
    and provide functions to modify the view.
js/map.js:
    Contains code and functions to do with the leaflet map:
    adding the map, the layers (track density images and cyclone tracks).
js/opendapRequest.js:
    Functions to request netcdf attributes from an opendap server.
js/WFSController.js:
    Functions to get data from the forms and to call other functions that
    prepare filters (in js/WFSFilters.js) and send the WFS request 
    (in js/WFSRequest.js).
js/WFSFilters.js:
    Functions that prepare filters from data gathered from the forms to be sent
    to the WFS that returns cyclone tracks.
js/WFSRequest.js:
    Functions to call the WFS.
    It then calls js/fixesToCyclones.js to get the appropriate data that it 
    uses to call js/map.js in order to display the cyclone tracks.
js/WPSRequest.js:
    Functions to call the track density WPS process:
    It calls the process and loads the XML status files until the final result
    is returned.

Directories:
mapping_client:
    The web page to call the Track Density WPS process, display its 
    representation as an image on a map and display the corresponding cyclone
    tracks from Filtering WFS.
mapping_client_dev:
    Same as mapping_client except the page display the queries sent to the 
    Filtering WFS.
testing:
    Contains some files to test the web page or the WFS.
