//Linkun

// HTML element to show stuff
var msg;
// How often is seconds the status of the WPS process is requested
var statusDocumentRefreshInterval = 1;
// Function to call when the Netcdf Url is returned by the WPS
var callbackNetcdfUrlReady = null;


/*
 * Request the address of the netcdf document to the WPS, call 
 * callbackNetcdfUrl with the url as an argument when the url has been 
 * returned. 
 */ 
function WPSrequest(url, callbackNetcdfUrl){
    msg = $('#msg');
    callbackNetcdfUrlReady = callbackNetcdfUrl;
    var request = $.get(url);

    request.success(function(data) {
        parseXML(data);
    });

    request.error(function(jq, statusText){
        alert("Error when calling the WPS: " + statusText);
    });

}

/*
 * Request a status document from the WPS at the url provided, then call
 * parseXML with that document.
 */ 
function WPSstatusRequest(url){
    var request = $.get(url);

    request.success(function(data) {
        parseXML(data);
    });

    request.error(function(jq, statusText){
        alert("Error when getting the WPS status: " + statusText);
    });
}



/*
 * This function parses the XML document received from the WPS
 */

function parseXML(data) {
    var finished = false;
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

    if(($(data).find('ProcessSucceeded').length > 0) || ($(data).find('wps\\:ProcessSucceeded').length > 0)) {
        // Chrome uses different syntax from Firefox.
        // 'wps\\:ProcessSucceeded' for Firefox, 'ProcessSucceeded' for Chrome.
        // Process finished.
        
        msg.html('WPS is finished');

        var netcdfUrl;
        if($(data).find('ProcessSucceeded').length > 0){
            netcdfUrl = $(data).find('ComplexData').text();
        }
        else{
            netcdfUrl = $(data).find('wps\\:ComplexData').text();
        }

        msg.html('NetCDF is Ready. URL: ' + netcdfUrl);
        //var filename = netcdfUrl.substring(netcdfUrl.lastIndexOf('/')+1);
        callbackNetcdfUrlReady(netcdfUrl);
    }
    else if(($(data).find('ProcessStarted').length > 0) || ($(data).find('wps\\:ProcessStarted').length > 0)){
        // Fetch status file location.
        statusUrl = null;
        
        var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        if(is_chrome){
            statusUrl = $(data).find('ExecuteResponse').attr('statusLocation');
        }
        else{
            statusUrl = $(data).find('wps\\:ExecuteResponse').attr('statusLocation');
        }

        msg.html('Got a status document');
        timer(statusDocumentRefreshInterval, statusUrl);
    }
    else if(($(data).find('ProcessFailed').length > 0) || ($(data).find('wps\\:ProcessFailed').length > 0)){
        var messageReturned = ""
        if(is_chrome){
            messageReturned = $(data).find('ExceptionText').text();
        }
        else{
            messageReturned = $(data).find('ows\\:ExceptionText').text();
        }
        msg.html('WPS failed: ' + messageReturned);
    }
    else if(($(data).find('ProcessAccepted').length > 0) || ($(data).find('wps\\:ProcessAccepted').length > 0)){
        msg.html('WPS accepted the request');
        
        statusUrl = null;
        if(is_chrome){
            statusUrl = $(data).find('ExecuteResponse').attr('statusLocation');
        }
        else{
            statusUrl = $(data).find('wps\\:ExecuteResponse').attr('statusLocation');
        }

        // Getting the status page:
        WPSstatusRequest(statusUrl)
    }
    else{
        msg.html("WPS response is not recognized!");
    }

}


/*
 * Wait for 'secs' seconds, then call WPSstatusRequest(statusUrl);
 */ 
function timer(secs, statusUrl) {
    setTimeout(function() {
        if(secs > 0){
            secs--;
            msg.html('Refreshing in '+ (secs+1) +' seconds...');
            timer(secs, statusUrl);
        }
        else{
            WPSstatusRequest(statusUrl);
        }
    }, 1000);
}
