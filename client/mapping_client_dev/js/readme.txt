A normal scenario:

TrackDensity WPS/OpenDAP server/contour_wms:
User inputs filtering arguments
  ->Clicks on Submit
  ->index.js gets data from the forms
  ->index.js calls WPSrequest.js (WPSrequest function) with a callback function
    c1 (c1 is in index.js: in doWPSRequest)
    ->WPSrequest.js gets the response from the WPS, calls c1 (in index.js)
    ->The response from the WPS is the URL to the netcdf file.
  ->c1 uses opendapRequest.js to get netcdf attributes using the netcdf file's
  URL and passes a callback function c2 (c2 is in index.js: in doWPSRequest)
    ->opendapRequest.js gets the netcdf attributes and calls c2
  ->c2 creates the WMS URL to get the image representation of the netcdf file
  ->c2 calls map.js
    ->map.js adds the image (using the WMS url) on top of the map
  ->c2 creates the WMS URL to get the legend for the image previously requested
  (the legend is an image) using the netcdf attributes gathered from the 
  OpenDAP server.
  
Filtering WFS:
User inputs filtering arguments
  ->WFSController.js gets data from the form.
  ->WFSController.js uses WFSFilters.js to build the XML query
  ->WFSController.js uses WFSRequest.js to send a request to the WFS
    ->WFSRequest.js gets the response from the WFS
    ->WFSRequest.js uses fixesToCyclones.js to group the fixes into cyclone
    tracks
    ->WFSRequest.js uses map.js to add the cyclone tracks
