/*
 * This file contains code to display a map, and add layers on top of it.
 */ 

// Layers
// Track density image layer:
var imagelayers = new L.LayerGroup();
// Cyclone tracks layers
var CycloneTracks = new L.LayerGroup();
// End of layers

// Map
var mapLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
});

var map = L.map('map', {
    center: [-30, 138],
    zoom: 3,
    minZoom: 1,
    maxZoom: 5,
    layers: [mapLayer, imagelayers, CycloneTracks]
});	

var overlays = {
    "Track Density": imagelayers,
    "CycloneTracks": CycloneTracks
};

L.control.layers(null, overlays, {position: 'bottomright'}).addTo(map);
// End of map

// Track density images:
var currentImage1 = null;
var currentImage2 = null;
function removeTrackDensityImages(){
        if(!(currentImage1 == null)){
        imagelayers.removeLayer(currentImage1);
    }
    if(!(currentImage2 == null)){
        imagelayers.removeLayer(currentImage2);
    }
}
function addImage(imageUrl){
    removeTrackDensityImages();
    imageBounds = [[70, -180], [-70, 180]];
    currentImage1 = L.imageOverlay(imageUrl, imageBounds, {opacity: 0.4});
    imagelayers.addLayer(currentImage1);
    imageBounds = [[70, 179.46], [-70, 540]];
    currentImage2 = L.imageOverlay(imageUrl, imageBounds, {opacity: 0.4});
    imagelayers.addLayer(currentImage2);
}
// End of Track density images:

// Legend (color scale)
var legend = L.control({position: 'bottomleft'});
var legendElement = null;
legend.onAdd = function (map) {
    this._legend = L.DomUtil.create('div', 'legend');
    legendElement = this._legend;
    this.update();
    return this._legend;
};
function addLegend(url) {
    legendElement.innerHTML = '<img src="'+url+'" alt="legend" />';
}
legend.update = function (lat,lng) {};
legend.addTo(map);
// End of Legend (color scale)

//change format of lat and lng: 0.000
function toDecimal(x) {    
    var f = parseFloat(x);    
    if (isNaN(f)) {    
        return false;    
    }    
    var f = Math.round(x*1000)/1000;    
    var s = f.toString();    
    var rs = s.indexOf('.');    
    if (rs < 0) {    
        rs = s.length;    
        s += '.';    
    }    
    while (s.length <= rs + 2) {    
        s += '0';    
    }    
    return s;    
}  
// Coordinate box
var info = L.control({position: 'topright'});
info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
};
info.update = function (lat,lng) {
    this._div.innerHTML = '<h4>Coordinates: </h4>' +  (lat,lng?
        '<b>Lng: ' + lng + '</b><br /><b>Lat: ' + lat + '</b>'
        : 'None');
};
info.addTo(map);
map.on('mousemove', function(e) {
    info.update(toDecimal(e.latlng.lat), toDecimal(e.latlng.lng));
});
map.on('mouseout', function(e) {
    info.update();
});
// End of Coordinate box

// Setting BBOX/SBOX
map.on('click', function(e) {
    info.update(toDecimal(e.latlng.lat), toDecimal(e.latlng.lng));
    document.getElementById("lat").value = toDecimal(e.latlng.lat);
    document.getElementById("lng").value = toDecimal(e.latlng.lng);										
});	
// End of Setting BBOX/SBOX

// Cyclone tracks
var currentCycloneTracks = null;
function deleteCycloneTracks(){
    // Delete old tracks
    if(!(currentCycloneTracks == null)){
        for(i=0;i<currentCycloneTracks.length;i++){
            CycloneTracks.removeLayer(currentCycloneTracks[i]);
        }
    }
}

function addCycloneTracks(featureCollection){
    var features = featureCollection.features;
    deleteCycloneTracks();
    // Add new tracks
    currentCycloneTracks = [];
    for(i=0;i<features.length;i++)
    {
        var myLines = [{
        "type": "LineString",
        "coordinates": features[i].geometry.coordinates
        }];
        
        var myStyle = {
        "color": "#" + i%10 + i%8 + i%6 + i%4 + i%2 + "f",
        "weight": 5,
        "opacity": 0.3
        };
        // Add new tracks
        currentCycloneTracks[i] = L.geoJson(myLines, {
            style: myStyle,
            onEachFeature: function (feature,layer) {
                layer.bindPopup("<div>Name: "+ features[i].properties.name + "<br/>Year: "+ features[i].properties.year +"</div>");
            }

        })
        currentCycloneTracks[i].addTo(CycloneTracks);
    }
}

// Add tracks from a file
/*
$.get( "Json.txt", function(data) {
    var obj = jQuery.parseJSON(data);
    addCycloneTracks(obj);
},"text")		
*/
// End of Cyclone tracks
