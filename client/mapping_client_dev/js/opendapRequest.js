/*
 * This file contains code to request NetCDF attributes to an opendap server.
 * It parses the result of this call to get the specific attributes we are
 * after: actual_min and actual_max.
 */ 
msg = null;
   
/*
 * Make a request to the opendap server to get the netcdf attributes.
 * Inputs:
 *     netcdfUrl: The url of the netcfd file on an opendap server.
 *     endCallBack: The callback function to call (its arg will be an array
 *         contaning the attributes (1st is actual_min, 2nd is actual_max):
 *         e.g.: doStuff(attrs){alert(attrs[0])}
 */
function NetCDFAttributesRequest(netcdfUrl, endCallBack){
    msg = $('#msg');
    // Build url
    netcdfUrl += ".das"
    // Do the request
	var request = $.get(netcdfUrl)
    var returnArray = []
    
    request.success(function(data) {
        attributes = getAttributesFromText(data);
        returnArray.push(attributes["actual_min"]);
        returnArray.push(attributes["actual_max"]);
        msg.html("NetCDF attributes: actual_min =" + returnArray[0] 
            + " actual_max=" + returnArray[1])
        endCallBack(returnArray)
    });
    
    request.error(function(jq, statusText){
        alert("Could not get netcdf attributes: "+ statusText 
            + "\nUsing default values");
        returnArray.push("0");
        returnArray.push("0.001");
        endCallBack(returnArray)
    })
}

/*
 * Get the density attribute values from the return string of the opendap 
 * server
 */
function getAttributesFromText(str){
    var lines=str.split("    ");
    var isDensity = false;
    // We use this object like a dictionary to store attributes
    var densityAttributes = new Object();
    for (line in lines){
        // Ignore the \n at the end of the line
        lineValue = lines[line].substring(0,lines[line].length-1);
        if (!isDensity){
            // If we were not looking at density attributes
            if (lineValue == "den {"){
                // We are now going through density attributes
                isDensity = true;
            }
            else{
                // Ignore attributes that are not density
                continue;
            }
        }
        else{
            // We are looking at density attributes
            if (lineValue == "}"){
                // End of density attributes
                break;
            }
            else{
                // Save the density attribute
                var attribute = lineValue.split(" ");
                // Not an attribute ?
                if (attribute.length < 3){
                    continue;
                }
                // Remove semicolon
                var attributeValue = 
                    attribute[2].substring(0, attribute[2].length-1)
                // Remove quotation marks
                if(attributeValue.charAt(0) == '"'){
                    attributeValue = 
                        attributeValue.substring(1, attributeValue.length-1)
                }
                densityAttributes[attribute[1]] = attributeValue;
            }
        }
    }
    return densityAttributes;
} 
