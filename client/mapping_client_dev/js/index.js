
var WMS_URL = "http://115.146.84.158/contour-wms/";
var TRACK_DENSITY_EXECUTE_URL = "http://115.146.84.158/zoo/?Service=WPS&Request=Execute&Version=1.1.0&Identifier=TrackDensity"

/*
 * Change the time selection tab (year, year list, year range, date list)
 */ 
function setTab(m,n){
    var tli=document.getElementById("menu"+m).getElementsByTagName("li");
    var mli=document.getElementById("main"+m).getElementsByTagName("ul");
    for(i=0;i<tli.length;i++){  
        tli[i].className=i==n?"hover":"";
        mli[i].style.display=i==n?"block":"none";
    }
}

function fillSelect(selectElementId, minValue, maxValue, selected){
    var ddlYear = document.getElementById(selectElementId);
    if(ddlYear == null){
        return;
    }
    for(var i = minValue; i <= maxValue; i++){
        var optionElement = document.createElement('option');
        if(i == selected){
            optionElement.setAttribute("selected", "selected")
        }
        var optionValue = document.createTextNode(String(i));
        optionElement.appendChild(optionValue);
        ddlYear.add(optionElement);
    }
}


function getBboxValues(suffix){
    minx_string = document.getElementById("minx" + suffix).value;
    miny_string = document.getElementById("miny" + suffix).value;
    maxx_string = document.getElementById("maxx" + suffix).value;
    maxy_string = document.getElementById("maxy" + suffix).value;
    
    if (minx_string == "" || miny_string || "" || maxx_string == "" || 
        maxy_string == ""){
        return null;  
    }     
    
    minx = parseFloat(minx_string);
    miny = parseFloat(miny_string);
    maxx = parseFloat(maxx_string);
    maxy = parseFloat(maxy_string); 
    
    return [minx, miny, maxx, maxy];
}

function getDrawCycloneTracks(suffix){
    return document.getElementById("check_cyclone-tracks" + suffix).checked;
}

/*
 * Return a WMS URL based on the arguments provided
 */ 
function getWMSUrl(netcdfUrl, minColorScaleRange, maxColorScaleRange, bbox){
    var widthStr = "&width=2048";
    var heightStr = "&height=2048";
    var bboxStr = "";
    if (!(bbox==null)){
        var width = (bbox[2] - bbox[0]) * (2048 / 360);
        widthStr = "&width=" + width;
        var height = (bbox[3] - bbox[1]) * (2048 / 180);
        heightStr = "&height=" + height;
        bboxStr = "&bbox= " + bbox[0] + ',' + bbox[1] + ',' + bbox[2] + ',' 
            + bbox[3];
    }
    wmsUrl = 
    WMS_URL + "?styles=grid&color_scale_range=" +
    minColorScaleRange + "," + maxColorScaleRange + "&color_range=-10,10" +
    "&TRANSPARENT=TRUE&format=image/png&source_url=" + netcdfUrl +
    "&layers=den&time_index=Default" + widthStr + heightStr + bboxStr +
    "&n_color=10&crs=EPSG:4326&palette=jet&request=GetMap&time=Default";
    
    return wmsUrl;
}

/*
 * Return a Legend URL based on the arguments provided
 */ 
function getLegendUrl(netcdfUrl, minColorScaleRange, maxColorScaleRange){
    legendUrl = 
    WMS_URL + "?styles=grid&color_scale_range=" +
    minColorScaleRange + "," + maxColorScaleRange +
    "&color_range=-10,10&TRANSPARENT=TRUE&format=image/png&source_url=" +
    netcdfUrl +
    "&layers=den&time_index=Default&n_color=10&crs=EPSG:4283" +
    "&request=GetLegend&time=Default";
    
    return legendUrl;
}

/*
 * Send a request to the URL 'WPSUrl', if successful, ask the opendap server
 * about the attributes to get the color_scale_min and color_scale_max,
 * Use those attributes to build the WMS URL and call the function to add
 * the image using that WMS url.
 */ 
function doWPSRequest(WPSUrl, bbox){
    // asynchronous.js
    WPSrequest(WPSUrl, function(netcdfUrl){
        if(netcdfUrl){
            //opendap_request.js
            NetCDFAttributesRequest(netcdfUrl, function(densityAttributes){
                imageUrl = getWMSUrl(netcdfUrl, densityAttributes[0], 
                    densityAttributes[1], bbox);
                // index.html
                addImage(imageUrl)
                
                legendUrl = getLegendUrl(netcdfUrl, densityAttributes[0], 
                    densityAttributes[1]);
                addLegend(legendUrl);
                
            })
        }})
}

// Functions used to call the WPS for each type of time selection
// (year, yearlist (which is actually also year), year_range, and date_list
var WPSUrlPrefix = TRACK_DENSITY_EXECUTE_URL + "&status=true&storeExecuteResponse=true&DataInputs=";

function callWPSYearList(){
    // Delete old track density images and tracks:
    deleteCycloneTracks();
    removeTrackDensityImages();
    // Get arguments and do the WPS request
    var WPSUrl = WPSUrlPrefix + getArgumentsYearList()
    var bbox = getBboxValues("02");
    doWPSRequest(WPSUrl, bbox);// WPSRequest.js
    // Get cyclone tracks if requested
    if(getDrawCycloneTracks("02")){
        getYearData();// WFSController.js
    }
}

function callWPSYearRange(){
    // Delete old track density images and tracks:
    deleteCycloneTracks();
    removeTrackDensityImages();
    // Get arguments and do the WPS request
    var WPSUrl = WPSUrlPrefix + getArgumentsYearRange()
    var bbox = getBboxValues("03");
    doWPSRequest(WPSUrl, bbox);// WPSRequest.js
    // Get cyclone tracks if requested
    if(getDrawCycloneTracks("03")){
        getYearRangeData();// WFSController.js
    }
}

function callWPSDateList(){
    // Delete old track density images and tracks:
    deleteCycloneTracks();
    removeTrackDensityImages();
    // Get arguments and do the WPS request
    var WPSUrl = WPSUrlPrefix + getArgumentsDateList()
    var bbox = getBboxValues("04");
    doWPSRequest(WPSUrl, bbox);// WPSRequest.js
    // Get cyclone tracks if requested
    if(getDrawCycloneTracks("04")){
        getDatelistData();// WFSController.js
    }
}

function addGenesis(dataInputs, suffix){
        genesis = document.getElementById("genesis" + suffix).value;
        dataInputs += "genesis=" + genesis + ";";
        return dataInputs;
}

function addRes_x(dataInputs, suffix){
    res_x = document.getElementById("res" + suffix).value;
    if(res_x !== ""){
        dataInputs += "res_x=" + res_x + ";";
    }
    return dataInputs;
}

function addRes_y(dataInputs, suffix){
    res_y = document.getElementById("res" + suffix).value;
    if(res_y !== ""){
        dataInputs += "res_y=" + res_y + ";";
    }
    return dataInputs;
}

/*
 * Get the WPS arguments for a list of years
 */ 
function getArgumentsYearList(){
        yearlist = document.getElementById("yearlist").value;
        minx02 = document.getElementById("minx02").value;
        miny02 = document.getElementById("miny02").value;
        maxx02 = document.getElementById("maxx02").value;
        maxy02 = document.getElementById("maxy02").value;
        sminx02 = document.getElementById("sminx02").value;
        sminy02 = document.getElementById("sminy02").value;
        smaxx02 = document.getElementById("smaxx02").value;
        smaxy02 = document.getElementById("smaxy02").value;
        pressmin02 = document.getElementById("pressmin02").value;
        pressmax02 = document.getElementById("pressmax02").value;
        hemisphere02 = document.getElementById("hemisphere02").value;
        output_format02 = document.getElementById("output_format02").value;
        month02 = document.getElementById("month02").value;
        DataInputs02 = ""
        
        
        if (yearlist !== "")  {
                DataInputs02 += "year=" + yearlist + ";";           
        }
		if (month02 !== "")  {
                DataInputs02 += "month=" + month02 + ";";   
        }
        if (minx02 !== "" && miny02 !== "" && maxx02 !== "" && maxy02 !== "")  {
                DataInputs02 += "bbox=" + minx02 + "," + miny02 + "," + maxx02 + "," + maxy02 + ";";    
        }
		if (sminx02 !== "" && sminy02 !== "" && smaxx02 !== "" && smaxy02 !== "")  {
                DataInputs02 += "sbox=" + sminx02 + "," + sminy02 + "," + smaxx02 + "," + smaxy02 + ";";        
        }
        if(pressmin02 !== "")
        {
                DataInputs02 += "pressmin=" + pressmin02 + ";";         
        }
        if(pressmax02 !== "")
        {
                DataInputs02 += "pressmax=" + pressmax02 + ";";         
        }
        if(hemisphere02 !== "0")
        {
                DataInputs02 += "hemisphere=" + hemisphere02 + ";";     
        }
        if(output_format02 !== "0")
        {
                DataInputs02 += "output_format=" + output_format02 + ";";
        }

        DataInputs02=addGenesis(DataInputs02, "02");
        DataInputs02 = addRes_x(DataInputs02, "02");
        DataInputs02 = addRes_y(DataInputs02, "02");
        DataInputs02=DataInputs02.substr(0, DataInputs02.length-1);  
        //document.getElementById("DataInputs02").value = DataInputs02;
        return DataInputs02;
}

/*
 * Get the WPS arguments for a year range
 */ 
function getArgumentsYearRange(){
        year_from = document.getElementById("year_from").value;
        year_to = document.getElementById("year_to").value;
        month_from = document.getElementById("month_from").value;
        month_to = document.getElementById("month_to").value;
        
        minx03 = document.getElementById("minx03").value;
        miny03 = document.getElementById("miny03").value;
        maxx03 = document.getElementById("maxx03").value;
        maxy03 = document.getElementById("maxy03").value;
		sminx03 = document.getElementById("sminx03").value;
        sminy03 = document.getElementById("sminy03").value;
        smaxx03 = document.getElementById("smaxx03").value;
        smaxy03 = document.getElementById("smaxy03").value;
        pressmin03 = document.getElementById("pressmin03").value;
		pressmax03 = document.getElementById("pressmax03").value;
        hemisphere03 = document.getElementById("hemisphere03").value;
        output_format03 = document.getElementById("output_format03").value;
        
        DataInputs03 = "";
        
        if (year_from !== "" && year_from !== "Year From"&& year_to !== "" && year_to !== "Year To")  {
                DataInputs03 += "year_range=" + year_from + "," + year_to + ";";        
        }
        if (month_from !== ""&& month_from !== "Month From" && month_to !== "" && month_to !== "Month To")  {
                DataInputs03 += "month_range=" + month_from + "," + month_to + ";";     
        }
        if (minx03 !== "" && miny03 !== "" && maxx03 !== "" && maxy03 !== "")  {
                DataInputs03 += "bbox=" + minx03 + "," + miny03 + "," + maxx03 + "," + maxy03 + ";";    
        }
        if (sminx03 !== "" && sminy03 !== "" && smaxx03 !== "" && smaxy03 !== "")  {
                DataInputs03 += "sbox=" + sminx03 + "," + sminy03 + "," + smaxx03 + "," + smaxy03 + ";";        
        }
        if(pressmin03 !== "")
        {
                DataInputs03 += "pressmin=" + pressmin03 + ";";         
        }
        if(pressmax03 !== "")
        {
                DataInputs03 += "pressmax=" + pressmax03 + ";";         
        }
        if(hemisphere03 !== "0")
        {
                DataInputs03 += "hemisphere=" + hemisphere03 + ";";     
        }
        if(output_format03 !== "0")
        {
                DataInputs03 += "output_format=" + output_format03 + ";";
        }

        DataInputs03=addGenesis(DataInputs03, "03");
        DataInputs03 = addRes_x(DataInputs03, "03");
        DataInputs03 = addRes_y(DataInputs03, "03");
        DataInputs03=DataInputs03.substr(0, DataInputs03.length-1);
        return DataInputs03;
}

/*
 * Get the WPS arguments for a list of year-month pair
 */ 
function getArgumentsDateList(){
        date_list = document.getElementById("date_list").value;
        minx04 = document.getElementById("minx04").value;
        miny04 = document.getElementById("miny04").value;
        maxx04 = document.getElementById("maxx04").value;
        maxy04 = document.getElementById("maxy04").value;
        sminx04 = document.getElementById("sminx04").value;
        sminy04 = document.getElementById("sminy04").value;
        smaxx04 = document.getElementById("smaxx04").value;
        smaxy04 = document.getElementById("smaxy04").value;
        pressmin04 = document.getElementById("pressmin04").value;
        pressmax04 = document.getElementById("pressmax04").value;
        hemisphere04 = document.getElementById("hemisphere04").value;
        output_format04 = document.getElementById("output_format04").value;
        DataInputs04 = ""
        
        
        if (date_list !== "")  {
                DataInputs04 += "date_list=" + date_list + ";";           
        }
        if (minx04 !== "" && miny04 !== "" && maxx04 !== "" && maxy04 !== "")  {
                DataInputs04 += "bbox=" + minx04 + "," + miny04 + "," + maxx04 + "," + maxy04 + ";";    
        }
		if (sminx04 !== "" && sminy04 !== "" && smaxx04 !== "" && smaxy04 !== "")  {
                DataInputs04 += "sbox=" + sminx04 + "," + sminy04 + "," + smaxx04 + "," + smaxy04 + ";";        
        }
        if(pressmin04 !== "")
        {
                DataInputs04 += "pressmin=" + pressmin04 + ";";         
        }
        if(pressmax04 !== "")
        {
                DataInputs04 += "pressmax=" + pressmax04 + ";";         
        }
        if(hemisphere04 !== "0")
        {
                DataInputs04 += "hemisphere=" + hemisphere04 + ";";     
        }
        if(output_format04 !== "0")
        {
                DataInputs04 += "output_format=" + output_format04 + ";";
        }
        DataInputs04=addGenesis(DataInputs04, "04");
        DataInputs04 = addRes_x(DataInputs04, "04");
        DataInputs04 = addRes_y(DataInputs04, "04");
        DataInputs04=DataInputs04.substr(0, DataInputs04.length-1);  
        return DataInputs04;
}


function AddStart(){
	if (document.getElementById("lat").value  !== ""&& document.getElementById("lng").value !== "")  {
		document.getElementById("startx").value = document.getElementById("lng").value ;
		document.getElementById("starty").value = document.getElementById("lat").value ;
	}
	else{
		alert('No coordinate!!')
	}
}

function AddEnd(){
	if (document.getElementById("lat").value  !== ""&& document.getElementById("lng").value !== "")  {
		document.getElementById("endx").value = document.getElementById("lng").value ;
		document.getElementById("endy").value = document.getElementById("lat").value ;
	}
	else{
		alert('No coordinate!!')
	}
}

function AddBBox(){
	document.getElementById("check_bbox").checked=true; 
	getBlock('check_bbox','hidden_bbox');
	document.getElementById("check_bbox02").checked=true; 
	getBlock('check_bbox02','hidden_bbox02');
	document.getElementById("check_bbox03").checked=true; 
	getBlock('check_bbox03','hidden_bbox03');
	startx = document.getElementById("startx").value;
	starty = document.getElementById("starty").value;
	endx = document.getElementById("endx").value;
	endy = document.getElementById("endy").value;
	
	var minx = 0;
	var miny = 0;
	var maxx = 0;
	var maxy = 0;
	
	if(startx !=="" && starty !=="" && endx !=="" & endy !==""){
		startx = Number(startx);
		starty = Number(starty);
		endx = Number(endx);
		endy = Number(endy);
		if(startx <= endx ){
			minx = startx;
			maxx = endx;
			if(starty <= endy ){
				miny = starty;
				maxy = endy;
			}
			else{
				miny = endy;
				maxy = starty;
			}
		}
		else{
			minx = endx;
			maxx = startx;
			if(starty <= endy ){
				miny = starty;
				maxy = endy;
			}
			else{
				miny = endy;
				maxy = starty;
			}
		}


		document.getElementById("minx02").value = minx;
		document.getElementById("minx03").value = minx;
		document.getElementById("minx04").value = minx;

		document.getElementById("miny02").value = miny;
		document.getElementById("miny03").value = miny;
		document.getElementById("miny04").value = miny;

		document.getElementById("maxx02").value = maxx;
		document.getElementById("maxx03").value = maxx;
		document.getElementById("maxx04").value = maxx;

		document.getElementById("maxy02").value = maxy;
		document.getElementById("maxy03").value = maxy;
		document.getElementById("maxy04").value = maxy;
	}
	else{
		alert('No start or end coordinate!!');
	}
}

function AddSBox(){
	document.getElementById("check_sbox").checked=true; 
	getBlock('check_sbox','hidden_sbox');
	document.getElementById("check_sbox02").checked=true; 
	getBlock('check_sbox02','hidden_sbox02');
	document.getElementById("check_sbox03").checked=true; 
	getBlock('check_sbox03','hidden_sbox03');

	startx = document.getElementById("startx").value;
	starty = document.getElementById("starty").value;
	endx = document.getElementById("endx").value;
	endy = document.getElementById("endy").value;
	
	var minx = 0;
	var miny = 0;
	var maxx = 0;
	var maxy = 0;
	
	if(startx !=="" && starty !=="" && endx !=="" & endy !==""){
		startx = Number(startx);
		starty = Number(starty);
		endx = Number(endx);
		endy = Number(endy);
		if(startx <= endx ){
			minx = startx;
			maxx = endx;
			if(starty <= endy ){
				miny = starty;
				maxy = endy;
			}
			else{
				miny = endy;
				maxy = starty;
			}
		}
		else{
			minx = endx;
			maxx = startx;
			if(starty <= endy ){
				miny = starty;
				maxy = endy;
			}
			else{
				miny = endy;
				maxy = starty;
			}
		}


		document.getElementById("sminx02").value = minx;
		document.getElementById("sminx03").value = minx;
		document.getElementById("sminx04").value = minx;

		document.getElementById("sminy02").value = miny;
		document.getElementById("sminy03").value = miny;
		document.getElementById("sminy04").value = miny;

		document.getElementById("smaxx02").value = maxx;
		document.getElementById("smaxx03").value = maxx;
		document.getElementById("smaxx04").value = maxx;

		document.getElementById("smaxy02").value = maxy;
		document.getElementById("smaxy03").value = maxy;
		document.getElementById("smaxy04").value = maxy;
	}
	else{
		alert('No start or end coordinate!!');
	}
}

/*
 * Adds a selected month to the month input field (For a single year)
 */ 
function addMonth(suffix){
	add = document.getElementById("add_month" + suffix).value;
	month = document.getElementById("month" + suffix).value;
	if (month !== "")
	{
		if (add !== "" && add !== "Add Month" && month.indexOf(add) < 0)  {
			document.getElementById("month" + suffix).value = month + "," + add;
		}
	}
	else
	{
		if (add !== "" && add !== "Add Month" && month.indexOf(add) < 0)  {
			document.getElementById("month" + suffix).value = add;
		}
	}
}

/*
 * Adds a selected year to the year input field (For a year list)
 */ 
function addYear(){

	add = document.getElementById("add_year").value;
	year = document.getElementById("yearlist").value;
	if (year !== "")
	{
		if (add !== "" && add !== "Add Year" && year.indexOf(add) < 0)  {
			document.getElementById("yearlist").value = year + "," + add;
		}
	}
	else
	{
		if (add !== "" && add !== "Add Year" && year.indexOf(add) < 0)  {
			document.getElementById("yearlist").value = add;
		}
	}

}

/*
 * Adds a selected date to the date list input field (For a date list)
 */ 
function addDate(){

	year = document.getElementById("add_date_year").value;
	month = document.getElementById("add_date_month").value;
	add = document.getElementById("date_list").value;
	if(add !==""){
		if (year !== "" && month !== "" && year !== "Add Year" && month !== "Add Month")
		{
			add_date = year + month;
			if(add.indexOf(add_date) < 0){
				document.getElementById("date_list").value = add + "," + add_date;
			}
		}
	}
	else{
		if (year !== "" && month !== "" && year !== "Add Year" && month !== "Add Month")
		{
			add_date = year + month;
			document.getElementById("date_list").value = add_date;
		}
	}
}

function getBlock(ck,d)
{
	var c = document.getElementById(ck);
	var d = document.getElementById(d);
	if(c.checked==true){
		d.style.display = "block";
	}
	else{
		d.style.display = "none";
	}
}
function mouseOver(flag)
{
	if(flag === "year_list")
	{
		document.getElementById('information').value =
            "Filter cyclones for a list of years.";
	}
	if(flag === "month_list")
	{
		document.getElementById('information').value =
            "The list of months that will be used for each year.";
	}
	if(flag === "BBox")
	{
		document.getElementById('information').value = 
            "Filter only fixes in the box defined by the coordinates (EPSG:4326).";
	}
	if(flag === "SBox")
	{
		document.getElementById('information').value =
            "Filter cyclones that have at least one fix in the box defined by the coordinates (EPSG:4326).";
	}
	if(flag === "pressure")
	{
		document.getElementById('information').value = 
            "Pressure, Min Pressure & Max Pressure";
	}
	if(flag === "others")
	{
		document.getElementById('information').value =
            "Other options, such as: Hemisphere, Genesis & Resolution";
	}
    if(flag === "hemisphere")
	{
		document.getElementById('information').value =
            "Year format: Northern for Janurary to December, Southern for July to June";
	}
    if(flag === "genesis")
	{
		document.getElementById('information').value =
            "Keep only fixes at the start of tropical cyclones (Genesis) or keep all fixes";
	}
    if(flag === "resolution")
	{
		document.getElementById('information').value =
            "Lower the resolution value for data and image to be finer grained";
	}
	if(flag === "year_range")
	{
		document.getElementById('information').value = 
            "Filter only fixes in this year range, the first month corresponds to the first year, the second month to the second year. Watch out for hemisphere";
	}
	if(flag === "date_list")
	{
		document.getElementById('information').value = 
            "List of year-month tuples in the format: YYYYMM";
	}
	
    if(flag === "cyclone_tracks")
	{
		document.getElementById('information').value = 
            "Get cyclone tracks from the filtering WFS";
	}
	
}
function mouseOut()
{
	document.getElementById('information').value ="";
}

function load()
{
	var form02 = document.getElementById("form02");
	var form03 = document.getElementById("range_form");
	var form04 = document.getElementById("form04");
	form02.reset();
	form03.reset();
	form04.reset();	
	document.getElementById("lng").value = "";
	document.getElementById("lat").value = "";
	document.getElementById("startx").value = "";
	document.getElementById("starty").value = "";
	document.getElementById("endx").value = "";
	document.getElementById("endy").value = "";
    // Call the WPS with the default arguments
    callWPSYearList();
}
