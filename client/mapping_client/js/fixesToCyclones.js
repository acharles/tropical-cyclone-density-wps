// This file contains methods to convert a FeatureCollection of fixes to a
// FeatureCollection of cyclones: using newCycloneCollection.

/*
 * Sort the fixes for a single cyclone for a chronological order
 */
function sortSingleCycloneFixes(a,b){
    return parseInt(a.id) - parseInt(b.id);
}

/*
 * Sort all the fixes by cyclone (using tc_id)
 */
function sortByCyclone(a,b){
    return parseInt(a.properties.tc_id) - parseInt(b.properties.tc_id)
}
    
/*
 * Create a feature collection of cyclones from a feature collection of
 * cyclone fixes:
 * Sort by cyclone, for each cyclone group all the fixes in a feature and sort
 * them by time, 
 * 
 * input: FeatureCollection of fixes. (GEOJson Javascript object)
 * output: FeatureCollection of cyclones. (GEOJson Javascript object)
 */
function newCycloneCollection(fixCollection){
    fixList = fixCollection["features"];
    // Sort by cyclone using tc_id
    fixList.sort(sortByCyclone);
    cycloneCollection = new Object();
    cycloneCollection.type = "FeatureCollection"
    // List of cyclones:
    features = [];
    var indexInFeatures = -1;
    // Go through all the fixes
    for(var i = 0; i < fixList.length; i++){
        if(i == 0){
            // First fix: create a cyclone, add the fix to that cyclone:
            var cyclone = newCyclone(fixList[i]);
            features.push(cyclone);
            indexInFeatures++;
            continue;
        }
        previousCyclone = features[indexInFeatures];
        if(previousCyclone.properties.tc_id == fixList[i].properties.tc_id){
            // If this fix belongs to the same cyclone as the previous,
            // Add a fix to the previous cyclone
            var coordinates = fixList[i].geometry.coordinates;
            // Adding fix properties (to sort)
            coordinates.id = fixList[i].properties.id;
            coordinates.datetime = fixList[i].properties.datetime;
            coordinates.pressure = fixList[i].properties.pressure;
            // pushing the fix to the fix array for the cyclone
            features[indexInFeatures].geometry.coordinates.push(coordinates);
            continue;
        }
        else{
            // Reorder previous cyclone
            features[indexInFeatures].geometry.coordinates.sort(sortSingleCycloneFixes);
            // Create a new cyclone
            var cyclone = newCyclone(fixList[i]);
            features.push(cyclone);
            indexInFeatures++;
            continue;
        }
    }
    // Reorder last cyclone:
    if(indexInFeatures > -1){
        features[indexInFeatures].geometry.coordinates.sort(sortSingleCycloneFixes);
    }
    cycloneCollection.features = features;
    return cycloneCollection;
}

/*
 * Create a new feature for a cyclone using a cyclone fix feature
 * (The next fixes will need to be added to the coordinates array)
 */
function newCyclone(cycloneFix){
    geometry = new Object();
    // Append 1st set of coordinates + id to sort
    coordinates = cycloneFix.geometry.coordinates;
    geometry.coordinates = [coordinates];
    // Set properties for the array (to sort)
    geometry.coordinates[0].id = cycloneFix.properties.id;
    geometry.coordinates[0].datetime = cycloneFix.properties.datetime;
    geometry.coordinates[0].pressure = cycloneFix.properties.pressure;
    geometry.type = "LineString";
    
    properties = new Object();
    properties.name = cycloneFix.properties.name;
    properties.tc_id = cycloneFix.properties.tc_id;
    properties.year = cycloneFix.properties.year;
    
    cyclone = new Object();
    cyclone.type = "Feature";
    cyclone.geometry = geometry;
    cyclone.properties = properties;
    
    return cyclone;
}

/*
 * Remove cyclones that don't have at least one fix un sbox
 * This function doesn't copy the cyclone features but copies references to
 * them. (modifying the cyclones in will modify both the input 
 * cycloneCollection and output cycloneCollection)
 * Inputs:
 *  cycloneCollection: Javascript object equivalent to a GEOJson 
 *      FeatureCollection of cyclones
 *  sbox: array [minlon, minlat, maxlon, maxlat]
 */ 
function filterSbox(cycloneCollection, sbox){
    // Create the new cycloneCollection object
    filteredCycloneCollection = new Object();
    filteredCycloneCollection.type = "FeatureCollection"
    filteredCycloneCollection.features = [];
    
    var cyclonesToRemove = [];
    // For each cyclone
    for(var i = 0; i < cycloneCollection.features.length ; i++){
        var fixes = cycloneCollection.features[i].geometry.coordinates;
        var numberOfFixes = fixes.length;
        //alert("i: " + i + " length: " + cycloneCollection.features.length);
        // Go through the fixes
        for(var j = 0; j < numberOfFixes; j++){
            if(fixes[j][0] >= sbox[0] && fixes[j][0] <= sbox[2] && 
               fixes[j][1] >= sbox[1] && fixes[j][1] <= sbox[3]){
                // At least one fix in sbox: keep cyclone
                filteredCycloneCollection.features.push(cycloneCollection.features[i]);
                break;
            }
        }
    }
    
    return filteredCycloneCollection;
}
