/*
 * This file contains code to send a request to filtering_wfs 
 * (that filters and returns cyclone fixes) and take appropriate action once
 * the response is received: group the fixes by cyclones so that the cyclone
 * tracks can be displayed on the map. The filtering method is located in 
 * fixesToCyclones.js
 */ 

var WMSurl = "http://115.146.84.158/cyclonetracks2/index2.php";
//var WMSurl = "http://reg.bom.gov.au/cgi-bin/ws/gis/ncc/tracker/beta/wfs_filter.cgi?source=sh&nofilter=true";

/*
 * Perform a POST request to WMSurl for an XML document, 
 * calls JSONCallback(jsonData) if successful, else calls errorCallback
 */
function WFSRequest(xmlData, JSONCallback, errorCallback, elementToPrintStatus){
    var requestObject;
    if (window.XMLHttpRequest){
        requestObject = new XMLHttpRequest();
    }
    requestObject.onreadystatechange=function(){
        if (requestObject.readyState==4 && requestObject.status==200){
            JSONCallback(requestObject.responseText, elementToPrintStatus);
        }
        else if (requestObject.readyState==4 && requestObject.status!=200){
            errorCallback(elementToPrintStatus);
        }
    }
    requestObject.open("POST", WMSurl, true);
    requestObject.setRequestHeader("Content-type","application/xml");
    requestObject.send(xmlData);
}

function printCyclones(data, elementToModify){
    features = data.features;
    var numberOfElements = features.length;
    htmldata = String(numberOfElements) + " elements: ";
    htmldata = htmldata + "<table>";
    for (var i = 0; i < numberOfElements; i++){
        htmldata = htmldata + "<tr><td>";
        htmldata = htmldata + features[i].properties.name + "  "; 
        htmldata = htmldata + "</td><td class=\"even\">";
        htmldata = htmldata + features[i].properties.tc_id + "  "; 
        htmldata = htmldata + "</td><td>";
        htmldata = htmldata + features[i].geometry.coordinates[0].datetime + "  ";
	htmldata = htmldata + "</td></tr>";
    }
    htmldata = htmldata + "</table>";
    document.getElementById(elementToModify).innerHTML = htmldata;
}

function performRequestFromXml(xmlData, elementToPrintStatus, failedRequest, successfulRequest){
    document.getElementById(elementToPrintStatus).innerHTML = "Requesting to WFS...";
    WFSRequest(xmlData, successfulRequest, failedRequest, elementToPrintStatus);
}
