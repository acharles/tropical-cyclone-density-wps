/*
 * This file contains code to get data from the forms on the web page.
 * The functions getYearData, getYearRangeData, and getDatelistData gather data
 * from the page and call the functions to build the XML query for the 
 * WFS filter.
 */ 

/* input the DOM element and a string,
 * returns a array
 */
function stringToArray(value, separator){
    if(value == ""){
        return null;
    }
	var array=value.split(',');
	return array;
}

/* input the DOM element array,
 * returns a array
 */
function elementsToArray(elementArray){
	var array=new Array();
	for (var i=0;i<elementArray.length;i++) {
		var val=elementArray[i].val();
		array.push(val);
	}
	return array;
}

/* convert yearlist to array
 * 
 */
function getYearListArray(){
	return stringToArray($('#yearlist').val(),',');
}
/* convert monthlist to array
 * 
 */
function getMonthListArray(){
	return stringToArray(document.getElementById("month02").value, ',');
}

/* convert bbox to array
 * 
 */
function getBBoxArray(suffix){
    bbox0 = document.getElementById("minx" + suffix).value;
    bbox1 = document.getElementById("miny" + suffix).value;
    bbox2 = document.getElementById("maxx" + suffix).value;
    bbox3 = document.getElementById("maxy" + suffix).value;
    if (bbox0 == "" || bbox1 == "" || bbox2 == "" || bbox3 == ""){
        return null;
    }
	return [bbox0, bbox1, bbox2, bbox3];
}

/* 
 * Convert sbox to array
 */
function getSBoxArray(suffix){
	var sbox=new Array();
	sbox0 = document.getElementById("sminx" + suffix).value;
    sbox1 = document.getElementById("sminy" + suffix).value;
    sbox2 = document.getElementById("smaxx" + suffix).value;
    sbox3 = document.getElementById("smaxy" + suffix).value;
    if (sbox0 == "" || sbox1 == "" || sbox2 == "" || sbox3 == ""){
        return null;
    }
    if (sbox0 == null || sbox1 == null || sbox2 == null || sbox3 == null){
        return null;
    }
	sbox.push(sbox0);
	sbox.push(sbox1);
	sbox.push(sbox2);
	sbox.push(sbox3);
	return (sbox);
}

/*retrieve date like this:
 * var dates=getDateList();
 * var year=dates[0].year;var month=dates[0].month;
 *
 */
function getDateList(){
	var strArray = stringToArray(document.getElementById('date_list').value, ',');
    if (strArray == null){
        return null;
    }
	var objArray=new Array();
	if(strArray.length>0){
		for(var i=0; i< strArray.length;i++){
			var date=new Object();
			date.year=strArray[i].slice(0,4);
			date.month=strArray[i].slice(4);
			objArray.push(date);
		}
	}
	return objArray;
}

function getYearRange(){
	var range = new Object();
    var start = document.getElementById("year_from").value;
    var end = document.getElementById("year_to").value;
    if(start == "" || end == ""){
        return null;
    }
    range.start = start;
	range.end = end;
	return range;
}

function getMonthRange(){
	var range = new Object();
    var start = document.getElementById("month_from").value;
    var end = document.getElementById("month_to").value;
    if(start == "" || end == ""){
        return null;
    }
    range.start = start;
	range.end = end;
	return range;
}

function getValue(name){
	var val = document.getElementById(name).value;
    if(val == ""){
        return null;
    }
    return val;
}

var sboxWFSFilterValues = null;

function getYearData(){
    var year = getYearListArray();
    var month = getMonthListArray();
    var hemisphere = getValue("hemisphere02");
    var bbox = getBBoxArray("02");
    // Will be used (or not) in successfulRequest
    sboxWFSFilterValues = getSBoxArray("02");
    var pressmin = getValue("pressmin02");
    var pressmax = getValue("pressmax02");
    var xml = getXMLYear(year, month, hemisphere, bbox, pressmin, pressmax);
    performRequestFromXml(xml, 'msg2', failedRequest, successfulRequest);
}

function getYearRangeData(){
    var year_range = getYearRange();
    var month_range = getMonthRange();
    var hemisphere = getValue("hemisphere03");
    var bbox = getBBoxArray("03");
    // Will be used (or not) in successfulRequest
    sboxWFSFilterValues = getSBoxArray("03");
    var pressmin = getValue("pressmin03");
    var pressmax = getValue("pressmax03");
    var xml = getXMLYearRange(year_range, month_range, hemisphere, bbox, pressmin, pressmax);
    performRequestFromXml(xml, 'msg2', failedRequest, successfulRequest);
}

function getDatelistData(){
    var datelist = getDateList();
    var hemisphere = getValue("hemisphere04");
    var bbox = getBBoxArray("04");
    // Will be used (or not) in successfulRequest
    sboxWFSFilterValues = getSBoxArray("04");
    var pressmin = getValue("pressmin04");
    var pressmax = getValue("pressmax04");
    var xml = getXMLDatelist(datelist, hemisphere, bbox, pressmin, pressmax);
    performRequestFromXml(xml, 'msg2', failedRequest, successfulRequest);
}

function failedRequest(elementToPrintStatus){
    document.getElementById(elementToPrintStatus).innerHTML = 
        'Request failed! (connection problem?)';
}

function successfulRequest(jsonData, elementToPrintStatus){
    document.getElementById(elementToPrintStatus).innerHTML = 
        'Server answered the request.';
 
    var data = null;
    try{
        data = JSON.parse(jsonData);
    }catch(e){
        var message = "The server didn't return proper JSON: ";
        message = message + "<br/> the output was: <br/>";
        message = message + jsonData;
        document.getElementById(elementToPrintStatus).innerHTML = message; 
        return;
    }
    if(data.type != 'FeatureCollection'){
        var message = "The server returned unexpected data: ";
        message = message + "<br/> the output was: <br/>";
        message = message + jsonData;
        document.getElementById(elementToPrintStatus).innerHTML = message;
        return;
    }
    
    if(data.features.length == 0){
        document.getElementById(elementToPrintStatus).innerHTML = "No cyclones returned!"
        return;
    }
    cyclones = newCycloneCollection(data);
    // Filter sbox
    if (sboxWFSFilterValues != null){
        cyclones = filterSbox(cycloneCollection, sboxWFSFilterValues);
        sboxWFSFilterValues = null;
    }
    
    document.getElementById(elementToPrintStatus).innerHTML = 
        cyclones.features.length + " cyclones returned.";
    addCycloneTracks(cyclones);
}
