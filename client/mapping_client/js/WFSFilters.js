/* 
 * This file has functions to create OGC filters used to filter cyclone fixes
 * The filters will be put in an XML file to be sent as a query to the 
 * filtering WFS.
 * There are no filters in this file for sbox and genesis.
 * 
 * Import OpenLayers (<script src="http://openlayers.org/api/OpenLayers.js"></script>)
 * before importing this file
 */

//This js is used for all IDC10022 filter arguments
//Filter for one year or year list
function FilterYear(year,hemisphere)
{
var datetime = null;
if (hemisphere.toUpperCase() == "N"){
	datetime = "-01-01 00:00:00+00";
	}
else if(hemisphere.toUpperCase() == "S"){
	datetime = "-07-01 00:00:00+00";
}
var yearInt = parseInt(year);
var filter = new OpenLayers.Filter.Logical({
    type: OpenLayers.Filter.Logical.AND,
    filters: [
        new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.GREATER_THAN_OR_EQUAL_TO,
            property: "datetime",
            value: String(yearInt)+datetime
        }),
        new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.LESS_THAN,
            property: "datetime",
            value: String(yearInt+1)+datetime
        })
    ]
});

	return filter;
}

//Filter for month arguments
function FilterMonth(month)
{
var filter = new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.EQUAL_TO,
            property: "EXTRACT(month from datetime)",
            value: String(month)
        });

	return filter;
} 

function filterYearList(year, hemisphere){
    yearFilter = null;
    if(year == null){
        return null;
    }
    if(year.length == 1){
        yearFilter = FilterYear(year[0],hemisphere);
    }
    else{
        yearFilters = [];
        // Get one filter per year
        for(i=0;i<year.length;i++){
            singleYearFilter = FilterYear(year[i],hemisphere);
            yearFilters.push(singleYearFilter);
        }
        // Add an 'or' around those year filters
        yearFilter  = new OpenLayers.Filter.Logical({
            type: OpenLayers.Filter.Logical.OR,
            filters: yearFilters
        });
    }
    return yearFilter;
}

function filterMonthList(month){
    monthFilter = null;
    if(month == null){
        return null;
    }
    if(month.length == 1){
        monthFilter = FilterMonth(month[0]);
    }
    else{
        monthFilters = [];
        // Get one filter per month
        for(i=0;i<month.length;i++){
            singleMonthFilter = FilterMonth(month[i]);
            monthFilters.push(singleMonthFilter);
        }
        // Add an 'or' around those month filters
        monthFilter  = new OpenLayers.Filter.Logical({
            type: OpenLayers.Filter.Logical.OR,
            filters: monthFilters
        });
    }
    return monthFilter;
}

function filterYearMonth(year, month, hemisphere){
    var yearMonthFilter = null;
    yearFilter = filterYearList(year, hemisphere);
    if(yearFilter == null){
        return null;
    }
    monthFilter = filterMonthList(month);
    if(monthFilter == null){
        return yearFilter;
    }
    else{
        yearMonthFilter  = new OpenLayers.Filter.Logical({
            type: OpenLayers.Filter.Logical.AND,
            filters: [yearFilter, monthFilter]
        });
    }
    return yearMonthFilter;
}

//Filter for both year range and month range arguments
function FilterYearRange(year_range,month_range,hemisphere)
{
    // The end date is strict inferior so we need to get the next month:
    var endDate = new Date(parseInt(year_range.end), parseInt(month_range.end), 1);
    endDate.setMonth(endDate.getMonth()+1);
    var endMonth = endDate.getMonth();
    
if (hemisphere.toUpperCase() == "N"){
	if(!month_range){
		var Startdatetime = String(year_range.start)+"-01-01 00:00:00+00";
		var Enddatetime = String(year_range.end)+"-01-01 00:00:00+00";
		}
	else	{
		var Startdatetime = String(year_range.start)+"-"+String(month_range.start)+"-01 00:00:00+00";
		var Enddatetime = String(year_range.end)+"-"+String(endMonth)+"-01 00:00:00+00";
		}
	}
else if(hemisphere.toUpperCase() == "S"){
	if(!month_range){
		var Startdatetime = String(year_range.start)+"-07-01 00:00:00+00";
		var Enddatetime = String(year_range.end)+"-07-01 00:00:00+00";
		}
	else	{
        var monthStartInt = parseInt(month_range.start);
		var monthStart = monthStartInt < 10 ? '0' + monthStartInt : monthStartInt;
		var monthEnd = endMonth < 10 ? '0' + endMonth : endMonth;
		if(monthStartInt<7){
			var Startdatetime = String(parseInt(year_range.start)+1)+"-"+monthStart+"-01 00:00:00+00";
		}
		else{
			var Startdatetime = String(year_range.start)+"-"+monthStart+"-01 00:00:00+00";
		}
		if(endMonth<7){
			var Enddatetime = String(parseInt(year_range.end)+1)+"-"+monthEnd+"-01 00:00:00+00";
		}
		else{
			var Enddatetime = String(year_range.end)+"-"+monthEnd+"-01 00:00:00+00";
		}
			}
	}
var filter = filter = new OpenLayers.Filter.Logical({
    type: OpenLayers.Filter.Logical.AND,
    filters: [
        new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.GREATER_THAN_OR_EQUAL_TO,
            property: "datetime",
            value: Startdatetime
        }),
        new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.LESS_THAN,
            property: "datetime",
            value: Enddatetime
        })
    ]
});

	return filter;
}

 
//Filter for one date list arguments
function FilterDate(year,month,hemisphere)
{
if (hemisphere.toUpperCase() == "N"){
	var date_year = year.name;
	var date_month = String(month < 10 ? '0' + month : month);
	}
else if(hemisphere.toUpperCase() == "S"){
    var monthInt = parseInt(month);
	var date_month = String(monthInt < 10 ? '0' + monthInt : monthInt);
    var yearInt = parseInt(year);
	if(parseInt(month)<7)
		{
			var date_year = String(yearInt+1);
		}
	else
		{
			var date_year = String(yearInt);
		}
}

var filter = filter = new OpenLayers.Filter.Logical({
    type: OpenLayers.Filter.Logical.AND,
    filters: [
       new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.EQUAL_TO,
            property: "EXTRACT(year from datetime)",
            value: date_year
        }),
        new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.EQUAL_TO,
            property: "EXTRACT(month from datetime)",
            value: date_month
        })
    ]
});

	return filter;
}

function FilterDateList(datelist, hemisphere){
    if(datelist == null || datelist.length == 0){
        return null;
    }
    dateFilters = [];
    for(i=0;i<datelist.length;i++){
        date = datelist[i];
        var dateFilter = FilterDate(date.year, date.month, hemisphere);
        dateFilters.push(dateFilter);
    }
    if(datelist.length == 1){
        // Only one datetime filter (adding 'or' around it would break it.)
        return dateFilters[0];
    }
    else{
        // Add an 'or' around those date filters
        var dateListFilter  = new OpenLayers.Filter.Logical({
            type: OpenLayers.Filter.Logical.OR,
            filters: dateFilters
        });
        return dateListFilter;
    }
}

//Filter for BBox
function FilterBBox(bbox)
{
    if(bbox == null){
        return null
    }
    minX = bbox[0];
    minY = bbox[1];
    maxX = bbox[2];
    maxY = bbox[3];
    /*
var filter = new OpenLayers.Filter.Spatial({
            type: OpenLayers.Filter.Spatial.BBOX,
			property: "snowman",
            value: new OpenLayers.Bounds(minX, minY, maxX, maxY),
            projection: "EPSG:4326"
});
*/
        
filterlon = new OpenLayers.Filter.Comparison({
                    type: OpenLayers.Filter.Comparison.BETWEEN,
                    property: "lon",
                    lowerBoundary:	String(bbox[0]),
                    upperBoundary:	String(bbox[2])
        });


filterlat = new OpenLayers.Filter.Comparison({
                    type: OpenLayers.Filter.Comparison.BETWEEN,
                    property: "lat",
                    lowerBoundary:	String(bbox[1]),
                    upperBoundary:	String(bbox[3])
        });
        
filterBbox = new OpenLayers.Filter.Logical({
            type: OpenLayers.Filter.Logical.AND,
            filters: [filterlon, filterlat]
        });

	return filterBbox;
}

//Filter for pressure
function FilterPressure(pressmin, pressmax)
{
    if(pressmin == null && pressmax == null){
        return null;
    }
    // Default value for pressmin
    pressmin = pressmin || "1";
    var filter = null;
    if (pressmax != null){
        filter = new OpenLayers.Filter.Comparison({
                    type: OpenLayers.Filter.Comparison.BETWEEN,
                    property: "pressure",
                    lowerBoundary:	pressmin,
                    upperBoundary:	pressmax
        });
    }
    else{
        filter = new OpenLayers.Filter.Comparison({
                    type: OpenLayers.Filter.Comparison.GREATER_THAN_OR_EQUAL_TO,
                    property: "pressure",
                    value:	pressmin
        });
    }

	return filter;
} 

function filterToString(filter){
    var filterAsString = null;
    if(filter == null){
        filterAsString = "";
    }
    else{
        var parser = new OpenLayers.Format.Filter.v1_1_0();
        var filterAsXml = parser.write(filter);
        var xml = new OpenLayers.Format.XML();
        var filterAsString = xml.write(filterAsXml);
    }
    
    var prefix = '<wfs:GetFeature xmlns:wfs="http://www.opengis.net/wfs" service="WFS" version="1.0.0" xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.0.0/WFS-transaction.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\
    <wfs:Query typeName="ms:IDC10022">';
    var suffix = '</wfs:Query> </wfs:GetFeature>';
    return prefix + filterAsString + suffix;
}

function arrayOfFilterForConjunctionToString(arrayOfFilters){
    if(arrayOfFilters.length == 0){
        return filterToString(null);
    }
    else if (arrayOfFilters.length == 1){
        return filterToString(arrayOfFilters[0]);
    }
    else{
        // Get first 2 elements from arrayOfFilters and store them in arraySlice
        //var arraySlice = arrayOfFilters.slice(0,2);
        // Add a conjunction filter around the filters
        var filter  = new OpenLayers.Filter.Logical({
            type: OpenLayers.Filter.Logical.AND,
            filters: arrayOfFilters
        });
        /*
        if (arrayOfFilters.length > 2){
            // Recursion to add another conjunction if needed
            var newArray = [filter];
            var filtersLeftToAdd = arrayOfFilters.slice(2)
            newArray = newArray.concat(filtersLeftToAdd);
            return arrayOfFilterForConjunctionToString(newArray);
        }
        * */
        return filterToString(filter);
    }
}
 
function getXMLYear(year, month, hemisphere, bbox, pressmin, pressmax){
    var filters = [];
    var filterTime = filterYearMonth(year, month, hemisphere);
    filters.push(filterTime);
    var filterArea = FilterBBox(bbox);
    filters.push(filterArea);
    var filterPres = FilterPressure(pressmin, pressmax)
    filters.push(filterPres);
    
    var filtersNotNull = [];
    for(i=0;i<filters.length;i++){
        if(filters[i] != null){
            filtersNotNull.push(filters[i]);
        }
    }

    return arrayOfFilterForConjunctionToString(filtersNotNull);
}

function getXMLYearRange(year_range, month_range, hemisphere, bbox, pressmin, pressmax){
    var filters = [];
    var filterTime = FilterYearRange(year_range, month_range, hemisphere);
    filters.push(filterTime);
    var filterArea = FilterBBox(bbox);
    filters.push(filterArea);
    var filterPres = FilterPressure(pressmin, pressmax)
    filters.push(filterPres);
    
    var filtersNotNull = [];
    for(i=0;i<filters.length;i++){
        if(filters[i] != null){
            filtersNotNull.push(filters[i]);
        }
    }

    return arrayOfFilterForConjunctionToString(filtersNotNull);
}

function getXMLDatelist(datelist, hemisphere, bbox, pressmin, pressmax){
    var filters = [];
    var filterTime = FilterDateList(datelist, hemisphere);
    filters.push(filterTime);
    var filterArea = FilterBBox(bbox);
    filters.push(filterArea);
    var filterPres = FilterPressure(pressmin, pressmax)
    filters.push(filterPres);
    
    var filtersNotNull = [];
    for(i=0;i<filters.length;i++){
        if(filters[i] != null){
            filtersNotNull.push(filters[i]);
        }
    }

    return arrayOfFilterForConjunctionToString(filtersNotNull);
}
 
