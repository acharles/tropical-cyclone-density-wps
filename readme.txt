This repository contains the Track Density WPS process and front end.
To use it, follow the installation instructions from wps/readme.txt.
Then follow the instructions in client/readme.txt.
In order to facilitate the configuration of the WPS process, you can use the
admin page located in bom/miscellaneous/AdminPage.

Directories:
    The wps directory contains the track density wps.

    The client directory contains files that are related to the client-side 
    (html, javascript, ...)

    The tools directory contains files used to set up to the wps.

    The miscellaneous directory contains files that do not fit the other 
    directories.
