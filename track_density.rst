Kernel Estimation of Tropical Cyclone Density
=============================================

:author: Andrew Charles

Kernel density estimation is a statstical technique for estimating a
probability density function from a finite set of point data samples.  Kernel
density estimation is simply described as dressing a set of data points with a
mixture of Gaussian (or Gaussian-shaped) kernels. The sum of these kernels
provides the field estimate.

This technique is useful for the analysis of tropical cyclone tracks because
it summarises information from a large number of fixes with a single field. 
Unlike methods based on the counting of storms that appear in or near grid
cells, density estimation is not sensitive to the resolution of the grid onto
which the field is mapped. The smoothed field does however depend strongly on
the kernel bandwidth, which is the range of smoothing applied to each point.

One major advantage of kernel density estimates is that if the densities are
normalised correctly, the density fields of different observational datasets or
models can be compared directly, even though the sources may be statistically
different.

This kernel can be very broad, which may produce a smoother field in the case
of many, noisy data points, or it may be narrow, in which case the radius of
influence of each data point is reduced.

Kernel estimation is implemented in scientific packages available in many
programming languages including Python, Java, and even Javascript.

The use of the method for tropical cyclone research is described in 

Ramsay, Hamish A., and Charles A. Doswell. “A Sensitivity Study of
Hodograph-Based Methods for Estimating Supercell Motion.” Weather and
Forecasting 20, no. 6 (December 2005): 954–970. doi:10.1175/WAF889.1.

and also in

Ramsay, Hamish A., Lance M. Leslie, Peter J. Lamb, Michael B. Richman, and Mark
Leplastrier. “Interannual Variability of Tropical Cyclones in the Australian
Region: Role of Large-Scale Environment.” Journal of Climate 21, no. 5 (March
2008): 1083–1103. doi:10.1175/2007JCLI1970.1.

At the time of writing, the Wikipedia article contains an accurate and concise
summary (http://en.wikipedia.org/wiki/Kernel_density_estimation) which appears to
be heavily based on the articles by Silverman and Rosenblatt:

Rosenblatt, Murray. “Remarks on Some Nonparametric Estimates of a Density
Function.” The Annals of Mathematical Statistics 27, no. 3 (September 1956):
832–837. doi:10.1214/aoms/1177728190.

Silverman, Bernard. W. Density Estimation for Statistics and Data Analysis. 1st
ed. Chapman & Hall/CRC, 1986.

