#!/bin/bash
#
# Author: Joon Hui WONG
#
# This script is meant to be used only on a fresh Nectar NCI's Fedora 19
# image and may require extensive changes to work otherwise.
#
# If it does not work, then you'll have to refer to the manual config
# for file changes (if the original file has changed since this was
# written, the script may not work).

# Define the following variable defaults first!
SERVER_IP="115.146.86.72" # IP or localhost
NAME="John Doe"

# Installing Dependencies
function install_dependencies {
    sudo yum -y install nano
    sudo yum -y install gcc 
    sudo yum -y groupinstall "Development Tools"
    sudo yum -y install freetype-devel
    sudo yum -y install python-matplotlib ipython ipython-notebook python-pandas sympy.noarch python-nose
    sudo yum -y install h5py
    sudo yum -y install netcdf4-python
    sudo yum -y install python-basemap
    sudo yum -y install python-psycopg2
    sudo yum -y install Cython
    sudo yum -y install httpd gcc-c++ zlib-devel libxml2-devel bison openssl openssl-devel subversion mod_fcgid libcurl curl-devel autoconf flex fcgi fcgi-devel
    #sudo yum -y install mozjs17 
    sudo yum -y install js-devel
    sudo yum -y install gdal gdal-devel
    sudo yum -y install python-pip
    sudo pip-python install -U pip
    sudo pip install beaker
}

# Get and Setup Zoo Project
function install_zooproject {
    svn co http://svn.zoo-project.org/svn/trunk zoo-project
    cd zoo-project/thirds/cgic206
    sudo make
    cd ../../zoo-project/zoo-kernel/
    sudo autoconf
    sudo ./configure --with-python
    sudo make

    # Fixing the test_service.py: Encoding Issues
    sed -i "2s/.*/# Author: $NAME/" ../zoo-services/hello-py/cgi-env/test_service.py

    # Setup main.cfg
    sed -i "4s/.*/serverAddress = http\:\/\/$SERVER_IP\/zoo/" main.cfg
    
    sudo cp main.cfg /var/www/cgi-bin/main.cfg
    sudo cp zoo_loader.cgi /var/www/cgi-bin/zoo_loader.cgi
    sudo cp ../zoo-services/hello-py/cgi-env/*.zcfg /var/www/cgi-bin
    sudo cp ../zoo-services/hello-py/cgi-env/*.py /var/www/cgi-bin
    sudo cp zoo_loader.cgi /var/www/cgi-bin
    
    # Testing the Zoo-Project Framework
    # TO-DO if you have too much time - Error checking and stop if Zoo does not work
    cd /var/www/cgi-bin
    ./zoo_loader.cgi "request=Execute&service=WPS&version=1.0.0&Identifier=HelloPy&DataInputs=a=$NAME"
    cd
}

# Setup Apache for allowing IP/zoo links (neater)
function setup_apache_for_zoo {
    sudo mkdir -p /var/www/html/zoo
    sudo sed -i "151s/AllowOverride None/AllowOverride All/" /etc/httpd/conf/httpd.conf
    sudo sed -i "95s/.*/ServerName $SERVER_IP\:80/" /etc/httpd/conf/httpd.conf
    echo "RewriteEngine on" > .htaccess
    echo "RewriteRule call/(.*)/(.*) /cgi-bin/zoo_loader.cgi?request=Execute&service=WPS&version=1.0.0&Identifier=$1&DataInputs=sid=$2&RawDataOutput=Result [L,QSA]" >> .htaccess
    echo "RewriteRule (.*)/(.*) /cgi-bin/zoo_loader.cgi?metapath=$1 [L,QSA]" >> .htaccess
    echo "RewriteRule (.*) /cgi-bin/zoo_loader.cgi [L,QSA]" >> .htaccess
    sudo mv .htaccess /var/www/html/zoo
    sudo apachectl restart
}

# We're using PostgreSQL v9.2
function install_postgresql92 {
    sudo rpm -Uvh http://yum.postgresql.org/9.2/fedora/fedora-19-x86_64/pgdg-fedora92-9.2-6.noarch.rpm
    sudo yum -y install postgresql92 postgresql92-server postgresql92-contrib
    sudo su - postgres -c /usr/pgsql-9.2/bin/initdb
    sudo sed -i "59s/#listen_addresses/listen_addresses/" /var/lib/pgsql/9.2/data/postgresql.conf
    sudo sed -i "59s/localhost/*/" /var/lib/pgsql/9.2/data/postgresql.conf
    sudo sed -i "63s/#port/port/" /var/lib/pgsql/9.2/data/postgresql.conf
    sudo systemctl start postgresql-9.2.service
    sudo systemctl enable postgresql-9.2.service
    sudo -u postgres psql -c "ALTER ROLE postgres WITH ENCRYPTED PASSWORD 'postgres'"
    sudo -u postgres psql -c "CREATE DATABASE tc WITH ENCODING 'UTF-8'"
    sudo sed -i "84s/trust/md5/" /var/lib/pgsql/9.2/data/pg_hba.conf
    sudo sed -i "86s/trust/md5/" /var/lib/pgsql/9.2/data/pg_hba.conf
    sudo sed -i "88s/trust/md5/" /var/lib/pgsql/9.2/data/pg_hba.conf
    sudo systemctl restart postgresql-9.2.service
}

function install_bom {
    # Clone Repository
    git clone https://bitbucket.org/bomproj/bom.git
    
    # Populate Database
    cp bom/wps/tcsh1969-2009.csv bom/tools/tcdata.csv
    sudo chmod 700 bom/tools/create_tc_storage.bash
    cd bom/tools
    sudo ./create_tc_storage.bash tcdata.csv
    rm tcdata.csv
    cd ../..
    
    # Enabling apache shell
    sudo chsh -s /bin/bash apache
    
    # Creating Folders for Output/Caching
    sudo mkdir -p /var/cache/TrackDensity
    sudo mkdir -p /var/www/html/temp
    sudo mkdir -p /var/www/html/pydap
    sudo mkdir -p /var/www/html/output
    sudo chown apache:apache /var/cache/TrackDensity
    sudo chown apache:apache /var/www/html/temp
    sudo chown apache:apache /var/www/html/pydap
    sudo chown apache:apache /var/www/html/output
    
    # Fixing variables
    sudo sed -i "5s/\/var\/www\/temp/\/var\/www\/html\/temp/" bom/wps/TrackDensityConfig.ini
    sudo sed -i "9s/\/var\/www\/pydap\/server\/data/\/var\/www\/html\/pydap/" bom/wps/TrackDensityConfig.ini
    sudo sed -i "s/115.146.84.158/$SERVER_IP/" bom/wps/TrackDensityConfig.ini
    sudo sed -i "13s/output_dir = \/var\/www\/output/output_dir = \/var\/www\/html\/output/" bom/wps/TrackDensityConfig.ini
    
    # Compiling the cython module
    cd bom/wps/modules/fixprocessor
    sudo python setup.py build_ext --inplace
    cd ../..
    
    # Turn them on in the config!
    sudo sed -i "s/False/True/" TrackDensityConfig.ini
    cd
}

function copy_bom {
    cd bom/wps
    sudo cp -fr . /var/www/cgi-bin
    cd
}

function set_permissions {
    cd /var/www/cgi-bin
    sudo chown apache:apache . -R
    cd
}

function run {
    install_dependencies
    install_zooproject
    setup_apache_for_zoo
    install_postgresql92
    install_bom
    copy_bom
    set_permissions
}

run