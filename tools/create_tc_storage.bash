#!/bin/bash

# Possible argument for CSV File 
if test "$1" != "" ; then
    CSV_FILE=$1
fi

##################################################
: ${CSV_FILE:='../wps/tcdata.csv'}

if [[ -f $CSV_FILE ]] ; then
    echo -e "\n##########################"
    echo "Using CSV FILE: $CSV_FILE"
    echo 
else 
    echo -e "\n##########################"
    echo "CSV FILE NOT FOUND: $CSV_FILE"
    echo "Exiting..."
    exit
fi

# DATABSE DETAILS
DB_NAME=tc
SCHEMA_NAME=tracker
PG_USER=postgres
PG_PASSWORD=postgres
PG_HOST=localhost
PG_PORT=5432

# HODB PROD
#PG_HOST=hodb

##################################################
# Anything below here should be left untouched
SQL_FILE=`basename $0 .bash`.sql
TMP_FILE=/tmp/create-tc-storage-$$.sql

TABLE_NAME=`basename $CSV_FILE .csv`

export PGPASSWORD=$PG_PASSWORD
#export PGPASSFILE=./.pgpass
PSQL_COMMAND="psql -U $PG_USER -h $PG_HOST -p $PG_PORT -d $DB_NAME"
PG_DSN="PG:user=$PG_USER password=$PG_PASSWORD host=$PG_HOST port=$PG_PORT dbname=$DB_NAME"

export SRID="4326"
export SRS="EPSG:${SRID}"

echo "SQL: $SQL_FILE"
echo "CSV: $CSV_FILE"
echo "TABLE: $TABLE_NAME"

# Change IFS to preserve leading whitespace
TMP_IFS=$IFS;IFS='\n' 

# Read file and substitute variables along the way
while read line
do
    eval "echo \"$line\""
done < $SQL_FILE  > $TMP_FILE
IFS=$TMP_IFS 

# INITIALISE SCHEMAS and raw file
$PSQL_COMMAND -c "CREATE SCHEMA $SCHEMA_NAME;"
$PSQL_COMMAND -c "DROP TABLE $SCHEMA_NAME.${TABLE_NAME}_raw;" 
ogr2ogr -F "PostgreSQL" "$PG_DSN" $CSV_FILE -a_srs $SRS -lco SCHEMA=$SCHEMA_NAME -nln ${TABLE_NAME}_raw

# Fill PostgreSQL database
$PSQL_COMMAND < $TMP_FILE

# Remove temporary SQL file
rm $TMP_FILE

