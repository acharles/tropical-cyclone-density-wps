""" A generic purpose kernel density script. """

import numpy as np
from math import *

def gauss_kernel(r,h):
    """ Ye old Gaussian SPH kernel in two dimensions Returns w """
    q = r/h
    factor = 1.e0 / ( (h**2) * (np.pi) )
    w = factor * exp(-q*q)
    return w

def splash_map_2d(x,y,r,m,h,bounds,cutoff): 
    """ Kernel density estimation for particles.
        Called 'splash_map' after Daniel Price's Splash
        SPH rendering code.
        
        res is the width of grid cells (todo, should be a parameter)

        x - x positions of grid
        y - y positions of grid
        r - particle positions
        bounds - box boundaries xmin,xmax,ymin,ymax
        cutoff - smoothing cutoff

    """

    nx = x.size
    ny = y.size

    if cutoff > nx: print "cutoff is too large this won't work"

    res = x[1]-x[0] #assume regular
    Z = np.zeros((nx,ny))
    xmin,xmax,ymin,ymax = bounds
    #loop over particle positions
    for i in range(m.size):
        # each particle contributes to pixels
        xpixmin = int(floor( (r[i,0] - cutoff - xmin)/res ) )
        ypixmin = int(floor( (r[i,1] - cutoff - ymin)/res ) )
        xpixmax = int(floor( (r[i,0] + cutoff - xmin)/res )   )
        ypixmax = int(floor( (r[i,1] + cutoff - ymin)/res )   )

        for ypx in range(ypixmin,ypixmax):
            for xpx in range(xpixmin,xpixmax):
                
                # where the pixel bounds cross the box bounds, need to 
                # wrap pixel coordinates
                if ypx >= ny:
                    continue
                if ypx < 0:
                    continue
                if xpx >= nx:
                    continue
                if xpx < 0:
                    continue
               
                dr = np.array( (r[i,0] - x[xpx], y[ypx] - r[i,1] ))
                rsq =  np.dot(dr,dr)
                if rsq < (cutoff*cutoff):
                    s = np.sqrt(rsq)
                    Z[xpx,ypx] += m[i]* gauss_kernel(s,h[i])
                                
    return Z

