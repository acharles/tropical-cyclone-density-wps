/* Remove geometry column entry for raw table since it does not have any geometries */
DELETE FROM geometry_columns WHERE f_table_schema = '${SCHEMA_NAME}' AND f_table_name = '${TABLE_NAME}_raw';

/*--------------------------------------------------
  TC Points
  --------------------------------------------------*/
DROP TABLE IF EXISTS ${SCHEMA_NAME}.${TABLE_NAME}_points CASCADE;

CREATE TABLE ${SCHEMA_NAME}.${TABLE_NAME}_points AS (
    SELECT 
        (year::int * 1000 + seq::int) AS tc_id,
        * 
    FROM ${SCHEMA_NAME}.${TABLE_NAME}_raw
    ORDER BY datetime
);

/* Prepare table for actual use */
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_points ALTER COLUMN datetime TYPE timestamp USING to_timestamp(datetime,'YYYYMMDDHH24MI');
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_points ALTER COLUMN year TYPE integer USING cast(trim(year) AS integer);
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_points ALTER COLUMN seq TYPE integer USING cast(trim(seq) AS integer);
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_points ALTER COLUMN lat TYPE float USING cast(trim(lat) AS float);
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_points ALTER COLUMN lon TYPE float USING cast(trim(lon) AS float);
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_points ALTER COLUMN pressure TYPE float USING CASE WHEN trim(pressure) = '' THEN NULL ELSE cast(trim(pressure) AS float) END;

/* create consistent id pk column */
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_points RENAME ogc_fid  TO id; 
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_points ADD CONSTRAINT pk_${TABLE_NAME}_points_id PRIMARY KEY (id);

/* From OGR2OGR 1.8 onwards a geometry column does not get created automatically */
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_points ADD COLUMN wkb_geometry geometry;
DELETE FROM geometry_columns WHERE f_table_schema = '${SCHEMA_NAME}' AND f_table_name = '${TABLE_NAME}_points';
INSERT INTO geometry_columns VALUES ('', '${SCHEMA_NAME}', '${TABLE_NAME}_points', 'wkb_geometry', 2, ${SRID}, 'POINT');

/* Try to convert the geometry column to a format that PostGIS 2.x understands better */
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_points ALTER COLUMN wkb_geometry TYPE geometry(Point, ${SRID});

/* Apply hack to make all negate longitudes positive */
--UPDATE ${SCHEMA_NAME}.${TABLE_NAME}_points SET lon = lon + 360,wkb_geometry = st_setsrid(makepoint(lon, lat), ${SRID})  WHERE lon < 0;
UPDATE ${SCHEMA_NAME}.${TABLE_NAME}_points SET lon = lon + 360 WHERE lon < 0;

/* Fill geometry column */
UPDATE ${SCHEMA_NAME}.${TABLE_NAME}_points SET wkb_geometry = st_setsrid(st_makepoint(lon::float, lat::float),${SRID});

-- DROP and CREATE indices
DROP INDEX IF EXISTS ${TABLE_NAME}_points_year_idx;
DROP INDEX IF EXISTS ${TABLE_NAME}_points_seq_idx;
DROP INDEX IF EXISTS ${TABLE_NAME}_points_datetime_idx;

CREATE INDEX ${TABLE_NAME}_points_year_idx ON ${SCHEMA_NAME}.${TABLE_NAME}_points (year);
CREATE INDEX ${TABLE_NAME}_points_seq_idx ON ${SCHEMA_NAME}.${TABLE_NAME}_points (seq);
CREATE INDEX ${TABLE_NAME}_points_datetime_idx ON ${SCHEMA_NAME}.${TABLE_NAME}_points (datetime);

/* Create spatial index */
CREATE INDEX idx_${TABLE_NAME}_points_wkb_geometry ON ${SCHEMA_NAME}.${TABLE_NAME}_points USING GIST (wkb_geometry);

/* Set geometry type to points */
UPDATE geometry_columns SET type='POINT' WHERE f_table_schema = '${SCHEMA_NAME}' AND f_table_name = '${TABLE_NAME}_points';

/*--------------------------------------------------
  TC Lines
  --------------------------------------------------*/
DROP TABLE IF EXISTS ${SCHEMA_NAME}.${TABLE_NAME}_lines;
CREATE TABLE ${SCHEMA_NAME}.${TABLE_NAME}_lines AS

SELECT 
        (st2.year * 1000 + st2.seq) AS tc_id,
        st2.year,
        st2.seq,
        st2.name,
        st2.dates_array[1] AS start,
        st2.dates_array[array_length(st2.dates_array,1)] AS end,

        /* The following 3 arrays won't work with MapServer on PostgreSQL 8.1 */
        --st2.dates_array, 
        --st2.winds_array,
        --st2.pressures_array,
 
        --linestringfromtext(
        st_geomfromtext(
            'LINESTRING(' 
            ||
            CASE WHEN (array_length(points_array,1) = 1) THEN 
                array_to_string(points_array || points_array,',')
            ELSE
                array_to_string(points_array,',')
            END
            || 
            ')' 
        , ${SRID}) AS wkb_geometry

FROM (
    SELECT
        st.year,
        st.seq,
        st.name,
        (select array_agg(datetime) from (select * from ${SCHEMA_NAME}.${TABLE_NAME}_points order by id) as s1 where s1.year = st.year and s1.seq = st.seq and s1.name = st.name) as dates_array,
        --(select array_agg(wind) from (select * from ${SCHEMA_NAME}.${TABLE_NAME}_points order by id) as s1 where s1.year = st.year and s1.seq = st.seq and s1.name = st.name) as winds_array,
        (select array_agg(pressure) from (select * from ${SCHEMA_NAME}.${TABLE_NAME}_points order by id) as s1 where s1.year = st.year and s1.seq = st.seq and s1.name = st.name) as pressures_array,
        (select array_agg(lon || ' ' || lat) from (select * from ${SCHEMA_NAME}.${TABLE_NAME}_points order by id) as s1 where s1.year = st.year and s1.seq = st.seq and s1.name = st.name) as points_array
    FROM ${SCHEMA_NAME}.${TABLE_NAME}_points AS st
    GROUP BY st.year, st.seq, st.name
    ) AS st2

ORDER BY year, seq;

-- add primary key column
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_lines ADD COLUMN id SERIAL;
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_lines ADD CONSTRAINT pk_${TABLE_NAME}_lines_id PRIMARY KEY (id);


/* Try to convert the geometry column to a format that PostGIS 2.x understands better */
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_lines ALTER COLUMN wkb_geometry TYPE geometry(Linestring, ${SRID});

DELETE FROM geometry_columns WHERE f_table_schema = '${SCHEMA_NAME}' AND f_table_name = '${TABLE_NAME}_lines';
INSERT INTO geometry_columns VALUES ('','${SCHEMA_NAME}','${TABLE_NAME}_lines','wkb_geometry',2, ${SRID},'LINESTRING');



/*--------------------------------------------------
  TC Segments
  --------------------------------------------------*/
DROP TABLE IF EXISTS ${SCHEMA_NAME}.${TABLE_NAME}_segments;
CREATE TABLE ${SCHEMA_NAME}.${TABLE_NAME}_segments AS
SELECT 
    (tc_points_1.year * 1000 + tc_points_1.seq) AS tc_id, 
    tc_points_1.year, tc_points_1.seq, tc_points_1.name,
    tc_points_1.pressure as pressure_start,
    tc_points_2.pressure as pressure_end,
    st_makeline(tc_points_1.wkb_geometry, tc_points_2.wkb_geometry) AS wkb_geometry,
    st_astext(st_makeline(tc_points_1.wkb_geometry, tc_points_2.wkb_geometry)) AS text

    --,pointn(wkb_geometry, generate_series(1, npoints(wkb_geometry)-1)) as sp,
        --pointn(wkb_geometry, generate_series(2, npoints(wkb_geometry)  )) as ep       
FROM ${SCHEMA_NAME}.${TABLE_NAME}_points AS tc_points_1

JOIN (
    SELECT 
        *
    FROM ${SCHEMA_NAME}.${TABLE_NAME}_points
) AS tc_points_2

ON
  tc_points_2.id = tc_points_1.id +1 AND
  tc_points_2.year = tc_points_1.year AND
  tc_points_2.seq  = tc_points_1.seq

--WHERE year = '1969' and seq = '1'
ORDER BY tc_points_1.id
--LIMIT 100
;

-- add primary key column
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_segments ADD COLUMN id SERIAL;
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_segments ADD CONSTRAINT pk_${TABLE_NAME}_segments_id PRIMARY KEY (id);

/* Try to convert the geometry column to a format that PostGIS 2.x understands better */
ALTER TABLE ${SCHEMA_NAME}.${TABLE_NAME}_segments ALTER COLUMN wkb_geometry TYPE geometry(Linestring, ${SRID});

DELETE FROM geometry_columns WHERE f_table_schema = '${SCHEMA_NAME}' AND f_table_name = '${TABLE_NAME}_segments';
INSERT INTO geometry_columns VALUES ('','${SCHEMA_NAME}','${TABLE_NAME}_segments','wkb_geometry',2, ${SRID},'MULTILINESTRING');

/* In case there are 'noname' values, rename them and give them a sequence ID! */
CREATE SEQUENCE seq_noname;
UPDATE ${SCHEMA_NAME}.${TABLE_NAME}_lines SET name = 'Unnamed - ' || cast(nextval('seq_noname') AS text) WHERE name = 'noname' OR name = 'unnamed' OR name = '';
UPDATE ${SCHEMA_NAME}.${TABLE_NAME}_points tcp SET name = (SELECT name FROM ${SCHEMA_NAME}.${TABLE_NAME}_lines tcl WHERE tcl.tc_id = tcp.tc_id) WHERE name = 'noname' OR name = 'unnamed' OR name = '';
UPDATE ${SCHEMA_NAME}.${TABLE_NAME}_segments tcs SET name = (SELECT name FROM ${SCHEMA_NAME}.${TABLE_NAME}_lines tcl WHERE tcl.tc_id = tcs.tc_id) WHERE name = 'noname' OR name = 'unnamed' OR name = '';
DROP SEQUENCE seq_noname;

